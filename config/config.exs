# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
use Mix.Config

# Configure Mix tasks and generators
config :oxpt,
  ecto_repos: [Oxpt.Repo]

config :oxpt_web,
  ecto_repos: [Oxpt.Repo],
  generators: [context_app: :oxpt]

# Configures the endpoint
config :oxpt_web, OxptWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "roqMmpepfGNV8TavNEA5jMIteUC47ro+q849/GZ0xFg0I6YtmR07qcYMKUQMiPBI",
  render_errors: [view: OxptWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Oxpt.PubSub,
  live_view: [signing_salt: "Q0f6GA8f"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason
config :phoenix, :filter_parameters, ["password"]

# Configuration for Oxpt Lounge
config :oxpt, :default_game, {
  Oxpt.Lounge,
  [
    games: [
      lounge: {Oxpt.Lounge, []}
      #boilerplate: {Oxpt.Boilerplate.Guest, []},
    ],
    cdn_host: System.get_env("CDN_HOST_DOMAIN") || "localhost:4000"
  ]
}

config :oxpt_web, :games, [
  {:lounge, {Oxpt.Lounge, []}},
  {:lounge_host, {Oxpt.Lounge.Host, []}}
  #{:boilerplate, {Oxpt.Boilerplate.Guest, []}},
  #{:boilerplate_host, {Oxpt.Boilerplate.Host, []}}
]

# Mailer configuration
# config :oxpt, Oxpt.Mailer,
  # adapter: Bamboo.MandrillAdapter,
  # api_key: "my_api_key"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
