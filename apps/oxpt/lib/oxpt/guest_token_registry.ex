defmodule Oxpt.GuestTokenRegistry do
  alias Cizen.SagaRegistry
  alias Cizen.SagaID
  alias Oxpt.RoomRegistry
  alias Oxpt.Persistence.Registers

  def child_spec(_opts) do
    %{
      id: __MODULE__,
      start: {SagaRegistry, :start_link, [[keys: :duplicate, name: __MODULE__]]}
    }
  end

  @spec register(SagaID.t(), SagaRegistry.key(), SagaRegistry.value()) ::
          {:ok, pid()} | {:error, :no_saga}
  def register(room_id, token, guest_id) do
    case SagaRegistry.register(__MODULE__, room_id, token, guest_id) do
      {:ok, _} = res ->
        if !Application.get_env(:oxpt, :restoring, false) && RoomRegistry.get_persists(room_id) do
          Registers.create(%{
            registry: __MODULE__,
            saga_id: room_id,
            key: token,
            value: guest_id
          })
          |> case do
            {:ok, _} ->
              :ok

            {:error, _} ->
              IO.inspect("Error")
          end
        end

        res

      err ->
        err
    end
  end

  @spec get(SagaID.t(), SagaRegistry.key()) :: {:ok, SagaRegistry.value()} | :error
  def get(room_id, token) do
    with entries <- SagaRegistry.lookup(__MODULE__, token),
         [{_room_id, guest_id}] <- Enum.filter(entries, fn {k, _} -> k == room_id end) do
      {:ok, guest_id}
    else
      _ -> :error
    end
  end

  @spec get_all(SagaRegistry.key()) :: [SagaRegistry.entry()]
  def get_all(token) do
    SagaRegistry.lookup(__MODULE__, token)
  end
end
