defmodule Oxpt.RoomKeyRegistry do
  alias Cizen.SagaRegistry
  alias Oxpt.RoomRegistry
  alias Oxpt.Persistence.Registers

  def start_link do
    SagaRegistry.start_link(keys: :unique, name: __MODULE__)
  end

  def get_room(key) do
    case SagaRegistry.lookup(__MODULE__, key) do
      [] -> nil
      [{room_id, _}] -> room_id
    end
  end

  def register(key, room_id) do
    case SagaRegistry.register(__MODULE__, room_id, key, :ok) do
      {:ok, _} = res ->
        if !Application.get_env(:oxpt, :restoring, false) && RoomRegistry.get_persists(room_id) do
          Registers.set(%{
            registry: __MODULE__,
            saga_id: room_id,
            key: key,
            value: :ok
          })
          |> case do
            {:ok, _} ->
              :ok

            {:error, _} ->
              IO.inspect("Error")
          end
        end

        res

      err ->
        err
    end
  end
end
