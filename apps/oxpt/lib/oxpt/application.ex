defmodule Oxpt.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Oxpt.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: Oxpt.PubSub},
      # Start a worker by calling: Oxpt.Worker.start_link(arg)
      # {Oxpt.Worker, arg}
      %{
        id: Oxpt.RoomKeyRegistry,
        start: {Oxpt.RoomKeyRegistry, :start_link, []
      }},
      {Oxpt.RoomRegistry, []},
      {Oxpt.GuestRegistry, []},
      {Oxpt.GuestRegistry.PlayingGameRegistry, []},
      {Oxpt.GuestRegistry.EnterRoomRegistry, []},
      {Oxpt.GuestTokenRegistry, []}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: Oxpt.Supervisor)
  end
end
