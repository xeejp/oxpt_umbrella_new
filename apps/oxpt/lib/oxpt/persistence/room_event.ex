defmodule Oxpt.Persistence.RoomEvent do
  use Ecto.Schema
  import Ecto.Changeset

  alias Oxpt.Persistence.Term

  schema "room_events" do
    field :event_id, :string
    field :saga_id, :string
    field :event_type, :string
    field :event_body, Term

    timestamps()
  end

  def changeset(room_event, attrs \\ %{}) do
    room_event
    |> cast(attrs, [:event_id, :saga_id, :event_type, :event_body])
    |> validate_required([:event_id, :saga_id, :event_type])
  end
end
