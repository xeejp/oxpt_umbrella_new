defmodule Oxpt.Persistence.Restorer do
  @moduledoc """
  Persists rooms and guests.
  """

  use Cizen.Effectful
  use Cizen.Effects
  alias Cizen.Saga

  alias Oxpt.Persistence.{RoomEvents, Registers}
  alias Oxpt.{Room, Guest}
  alias Oxpt.{RoomRegistry, RoomKeyRegistry, GuestRegistry, GuestTokenRegistry}

  require Logger

  defstruct []

  def run() do
    Application.put_env(:oxpt, :restoring, true)

    handle(fn ->
      %{room_events: RoomEvents.list(), registers: Registers.list()}
      |> start_rooms()
      |> room_registry()
      |> room_key_registry()
      |> start_games()
      |> start_guests()
      |> guest_registry()
      |> guest_token_registry()
    end)

    :timer.sleep(1000)
    Application.put_env(:oxpt, :restoring, false)
    Logger.info("completely restored!")
  end

  defp start_rooms(%{room_events: room_events} = state) do
    room_events
    |> Enum.filter(fn re -> re.event_type === "yield_room" end)
    |> Enum.map(fn %{saga_id: saga_id, event_body: %{state: room_state}} ->
      Saga.resume(saga_id, %Room{persists?: true, start_game: false}, room_state)
    end)

    Logger.info("resumed rooms")
    :timer.sleep(300)

    state
  end

  defp start_games(%{room_events: room_events} = state) do
    room_events
    |> Enum.filter(fn re -> re.event_type === "yield_game" end)
    |> Enum.map(fn %{saga_id: saga_id, event_body: %{yield: %{state: game_state}, game: saga}} ->
      Saga.resume(saga_id, saga, game_state)
    end)

    Logger.info("resumed games")
    :timer.sleep(300)
    state
  end

  defp start_guests(%{room_events: room_events} = state) do
    room_events
    |> Enum.filter(fn re -> re.event_type === "entered" end)
    |> Enum.map(fn %{event_body: %{guest_id: guest, room_id: room}} ->
      Saga.start(%Guest{room_id: room}, saga_id: guest)
    end)

    Logger.info("resumed guests")
    :timer.sleep(300)
    state
  end

  defp room_registry(%{registers: registers} = state) do
    registers
    |> Enum.filter(fn r -> r.registry === RoomRegistry end)
    |> Enum.map(fn %{saga_id: saga, key: key, value: value} ->
      case key do
        {:default_game, _} ->
          RoomRegistry.put_default_game(saga, value)

        {:persists?, _} ->
          RoomRegistry.put_persists(saga, value)

        _ ->
          RoomRegistry.add_room(key, saga)
      end
    end)

    Logger.info("restored RoomRegistry")
    state
  end

  defp room_key_registry(%{registers: registers} = state) do
    registers
    |> Enum.filter(fn r -> r.registry === RoomKeyRegistry end)
    |> Enum.map(fn %{saga_id: saga, key: key} ->
      RoomKeyRegistry.register(key, saga)
    end)

    Logger.info("restored RoomKeyRegistry")
    state
  end

  defp guest_registry(%{registers: registers} = state) do
    registers
    |> Enum.filter(fn r -> r.registry === GuestRegistry.PlayingGameRegistry end)
    |> Enum.map(fn %{saga_id: saga, value: value} ->
      GuestRegistry.put_playing_game(saga, value)
    end)

    Logger.info("restored GuestRegistry")
    state
  end

  defp guest_token_registry(%{registers: registers} = state) do
    registers
    |> Enum.filter(fn r -> r.registry === GuestTokenRegistry end)
    |> Enum.map(fn %{saga_id: saga, key: key, value: value} ->
      GuestTokenRegistry.register(saga, key, value)
    end)

    Logger.info("restored GuestTokenRegistry")
    state
  end
end
