defmodule Oxpt.Persistence.Room do
  @moduledoc """
  Persists rooms and guests.
  """

  use Cizen.Automaton
  use Cizen.Effects

  alias Cizen.Filter
  alias Cizen.Automaton.Yield

  alias Oxpt.Persistence.{RoomEvents, RoomEvent}

  # Events
  alias Oxpt.StoredRoomEvent
  alias Oxpt.EnterRoom
  alias Oxpt.StartGame

  defstruct [:room_id]

  @impl true
  def spawn(%__MODULE__{room_id: room}) do
    perform(%Subscribe{
      event_filter:
        Filter.any([
          Filter.new(fn %EnterRoom.Entered{room_id: ^room} -> true end),
          Filter.new(fn %StartGame{room_id: ^room} -> true end),
          Filter.new(fn %Yield{saga_id: ^room} -> true end)
        ])
    })

    {:loop, %{room_id: room}}
  end

  @impl true
  def yield(state) do
    event = perform(%Receive{})

    handle_received(event, event, state)
  end

  def handle_received(event, %EnterRoom.Entered{room_id: room_id} = body, state) do
    RoomEvents.create(%{
      event_id: event.id,
      saga_id: room_id,
      event_type: "entered",
      event_body: body
    })
    |> case do
      {:ok, %RoomEvent{id: room_event_id}} ->
        perform(%Dispatch{
          body: %StoredRoomEvent{room_id: room_id, id: room_event_id, type: "entered"}
        })

      {:error, _} ->
        IO.inspect("Error")
    end

    state
  end

  def handle_received(event, %StartGame{room_id: room_id} = body, state) do
    RoomEvents.create(%{
      event_id: event.id,
      saga_id: room_id,
      event_type: "start_game",
      event_body: body
    })
    |> case do
      {:ok, %RoomEvent{id: room_event_id}} ->
        perform(%Dispatch{
          body: %StoredRoomEvent{room_id: room_id, id: room_event_id, type: "start_game"}
        })

      {:error, _} ->
        IO.inspect("Error")
    end

    state
  end

  def handle_received(event, %Yield{} = body, {:loop, %{room_id: room_id}} = state) do
    RoomEvents.set(%{
      event_id: event.id,
      saga_id: room_id,
      event_type: "yield_room",
      event_body: body
    })
    |> case do
      {:ok, %RoomEvent{id: room_event_id}} ->
        perform(%Dispatch{
          body: %StoredRoomEvent{room_id: room_id, id: room_event_id, type: "yield_room"}
        })

      {:error, _} ->
        IO.inspect("Error")
    end

    state
  end
end
