defmodule Oxpt.Persistence.Registers do
  @moduledoc """
  The Registers context.
  """

  import Ecto.Query, warn: false
  alias Oxpt.Repo

  alias Oxpt.Persistence.Register

  @doc """
  Returns the list of Register.

  ## Examples

      iex> list()
      [%Register{}, ...]

  """
  def list do
    Repo.all(Register)
  end

  @doc """
  Gets a single register.

  Raises `Ecto.NoResultsError` if the Register does not exist.

  ## Examples

      iex> get!(123)
      %Register{}

      iex> get!(456)
      ** (Ecto.NoResultsError)

  """
  def get!(id), do: Repo.get!(Register, id)

  @doc """
  Creates a register.

  ## Examples

      iex> create(%{field: value})
      {:ok, %Register{}}

      iex> create(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create(attrs) do
    %Register{}
    |> Register.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Insert or update a register.

  ## Examples

      iex> set(%{field: value})
      {:ok, %Register{}}

      iex> set(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def set(attrs) do
    case Repo.get_by(Register,
           registry: attrs[:registry],
           saga_id: attrs[:saga_id],
           key: attrs[:key]
         ) do
      nil ->
        create(attrs)

      register ->
        Ecto.Changeset.change(register,
          value: attrs[:value]
        )
        |> Repo.update()
    end
  end

  @doc """
  Deletes a Register.

  ## Examples

      iex> delete(register)
      {:ok, %Register{}}

      iex> delete(register)
      {:error, %Ecto.Changeset{}}

  """
  def delete(attrs) do
    case Repo.get_by(Register,
           registry: attrs[:registry],
           saga_id: attrs[:saga_id],
           key: attrs[:key]
         ) do
      nil ->
        {:error, :not_found}

      register ->
        Repo.delete(register)
    end
  end

  @doc """
  Deletes all Registers.

  """
  def truncate() do
    Repo.delete_all(Register)
  end
end
