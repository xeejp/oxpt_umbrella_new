defmodule Oxpt.Persistence.Register do
  use Ecto.Schema
  import Ecto.Changeset

  alias Oxpt.Persistence.Term

  schema "registers" do
    field :registry, Term
    field :saga_id, :string
    field :key, Term
    field :value, Term

    timestamps()
  end

  @doc false
  def changeset(register, attrs) do
    register
    |> cast(attrs, [:registry, :saga_id, :key, :value])
    |> validate_required([:registry, :saga_id, :key])
  end
end
