defmodule Oxpt.Persistence.Term do
  @behaviour Ecto.Type
  @impl Ecto.Type
  def type, do: :string

  @impl Ecto.Type
  def cast(binary = <<131, _::binary>>) do
    try do
      {:ok, :erlang.binary_to_term(binary)}
    catch
      _ ->
        {:error, "Invalid binary"}
    end
  end

  @impl Ecto.Type
  def cast(base64) when is_binary(base64) do
    case base64 |> Base.decode64() do
      {:ok, binary} ->
        cast(binary)

      :error ->
        {:ok, base64}
    end
  end

  @impl Ecto.Type
  def cast(any), do: {:ok, any}

  @impl Ecto.Type
  def load(any) do
    cast(any)
  end

  @impl Ecto.Type
  def dump(any) do
    try do
      {:ok, :erlang.term_to_binary(any) |> Base.encode64()}
    catch
      _ ->
        :error
    end
  end

  @doc false
  @impl Ecto.Type
  def equal?(term1, term2) do
    term1 == term2
  end
end
