defmodule Oxpt.Persistence.Game do
  @moduledoc """
  Persists games.
  """

  use Cizen.Automaton
  use Cizen.Effects

  alias Cizen.Automaton.Yield
  alias Cizen.CizenSagaRegistry
  alias Cizen.Filter

  alias Oxpt.RoomRegistry
  alias Oxpt.Persistence.{RoomEvents, RoomEvent}

  # Events
  alias Oxpt.JoinGame
  alias Oxpt.StoredRoomEvent

  defstruct [:room_id, :game_id, :game]

  def setup(id, room_id, persists? \\ nil) do
    case persists? do
      nil ->
        RoomRegistry.get_persists(room_id)

      _ ->
        persists?
    end
    |> if do
      perform(%Start{
        saga: %__MODULE__{game_id: id, room_id: room_id}
      })
    end
  end

  @impl true
  def spawn(%__MODULE__{room_id: room_id, game_id: game_id}) do
    perform(%Subscribe{
      event_filter:
        Filter.any([
          Filter.new(fn %JoinGame{game_id: ^game_id} -> true end),
          Filter.new(fn %Yield{saga_id: ^game_id} -> true end)
        ])
    })

    {:loop, %{room_id: room_id, game_id: game_id}}
  end

  @impl true
  def yield({:loop, state}) do
    event = perform(%Receive{})

    state = handle_received(event, event, state)

    {:loop, state}
  end

  def handle_received(
        event,
        %JoinGame{} = body,
        %{room_id: room_id, game_id: game_id} = state
      ) do
    RoomEvents.create(%{
      event_id: event.id,
      saga_id: game_id,
      event_type: "join_game",
      event_body: body
    })
    |> case do
      {:ok, %RoomEvent{id: room_event_id}} ->
        perform(%Dispatch{
          body: %StoredRoomEvent{room_id: room_id, id: room_event_id, type: "join_game"}
        })

      {:error, _} ->
        IO.inspect("Error")
    end

    state
  end

  def handle_received(event, %Yield{} = body, %{room_id: room_id, game_id: game_id} = state) do
    {:ok, saga} = CizenSagaRegistry.get_saga(game_id)

    RoomEvents.set(%{
      event_id: event.id,
      saga_id: game_id,
      event_type: "yield_game",
      event_body: %{yield: body, game: saga}
    })
    |> case do
      {:ok, %RoomEvent{id: room_event_id}} ->
        perform(%Dispatch{
          body: %StoredRoomEvent{room_id: room_id, id: room_event_id, type: "yield_game"}
        })

      {:error, _} ->
        IO.inspect("Error")
    end

    state
  end
end
