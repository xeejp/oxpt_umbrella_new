defmodule Oxpt.Persistence.RoomEvents do
  @moduledoc """
  The RoomEvents context.
  """

  import Ecto.Query, warn: false
  alias Oxpt.Repo

  alias Oxpt.Persistence.RoomEvent

  @doc """
  Returns the list of RoomEvent.

  ## Examples

      iex> list()
      [%RoomEvent{}, ...]

  """
  def list do
    Repo.all(RoomEvent)
  end

  @doc """
  Gets a single room_event.

  Raises `Ecto.NoResultsError` if the RoomEvent does not exist.

  ## Examples

      iex> get!(123)
      %RoomEvent{}

      iex> get!(456)
      ** (Ecto.NoResultsError)

  """
  def get!(id), do: Repo.get!(RoomEvent, id)

  @doc """
  Creates a room_event.

  ## Examples

      iex> create(%{field: value})
      {:ok, %RoomEvent{}}

      iex> create_room_evnet(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create(attrs \\ %{}) do
    %RoomEvent{}
    |> RoomEvent.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Insert or update a room_event.

  ## Examples

      iex> set(%{field: value})
      {:ok, %RoomEvent{}}

      iex> set(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def set(attrs \\ %{}) do
    case Repo.get_by(RoomEvent, saga_id: attrs[:saga_id], event_type: attrs[:event_type]) do
      nil ->
        create(attrs)

      room_event ->
        Ecto.Changeset.change(room_event,
          event_id: attrs[:event_id],
          event_body: attrs[:event_body]
        )
        |> Repo.update()
    end
  end

  @doc """
  Deletes a RoomEvent.

  ## Examples

      iex> delete(room_event)
      {:ok, %RoomEvent{}}

      iex> delete(room_event)
      {:error, %Ecto.Changeset{}}

  """
  def delete(%RoomEvent{} = room_event) do
    Repo.delete(room_event)
  end

  @doc """
  Deletes all RoomEvents.

  """
  def truncate() do
    Repo.delete_all(RoomEvent)
  end
end
