defmodule Oxpt.Game do
  @moduledoc """
  A behaviour module for a game.
  """
  use Cizen.Effects
  use Cizen.Effectful

  alias Cizen.Saga
  alias Cizen.SagaID
  alias Oxpt.Player.{Input, Request}

  defmodule JoinGuest do
    @type t :: %__MODULE__{game_id: SagaID.t(), guest_id: SagaID.t()}
    @enforce_keys [:game_id, :guest_id]
    defstruct game_id: nil,
              guest_id: nil,
              host: false

    defmodule Joined do
      @type t :: %__MODULE__{game_id: SagaID.t()}
      @enforce_keys [:game_id]
      defstruct [:game_id]
    end
  end

  @callback install_deps(opts :: keyword) :: term
  @callback build_assets(out_dir :: Path.t(), public_url :: String.t()) :: term
  @callback watch_assets(out_dir :: Path.t(), public_url :: String.t()) :: term
  @callback join_guest(JoinGuest.t()) :: JoinGuest.Joined.t()
  @callback input(%Input{}) :: term
  @callback request(%Request{}) :: {:ok, result :: term} | {:error, reason :: term} | :timeout
  @callback new(room_id :: SagaID.t(), opts :: keyword) :: Saga.t()
  @callback metadata() :: term

  def run_watcher(watcher, opts) do
    script = """
    #{Enum.join(watcher, " ")} &
    pid=$!
    while read line ; do
    :
    done
    kill $pid
    """

    System.cmd("sh", ["-c", script], opts)
  end

  defmacro __using__(opts) do
    root_dir = opts[:root_dir]
    index = opts[:index] || "index.jsx"
    oxpt_path = opts[:oxpt_path] || "../../oxpt"

    quote do
      @behaviour Oxpt.Game
      @root_dir unquote(root_dir)

      @impl Oxpt.Game
      def build_assets(out_dir, public_url) do
        IO.inspect(@root_dir)

        opts = [into: IO.stream(:stdio, :line), cd: unquote(root_dir)]
        install_deps(opts)

        #System.cmd(
        #  "npm",
        #  ["run", "build", "--", "--dist-dir", out_dir, "--public-url", public_url],
        #  opts
        # )

        System.cmd(
          "npx",
          [
            "webpack",
            unquote(root_dir) <> "/src/index.jsx",
            "--mode",
            "production",
            "--watch-options-stdin",
            "--config",
            unquote(root_dir) <> "/webpack.config.js"
          ],
          opts
        )
      end

      @impl Oxpt.Game
      def watch_assets(out_dir, public_url) do
        opts = [into: IO.stream(:stdio, :line), cd: unquote(root_dir)]
        install_deps(opts)

        alias Oxpt.Game

        Game.run_watcher(
          [
            "npx",
            "webpack",
            unquote(root_dir) <> "/src/index.jsx",
            "--mode",
            "development",
            "--watch-stdin",
            "--config",
            unquote(root_dir) <> "/webpack.config.js"
          ],
          opts
        )
      end

      @impl Oxpt.Game
      def install_deps(opts) do
        # System.cmd("npm", ["install", "--save", unquote(oxpt_path)], opts)
        # System.cmd("npm", ["install", "--save", "parcel"], opts)
      end

      @impl Oxpt.Game
      def input(input) do
        IO.warn("ignored input: #{inspect(input)}")
      end

      @impl Oxpt.Game
      def request(request) do
        IO.warn("ignored request: #{inspect(request)}")
      end

      @impl Oxpt.Game
      def metadata(), do: %{}

      defoverridable metadata: 0,
                     build_assets: 2,
                     watch_assets: 2,
                     install_deps: 1,
                     input: 1,
                     request: 1
    end
  end
end
