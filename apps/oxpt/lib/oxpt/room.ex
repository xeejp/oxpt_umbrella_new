defmodule Oxpt.Room do
  use Cizen.Automaton
  use Cizen.Effects

  alias Cizen.{Filter, Dispatcher, Saga}
  alias Cizen.Automaton.Call

  alias Oxpt.GuestRegistry
  alias Oxpt.Guest

  # Events
  alias Oxpt.EnterRoom
  alias Oxpt.StartGame
  alias Oxpt.JoinGame

  alias Oxpt.Persistence

  defdelegate guests(room_id), to: GuestRegistry, as: :get_all

  defstruct persists?: false, start_game: true

  @impl true
  def spawn(%__MODULE__{persists?: persists?, start_game: start_game}) do
    id = Saga.self()

    if persists? do
      perform(%Fork{
        saga: %Persistence.Room{room_id: id}
      })
    end

    perform(%Subscribe{
      event_filter:
        Filter.any([
          Filter.new(fn %StartGame{room_id: ^id} -> true end)
        ])
    })

    if start_game do
      {module, params} = Application.get_env(:oxpt, :default_game)

      default_game =
        perform(%Fork{
          saga: module.new(id, params ++ [persists?: persists?])
        })

      %{
        default_game: default_game
      }
    else
      %{
        default_game: nil
      }
    end
  end

  @impl true
  def yield(state) do
    id = Saga.self()
    event = perform(%Receive{})

    case event do
      %Call{request: %EnterRoom{}, from: from} ->
        guest =
          perform(%Fork{
            saga: %Guest{room_id: id}
          })

        entered = %EnterRoom.Entered{
          room_id: id,
          guest_id: guest,
          default_game: state.default_game
        }

        Dispatcher.dispatch(entered)
        Saga.reply(from, entered)

      %Call{request: %StartGame{game: game}, from: from} ->
        game_id =
          perform(%Fork{
            saga: game
          })

        Saga.reply(from, %StartGame.Started{game_id: game_id})
    end

    state
  end

  def enter(room_id, host \\ false) do
    %{guest_id: guest_id, default_game: game_id} = Saga.call(room_id, %EnterRoom{host: host})

    join_game = %JoinGame{
      guest_id: guest_id,
      game_id: game_id,
      host: host
    }

    Dispatcher.dispatch(join_game)
    Saga.call(guest_id, join_game)

    guest_id
  end

  def start_game(room_id, game) do
    Saga.call(room_id, %StartGame{room_id: room_id, game: game})
  end
end
