defmodule Oxpt.Guest do
  @moduledoc """
  Stores and broadcast view states for a guest.
  """

  use Cizen.Automaton
  defstruct [:room_id, :dummy]

  use Cizen.Effects
  alias Cizen.{Automaton, Filter, Saga, Dispatcher}
  alias Cizen.Automaton.Call
  alias Oxpt.GuestRegistry

  # Events
  alias Oxpt.JoinGame
  alias Oxpt.LeaveGame
  alias Oxpt.RemoveGuest

  @impl true
  def spawn(%__MODULE__{room_id: room}) do
    id = Saga.self()

    perform(%Subscribe{
      event_filter:
        Filter.any([
          Filter.new(fn %JoinGame{guest_id: ^id} -> true end),
          Filter.new(fn %RemoveGuest{guest_id: ^id} -> true end)
        ])
    })

    GuestRegistry.register(room, id)
    :loop
  end

  @impl true
  def yield(:loop) do
    id = Saga.self()

    event = perform(%Receive{})

    case event do
      %JoinGame{} = join_game ->
        handle_join_game(join_game)
        :loop

      %Call{from: from, request: %JoinGame{} = join_game} ->
        Saga.reply(from, handle_join_game(join_game))
        :loop

      %RemoveGuest{} ->
        playing = GuestRegistry.get_playing_game(id)

        perform(%Dispatch{
          body: %LeaveGame{guest_id: id, game_id: playing}
        })

        Automaton.finish()
    end
  end

  defp handle_join_game(%JoinGame{game_id: game}) do
    id = Saga.self()

    playing = GuestRegistry.get_playing_game(id)

    unless is_nil(playing) do
      perform(%Dispatch{
        body: %LeaveGame{guest_id: id, game_id: playing}
      })
    end

    GuestRegistry.put_playing_game(id, game)

    joined = %JoinGame.Joined{game_id: playing, guest_id: id}
    Dispatcher.dispatch(joined)

    joined
  end
end
