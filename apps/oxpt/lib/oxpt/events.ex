defmodule Oxpt.EnterRoom do
  @enforce_keys []
  defstruct [:host]

  defmodule Entered do
    @enforce_keys [:room_id, :guest_id, :default_game]
    defstruct [:room_id, :guest_id, :default_game]
  end
end

defmodule Oxpt.RemoveGuest do
  @enforce_keys [:guest_id]
  defstruct [:guest_id]
end

defmodule Oxpt.LeaveGame do
  @enforce_keys [:game_id, :guest_id]
  defstruct [:game_id, :guest_id]
end

defmodule Oxpt.JoinGame do
  @enforce_keys [:game_id, :guest_id]
  defstruct [:game_id, :guest_id, :host]

  defmodule Joined do
    @enforce_keys [:game_id, :guest_id]
    defstruct [:game_id, :guest_id, :host]
  end
end

defmodule Oxpt.StartGame do
  @enforce_keys [:room_id, :game]
  defstruct [:room_id, :game]

  defmodule Started do
    @enforce_keys [:game_id]
    defstruct [:game_id]
  end
end

defmodule Oxpt.StoredRoomEvent do
  @enforce_keys [:room_id, :id, :type]
  defstruct [:room_id, :id, :type]
end

defmodule Oxpt.Player.Input do
  @enforce_keys [:game_id, :guest_id, :event, :payload]
  defstruct [:game_id, :guest_id, :event, :payload]
end

defmodule Oxpt.Player.Output do
  @enforce_keys [:game_id, :guest_id, :event, :payload]
  defstruct [:game_id, :guest_id, :event, :payload]
end

defmodule Oxpt.Player.Request do
  @enforce_keys [:game_id, :guest_id, :event, :payload]
  defstruct [:game_id, :guest_id, :event, :payload, :timeout]
end

defmodule Oxpt.GetLog do
  @enforce_keys [:game_id, :guest_id]
  defstruct [:game_id, :guest_id]
end
