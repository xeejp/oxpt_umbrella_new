defmodule Oxpt.RoomRegistry do
  alias Cizen.SagaRegistry
  alias Oxpt.Persistence.Registers

  def child_spec(_opts) do
    %{
      id: __MODULE__,
      start: {SagaRegistry, :start_link, [[keys: :duplicate, name: __MODULE__]]}
    }
  end

  def get_rooms(user) do
    entries = SagaRegistry.lookup(__MODULE__, user)
    Enum.map(entries, &elem(&1, 0))
  end

  def delete_room(user, room) do
    case SagaRegistry.unregister(__MODULE__, room, user) do
      {:ok, _} = res ->
        if !Application.get_env(:oxpt, :restoring, false) && get_persists(room) do
          Registers.delete(%{
            registry: __MODULE__,
            saga_id: room,
            key: user
          })
          |> case do
            {:ok, _} ->
              :ok

            {:error, _} ->
              IO.inspect("Error")
          end
        end

        res

      err ->
        err
    end
  end

  def add_room(user, room) do
    case SagaRegistry.register(__MODULE__, room, user, :ok) do
      {:ok, _} = res ->
        if !Application.get_env(:oxpt, :restoring, false) && get_persists(room) do
          Registers.create(%{
            registry: __MODULE__,
            saga_id: room,
            key: user,
            value: :ok
          })
          |> case do
            {:ok, _} ->
              :ok

            {:error, _} ->
              IO.inspect("Error")
          end
        end

        res

      err ->
        err
    end
  end

  def get_default_game(room) do
    case SagaRegistry.lookup(__MODULE__, {:default_game, room}) do
      [] -> nil
      [{^room, game}] -> game
    end
  end

  def put_default_game(room, game) do
    case SagaRegistry.register(__MODULE__, room, {:default_game, room}, game) do
      {:ok, _} = res ->
        if !Application.get_env(:oxpt, :restoring, false) && get_persists(room) do
          Registers.set(%{
            registry: __MODULE__,
            saga_id: room,
            key: {:default_game, room},
            value: game
          })
          |> case do
            {:ok, _} ->
              :ok

            {:error, _} ->
              IO.inspect("Error")
          end
        end

        res

      err ->
        err
    end
  end

  def get_persists(room) do
    case SagaRegistry.lookup(__MODULE__, {:persists?, room}) do
      [] -> nil
      [{^room, persists?}] -> persists?
    end
  end

  def put_persists(room, persists?) do
    case SagaRegistry.register(__MODULE__, room, {:persists?, room}, persists?) do
      {:ok, _} = res ->
        if !Application.get_env(:oxpt, :restoring, false) && persists? do
          Registers.set(%{
            registry: __MODULE__,
            saga_id: room,
            key: {:persists?, room},
            value: persists?
          })
          |> case do
            {:ok, _} ->
              :ok

            {:error, _} ->
              IO.inspect("Error")
          end
        end

        res

      err ->
        err
    end
  end
end
