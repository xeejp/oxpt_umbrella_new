defmodule Oxpt.Repo do
  use Ecto.Repo,
    otp_app: :oxpt,
    adapter: Ecto.Adapters.Postgres
end
