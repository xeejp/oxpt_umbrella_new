defmodule Oxpt.Email do
  import Bamboo.Email
  # import Bamboo.Phoenix

  def contact_email(player_uuid, account_uuid, username, name, email, subject, body) do
    base_email()
    |> from(email)
    |> subject(subject)
    |> text_body("player_uuid #{player_uuid}\r\naccount_uuid #{account_uuid}\r\nusername #{username}\r\nname #{name}\r\nemail #{email}\r\nsubject #{subject}\r\nbody #{body}")
    |> html_body("player_uuid #{player_uuid}<br />account_uuid #{account_uuid}<br />username #{username}<br />name #{name}<br />email #{email}<br />subject #{subject}<br />body #{body}")
  end

  def sign_up_email(player_uuid, account_uuid, username) do
    base_email()
    |> from("sign_in@xee.jp")
    |> subject("[New User]")
    |> text_body("player_uuid #{player_uuid}\r\naccount_uuid #{account_uuid}\r\nusername #{username}")
    |> html_body("player_uuid #{player_uuid}<br />account_uuid #{account_uuid}<br />username #{username}")
  end

  def delete_account_email(player_uuid, account_uuid) do
    base_email()
    |> from("delete_account@xee.jp")
    |> subject("[Delete User]")
    |> text_body("player_uuid #{player_uuid}\r\naccount_uuid #{account_uuid}")
    |> html_body("player_uuid #{player_uuid}<br />account_uuid #{account_uuid}")
  end

  def new_room_email(player_uuid, room_id, persists, room_key, host_game_id, host_guest_id) do
    base_email()
    |> from("new_room@xee.jp")
    |> subject("[New Room]")
    |> text_body("player_uuid #{player_uuid}\r\nroom_id #{room_id}\r\npersists #{persists}\r\nroom_key #{room_key}\r\nhost_game_id #{host_game_id}\r\nhost_guest_id #{host_guest_id}")
    |> html_body("player_uuid #{player_uuid}<br />room_id #{room_id}<br />persists #{persists}<br />room_key #{room_key}<br />host_game_id #{host_game_id}<br />host_guest_id #{host_guest_id}")
  end

  defp base_email do
    new_email()
    |> to("contact@xee.jp")
  end
end