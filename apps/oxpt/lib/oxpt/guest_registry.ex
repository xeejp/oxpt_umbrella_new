defmodule Oxpt.GuestRegistry do
  alias Cizen.SagaRegistry
  alias Oxpt.RoomRegistry
  alias Oxpt.Persistence.Registers

  defmodule PlayingGameRegistry do
    def child_spec(_opts) do
      %{
        id: __MODULE__,
        start: {SagaRegistry, :start_link, [[keys: :unique, name: __MODULE__]]}
      }
    end
  end

  defmodule EnterRoomRegistry do
    def child_spec(_opts) do
      %{
        id: __MODULE__,
        start: {SagaRegistry, :start_link, [[keys: :unique, name: __MODULE__]]}
      }
    end
  end

  def register(room, guest) do
    res = SagaRegistry.register(__MODULE__, guest, room, guest)
    SagaRegistry.register(__MODULE__.PlayingGameRegistry, guest, guest, nil)
    SagaRegistry.register(__MODULE__.EnterRoomRegistry, guest, guest, room)

    if !Application.get_env(:oxpt, :restoring, false) && RoomRegistry.get_persists(room) do
      case Registers.set(%{
             registry: __MODULE__.PlayingGameRegistry,
             saga_id: guest,
             key: guest,
             value: nil
           }) do
        {:ok, _} ->
          :ok

        {:error, e} ->
          IO.inspect(e, label: "Error")
      end
    end

    res
  end

  def get_all(room) do
    Enum.map(SagaRegistry.lookup(__MODULE__, room), &elem(&1, 1))
  end

  def child_spec(_opts) do
    %{
      id: __MODULE__,
      start: {SagaRegistry, :start_link, [[keys: :duplicate, name: __MODULE__]]}
    }
  end

  def get_playing_game(guest) do
    case SagaRegistry.lookup(__MODULE__.PlayingGameRegistry, guest) do
      [] -> nil
      [{_guest, game}] -> game
    end
  end

  def put_playing_game(guest, game) do
    res =
      SagaRegistry.update_value(__MODULE__.PlayingGameRegistry, guest, guest, fn _ -> game end)

    room = get_room(guest)

    if !Application.get_env(:oxpt, :restoring, false) && RoomRegistry.get_persists(room) do
      Registers.set(%{
        registry: __MODULE__.PlayingGameRegistry,
        saga_id: guest,
        key: guest,
        value: game
      })
      |> case do
        {:ok, _} ->
          :ok

        {:error, _} ->
          IO.inspect("Error")
      end
    end

    res
  end

  def get_room(guest) do
    case SagaRegistry.lookup(__MODULE__.EnterRoomRegistry, guest) do
      [] -> nil
      [{_guest, room}] -> room
    end
  end
end
