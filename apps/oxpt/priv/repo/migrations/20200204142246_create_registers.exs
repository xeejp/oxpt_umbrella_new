defmodule Oxpt.Repo.Migrations.CreateRegisters do
  use Ecto.Migration

  def change do
    create table(:registers) do
      add :registry, :text, null: false, primary: true
      add :saga_id, :string, primary: true
      add :key, :text, primary: true
      add :value, :text

      timestamps()
    end

    create index(:registers, [:registry, :saga_id, :key])
  end
end
