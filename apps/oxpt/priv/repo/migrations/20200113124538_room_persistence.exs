defmodule Oxpt.Repo.Migrations.RoomPersistence do
  use Ecto.Migration

  def change do
    create table(:room_events) do
      add :event_id, :string, null: false
      add :saga_id, :string, null: false, primary: true
      add :event_type, :string, null: false, primary: true
      add :event_body, :text

      timestamps()
    end

    create index(:room_events, [:saga_id, :event_type])
  end
end
