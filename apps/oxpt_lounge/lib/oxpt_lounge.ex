defmodule Oxpt.Lounge do
  use Cizen.Automaton
  defstruct [:room_id, :params]

  use Cizen.Effects
  use Cizen.Effectful
  alias Cizen.{Dispatcher, Filter, Saga}
  alias Cizen.Automaton.Call
  alias Oxpt.JoinGame
  alias Oxpt.Lounge.Host
  alias Oxpt.Persistence
  alias Oxpt.Game
  alias Oxpt.Game.JoinGuest

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../..") |> Path.expand(),
    index: "src/index.jsx",
    oxpt_path: "../oxpt"

  # @impl Oxpt.Game
  # def watch_assets(out_dir, public_url) do
  # end

  def player_socket(game_id, guest_id) do
    {:ok, %__MODULE__{room_id: room_id}} = Saga.get_saga(game_id)
    %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id, room_id: room_id}
  end

  @impl Oxpt.Game
  def join_guest(%JoinGuest{game_id: game_id, host: true} = join_guest) do
    host_game_id = Saga.call(game_id, :host_game_id)
    Host.join_guest(%JoinGuest{join_guest | game_id: host_game_id})
  end

  def join_guest(%JoinGuest{game_id: game_id, guest_id: guest_id}) do
    {:ok, %{room_id: room_id}} = Saga.get_saga(game_id)
    saga = %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id, room_id: room_id}
    Saga.start(saga, lifetime: game_id)
    %JoinGuest.Joined{game_id: game_id}
  end

  @impl Oxpt.Game
  def input(input) do
    Dispatcher.dispatch(input)
  end

  @impl Oxpt.Game
  def request(_request) do
    # Dispatcher.dispatch(request)
  end

  @impl Oxpt.Game
  def new(room_id, params) do
    %__MODULE__{room_id: room_id, params: params}
  end

  @impl Oxpt.Game
  def metadata(),
    do: %{
      label: "label_lounge",
      category: "category_other"
    }

  @impl true
  def spawn(%__MODULE__{room_id: room_id, params: params}) do
    id = Saga.self()
    Persistence.Game.setup(id, room_id, params[:persists?])

    host =
      unless Application.get_env(:oxpt, :restoring, false) do
        perform(%Start{saga: Host.new(room_id, lounge_id: id, params: params)})
      else
        nil
      end

    perform(%Subscribe{
      event_filter:
        Filter.new(fn %JoinGame{
                        game_id: ^id,
                        host: true
                      } ->
          true
        end)
    })

    %{host: host}
  end

  @impl true
  def yield(state) do
    id = Saga.self()
    event = perform(%Receive{})

    case event do
      %Call{from: from, request: :host_game_id} ->
        Saga.reply(from, state.host)

      %Call{from: from, request: %JoinGuest{guest_id: guest_id}} ->
        id =
          perform(%Fork{
            saga: player_socket(id, guest_id)
          })

        Saga.reply(from, id)

      %JoinGame{host: true, guest_id: guest_id} ->
        perform(%Dispatch{
          body: %JoinGame{game_id: state.host, guest_id: guest_id, host: true}
        })
    end

    state
  end
end
