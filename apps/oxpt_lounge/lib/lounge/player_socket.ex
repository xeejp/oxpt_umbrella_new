defmodule Oxpt.Lounge.PlayerSocket do
  defstruct [:game_id, :guest_id, :room_id, :channel_pid]

  use Cizen.Saga

  alias Cizen.{Dispatcher, Filter}
  alias Oxpt.Player.{Input, Output}
  alias Oxpt.GuestRegistry
  alias Oxpt.Lounge.{FetchPreloads, UpdatePreloadsAll, UpdatePreloads}
  require Filter

  @impl true
  def on_start(%__MODULE__{game_id: game_id, guest_id: guest_id} = socket) do
    Dispatcher.listen(Filter.new(fn %UpdatePreloadsAll{lounge_id: ^game_id} -> true end))
    Dispatcher.listen(Filter.new(fn %Input{game_id: ^game_id, guest_id: ^guest_id} -> true end))

    Dispatcher.listen(
      Filter.new(fn %UpdatePreloads{
                      lounge_id: ^game_id,
                      guest_id: ^guest_id
                    } ->
        true
      end)
    )

    Dispatcher.dispatch(%FetchPreloads{
      lounge_id: game_id,
      guest_id: guest_id
    })

    %{socket: socket}
  end

  @impl true
  def handle_event(%UpdatePreloadsAll{preloads: preloads}, state) do
    Dispatcher.dispatch(%Output{
      game_id: state.socket.game_id,
      guest_id: state.socket.guest_id,
      event: "update_preloads",
      payload: %{preloads: preloads}
    })

    state
  end

  @impl true
  def handle_event(%UpdatePreloads{preloads: preloads, base_url: base_url}, state) do
    Dispatcher.dispatch(%Output{
      game_id: state.socket.game_id,
      guest_id: state.socket.guest_id,
      event: "update_preloads",
      payload: %{preloads: preloads, base_url: base_url}
    })

    state
  end

  def handle_event(%Input{event: "get_guest_list"}, %{socket: socket} = state) do
    guest_list = GuestRegistry.get_all(socket.room_id)

    response =
      Enum.map(
        guest_list,
        fn guest_list ->
          %{
            game_id: GuestRegistry.get_playing_game(guest_list),
            guest_id: guest_list
          }
        end
      )

    # Why get_guest_list sends a guest_list to ALL of guests？
    guest_list
    |> Enum.each(fn guest_id ->
      Dispatcher.dispatch(%Output{
        game_id: state.socket.game_id,
        guest_id: guest_id,
        event: "update_guest_list",
        payload: %{guest_list: response}
      })
    end)

    state
  end
end
