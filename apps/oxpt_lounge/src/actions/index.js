import { createActions, handleActions } from 'redux-actions'
import { fromSnakeToCamel } from '../util'

export const initialState = {
  props: {
    guestList: [],
    preloads: [],
    baseUrl: null,
  }
}

export const { requestGuestList, receiveGuestList, receivePreloads } = createActions({
  REQUEST_GUEST_LIST: () => null,
  RECEIVE_GUEST_LIST: (guestList) => ({guestList: guestList}),
  RECEIVE_PRELOADS: (payload) => payload,
})

export const reducer = handleActions({
  RECEIVE_GUEST_LIST: (state, action) => ({...state, guestList: action.payload.guestList}),
  RECEIVE_PRELOADS: (state, action) => ({...state, ...fromSnakeToCamel(action.payload)})
}, initialState)
