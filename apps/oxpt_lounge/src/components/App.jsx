import React, { useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

import { useTranslation } from "react-i18next";
import { makeStyles } from "@material-ui/core/styles";
import PreloaderList from "./preloader_list";

const useStyles = makeStyles((theme) => ({
  items: {
    padding: theme.spacing(2),
    backgroundColor: "#999",
  },
  container: {
    padding: theme.spacing(2),
    paddingBottom: "2.5em",
  },
  chart: {
    height: "70vh",
  },
}));

function App() {
  const classes = useStyles();
  const { t, i18n } = useTranslation();

  useEffect(() => {
    window.addEventListener("message", messageListener, false);
    return () => window.removeEventListener("message", messageListener, false);
  }, []);

  const messageListener = (event) => {
    if (event.data.lang && event.data.lang !== i18n.language) {
      i18n.changeLanguage(event.data.lang);
    }
  };

  return (
    <Grid
      container
      direction="row"
      justify="space-between"
      alignItems="stretch"
      className={classes.container}
      spacing={4}
    >
      <Grid item xs={12}>
        <Typography variant="h5">{t("waiting.title")}</Typography>
        <Typography variant="subtitle1">
          {t("waiting.message")}
        </Typography>
      </Grid>
      <Grid item container xs={12} alignItems="center" spacing={2}>
        <Grid item xs={12}>
          <Typography variant="h5">{t("preload.title")}</Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant="subtitle1">
            {t("preload.message")}
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <PreloaderList />
        </Grid>
      </Grid>
    </Grid>
  );
}

export default App;
