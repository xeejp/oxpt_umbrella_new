import React from "react";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import usePreloaders from "../hooks/use_preloaders";
import PreloaderCard from "./preloader_list/preloader_card";
import { useTranslation } from "react-i18next";

const useStyles = makeStyles((_theme) => ({
  cardContainer: {
    paddingBottom: "1.5em",
  },
}));

export default () => {
  const preloaders = usePreloaders();
  const classes = useStyles();
  const { t } = useTranslation();
  return (
    <Grid container alignItems="center">
      {preloaders.map((preloader, i) => (
        <Grid className={classes.cardContainer} item key={`preloader-${i}`} xs={12}>
          <PreloaderCard title={t("preload.module", {num: i+1})} preloader={preloader} />
        </Grid>
      ))}
    </Grid>
  );
};
