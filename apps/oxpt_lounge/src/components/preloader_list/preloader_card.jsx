import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress'
import CachedIcon from '@material-ui/icons/Cached';

export default function PreloaderCard({preloader, title}) {
  return (
    <Card>
      <CardHeader
        action={
          <IconButton aria-label={preloader.progress} disabled>
            { preloader.progress == 0 ?
              <CachedIcon />
            :
              <CircularProgress variant="determinate" value={preloader.progress} />
            }
          </IconButton>
        }
        title={title}
        subheader={`${preloader.progress}%`}
      />
    </Card>
  );
}
