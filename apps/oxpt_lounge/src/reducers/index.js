import PropTypes from 'prop-types'
import { combineReducers } from 'redux'
import { reducer } from '../actions'
import { connectRouter } from 'connected-react-router'

const rootReducer = (history) => combineReducers({
  props: reducer,
  router: connectRouter(history),
})

rootReducer.propTypes = {
  hostory: PropTypes.object.isRequired,
}

export default rootReducer
