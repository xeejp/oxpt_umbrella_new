import '@babel/polyfill'
import React, { Suspense } from "react";
import ReactDOM from "react-dom";

import { Provider } from 'react-redux'
import App from './components/App'
import { ConnectedRouter as Router } from 'connected-react-router'
import configureStore, { history } from './store/configureStore'
import rootSaga from './saga'
import channel from './socket'

// Theme
import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'

// i18n
import i18n from './i18n'
import { useTranslation } from 'react-i18next'

channel.join()
let store = configureStore()
store.runSaga(rootSaga)
let theme = createMuiTheme()
theme = responsiveFontSizes(theme)

// Spinner
function Spinner() {
  const { t } = useTranslation('common')
  return (
    <div id='loading'>
      <div id='loadingText'>
        <div id='loadingImage'></div>
        {t('loading')}
      </div>
    </div>
  )
}

ReactDOM.render(
  <Provider store={store.store}>
    <Router history={history}>
      <ThemeProvider theme={theme}>
        <Suspense fallback={<Spinner />}>
          <App />
        </Suspense>
      </ThemeProvider>
    </Router>
  </Provider>,
  document.getElementById('root')
)