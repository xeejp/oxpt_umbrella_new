import { takeEvery, put, take, call } from 'redux-saga/effects'
import { eventChannel } from 'redux-saga'
import channel from '../socket'
import * as Actions from '../actions'

// channel.join()

function subscribe() {
  return eventChannel(emit => {
    channel.on("update_guest_list", ({guest_list: guestList}) => {
      emit(Actions.receiveGuestList(guestList))
    })

    channel.on("update_preloads", (payload) => {
      emit(Actions.receivePreloads(payload))
    })

    return () => {}
  })
}

function * tryGetGuestList(_action) {
  channel.push("input", {event: "get_guest_list", payload: null})
}

function * tryUpdateGuestList(_action) {
  channel.push("input", {event: "update_guest_list", payload: null})
}

function * receiveSocket(action) {
  yield put(action)
}

export default function * rootSaga() {
  const chan = yield call(subscribe)

  yield takeEvery("REQUEST_GUEST_LIST", tryGetGuestList)
  yield takeEvery("REQUEST_UPDATE_GUEST_LIST", tryUpdateGuestList)
  yield takeEvery(chan, receiveSocket)
}
