import i18n from 'i18next'
import Backend from 'i18next-xhr-backend'
import LanguageDetector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'
import enTrans from './locales/en/common.json'
import jaTrans from './locales/ja/common.json'

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: 'en',

    // have a common namespace used around the full app
    ns: ['common'],
    defaultNS: 'common',

    debug: true,

    resources: {
      en: { common: enTrans },
      ja: { common: jaTrans }
    },

    interpolation: {
      escapeValue: false // not needed for react!!
    },

    react: {
      useSuspense: false
    },

    returnObjects: true
  })

export default i18n
