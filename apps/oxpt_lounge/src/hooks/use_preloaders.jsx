import React, { useEffect, useState } from "react";
import { useSelector } from 'react-redux'

export default () => {
  const { preloads, baseUrl } = useSelector(state => state.props)
  const [xhr, setXhr] = useState(new XMLHttpRequest);
  const [state, setState] = useState({current: NaN, preloaders: []});
  const { current, preloaders} = state

  useEffect(() => {
    if (preloads.length !== 0) {
      setState({
        current: 0,
        preloaders: preloads.reduce((acc, preload) => {acc.push({...preload, progress: 0}); return acc}, [])
      })
    }
  }, [preloads]);

  useEffect(() => {
    if (current < preloads.length) {
      const newXhr = new XMLHttpRequest();
      newXhr.open("GET", `${baseUrl ? baseUrl : ""}/games/${preloads[current].name}/index.js`)
      newXhr.addEventListener("load", onLoad)
      newXhr.addEventListener("progress", onProgress)
      newXhr.addEventListener("error", onError)
      newXhr.send();
      setXhr(newXhr);
      return () => newXhr.abort();
    }
  }, [state.current])

  const updateElement = (array, index, value) => {
    let newArray = array;
    newArray[index] = value;
    return newArray;
  }

  const onLoad = function () {
    setState({
      current: current + 1,
      preloaders: updateElement(preloaders, current, {...preloaders[current], progress: 100})
    });
  };

  const onProgress = function (event) {
    const progress = Math.floor((event.loaded / event.total) * 100)
    setState({
      ...state,
      preloaders: updateElement(preloaders, current, {...preloaders[current], progress, message: xhr.statusText, status: xhr.status})
    });
  };

  const onError = function () {
    setState({
      current: current + 1,
      preloaders: updateElement(preloaders, current, {...preloaders[current], message: "Request failed"})
    });
  };

  return preloaders
};

export const usePreloader = (preload) => {
  const [xhr] = useState(new XMLHttpRequest());
  const [state, setState] = useState({});

  useEffect(() => {
    if (preload) {
      xhr.open("GET", `/games/${preload}/index.js`);
      xhr.send();
      return () => xhr.abort();
    }
  }, [preload]);

  xhr.onload = function () {
    setState({
      progress: 100, message: xhr.statusText, status: xhr.status
    });
  };

  xhr.onprogress = function (event) {
    if (event.lengthComputable) {
      setState({
        progress: Math.floor((event.loaded / event.total) * 100),
        message: xhr.statusText,
        status: xhr.status,
      });
    } else {
      setState({
        progress: 100,
        message: xhr.statusText,
        status: xhr.status,
      });
    }
  };

  xhr.onerror = function () {
    setState({
      ...state,
      message: "Request failed",
    });
  };

  return state
};