defmodule Mix.Tasks.Oxpt do
  @base_dir __ENV__.file
            |> Path.dirname()
            |> Path.join("../../priv/static/games")
            |> Path.expand()
  def run(function) do
    Application.get_env(:oxpt_web, :games)
    |> IO.inspect(label: "Games")
    |> Enum.map(fn {name, {module, _params}} ->
      string_name = to_string(name)
      out_dir = Path.join(@base_dir, string_name)
      public_url = "/games/#{string_name}"

      Task.async(fn ->
        apply(module, function, [out_dir, public_url])
      end)
    end)
    |> Enum.each(fn task -> Task.await(task, :infinity) end)
  end
end
