defmodule Mix.Tasks.Oxpt.Build do
  use Mix.Task

  def run(_args) do
    Mix.Tasks.Oxpt.run(:build_assets)
  end
end
