defmodule Mix.Tasks.Oxpt.Watch do
  use Mix.Task

  def run(_args) do
    Mix.Tasks.Oxpt.run(:watch_assets)
  end
end
