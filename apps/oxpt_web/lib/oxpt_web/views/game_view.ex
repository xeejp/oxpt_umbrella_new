defmodule OxptWeb.GameView do
  use OxptWeb, :view

  def render("index.json", %{games: games}) do
    %{data: render_many(games, __MODULE__, "game.json")}
  end

  def render("show.json", %{game: game}) do
    %{data: render_one(game, __MODULE__, "game.json")}
  end

  def render("game.json", %{game: game}) do
    %{
      room_id: elem(game, 0),
      guest_id:
        case elem(game, 1) do
          {:host, guest_id} -> %{host: true, id: guest_id}
          guest_id -> %{host: false, id: guest_id}
        end
    }
  end

  def render("error.json", %{reason: reason}) do
    %{errors: reason}
  end
end
