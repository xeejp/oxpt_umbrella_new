defmodule OxptWeb.Router do
  use OxptWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug CORSPlug, origin: "*"
    plug :accepts, ["json"]
  end

  pipeline :js do
    plug :accepts, ["js", "js.map"]
  end

  scope "/", OxptWeb do
    pipe_through :browser
    pipe_through :api
    pipe_through :js

    get "/game", GameController, :index
    get "/game/:game_id/:guest_id",  GameController, :show

    get "/", PageController, :index
    get "/about", PageController, :index
    get "/account", PageController, :index
    get "/award", PageController, :index
    get "/contact", PageController, :index
    get "/create_room", PageController, :index
    get "/delete_account", PageController, :index
    get "/faq", PageController, :index
    get "/game_list", PageController, :index
    get "/room", PageController, :index
    get "/room_list", PageController, :index
    get "/room_management", PageController, :index
    get "/team", PageController, :index
    get "/terms_of_service", PageController, :index
    get "/usage", PageController, :index
    
    get "/delete_player_token", PageController, :delete_player_token
    get "/delete_account_token", PageController, :delete_account_token
    get "/delete_all", PageController, :delete_all
    get "/put_player_token/:player_token/:redirect_url", PageController, :put_player_token
    get "/put_account_token/:account_token/:redirect_url", PageController, :put_account_token
  end

  # Other scopes may use custom stacks.
  # scope "/api", OxptWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: OxptWeb.Telemetry
    end
  end
end
