defmodule OxptWeb.RoomChannel do
  use Phoenix.Channel
  use Cizen.Effectful
  use Cizen.Effects

  alias Cizen.Saga
  alias Oxpt.{Room, RoomKeyRegistry, GuestTokenRegistry, GuestRegistry, RoomRegistry}

  require Logger

  def join("room:host", _payload, socket) do
    {:ok, socket}
  end

  def join("room:guest", _payload, socket) do
    {:ok, socket}
  end

  def join(_topic, _params, _socket) do
    {:error, %{reason: :not_found}}
  end

  def handle_in("create_room", %{"roomKey" => room_key, "persists" => persists?}, socket) do
    replaced_room_key = room_key |> String.downcase() |> String.trim()
    player_uuid = socket.assigns.player_uuid
    
    case create_room(replaced_room_key, persists?) do
      {:ok, room_id, room_key} ->
        case enter_room(room_id, player_uuid, host: true) do
          {:ok, guest_id} ->
            case get_playing_game(guest_id) do
              {:ok, game_id} ->
                response = %{
                  room_id: room_id,
                  room_key: room_key,
                  persists: persists?,
                  guest_id: guest_id,
                  game_id: game_id
                }
                
                case Oxpt.Email.new_room_email(player_uuid, room_id, persists?, room_key, game_id, guest_id) |> Oxpt.Mailer.deliver_now(response: true) do
                  {:ok, _, _} -> 
                    {:reply, {:ok, response}, socket}
                  {:error, error} -> 
                    {:reply, {:error, %{error: error}}, socket}
                end

              {:error, _} ->
                {:reply, {:error, %{reason: :get_game_id_failed}}, socket}
            end
          {:error, _} ->
            {:reply, {:error, %{reason: :enter_room_failed}}, socket}
        end

      {:error, :already} ->
        {:reply, {:error, %{reason: :already_exist}}, socket}
      
      {:error, :invalid} ->
        {:reply, {:error, %{reason: :already_exist}}, socket}

      {:error, _} ->
        {:reply, {:error, %{reason: :unknown_error}}, socket}
    end
  end

  def handle_in("get_guest_room_list", _, socket) do
    player_uuid = socket.assigns.player_uuid

    room_list =
      GuestTokenRegistry.get_all(player_uuid)
      |> Enum.map(&elem(&1, 0))

    response =
      Enum.map(
        room_list,
        fn room_id ->
          [room_key | _tail] = Cizen.SagaRegistry.keys(Oxpt.RoomKeyRegistry, room_id)

          %{
            room_key: room_key,
            room_id: room_id
          }
        end
      )

      {:reply, {:ok, response}, socket}
  end

  def handle_in("get_host_room_list", _, socket) do
    player_uuid = socket.assigns.player_uuid

    room_list =
      GuestTokenRegistry.get_all({:host, player_uuid})
      |> Enum.map(&elem(&1, 0))

    response =
      Enum.map(
        room_list,
        fn room_id ->
          [room_key | _tail] = Cizen.SagaRegistry.keys(Oxpt.RoomKeyRegistry, room_id)
          persists? = RoomRegistry.get_persists(room_id)
          %{
            room_key: room_key,
            room_id: room_id,
            persists: persists?
          }
        end
      )

    {:reply, {:ok, response}, socket}
  end

   def handle_in("join_room_as_guest", %{"roomKey" => room_key}, socket) do
    player_uuid = socket.assigns.player_uuid

    duplicated_room_key = room_key |> String.downcase() |> String.trim()
    
    data =
      case RoomKeyRegistry.get_room(duplicated_room_key) do
        nil -> 
          {:error, %{reason: :not_exist, response: %{}}}
        
        room_id ->
          guest_id =
            case GuestTokenRegistry.get(room_id, player_uuid) do
              {:ok, guest_id} ->
                guest_id
              :error ->
                case enter_room(room_id, player_uuid, host: false) do
                  nil -> :error
                  {:ok, guest_id} -> guest_id
                end
            end

          game_id = GuestRegistry.get_playing_game(guest_id)

          response = %{
            room_key: duplicated_room_key,
            room_id: room_id,
            guest_id: guest_id,
            game_id: game_id
          }

          {:ok, response}           
      end

    {:reply, data, socket}
  end

  def handle_in("join_room_as_host", %{"roomId" => room_id}, socket) do
    player_uuid = socket.assigns.player_uuid
    room_key = Cizen.SagaRegistry.keys(Oxpt.RoomKeyRegistry, room_id)

    data =
      case GuestTokenRegistry.get(room_id, {:host, player_uuid}) do
        {:ok, guest_id} ->
          game_id = GuestRegistry.get_playing_game(guest_id)

          response = %{
            room_key: room_key,
            room_id: room_id,
            guest_id: guest_id,
            game_id: game_id
          }

          {:ok, response}

        :error ->
          {:error, %{reason: :not_exist, response: %{}}}
      end

    {:reply, data, socket}
  end

  defp create_room(room_key, persists?) do
    case RoomKeyRegistry.get_room(room_key) do
      nil -> 
        {:ok, room_id} = Saga.start(%Room{persists?: persists?}, return: :saga_id)
        
        RoomRegistry.put_persists(room_id, persists?)

        case RoomKeyRegistry.register(room_key, room_id) do
          {:ok, _} -> {:ok, room_id, room_key}
          {:error, _} -> {:error, :invalid}
        end

      _ -> {:error, :already}
    end
  end 

  defp enter_room(room_id, uuid, params) do
    host? = Keyword.get(params, :host, false)
    guest_id = Room.enter(room_id, host?)

    uuid_new = if host?, do: {:host, uuid}, else: uuid
    GuestTokenRegistry.register(room_id, uuid_new, guest_id)
    :timer.sleep(500)
    case guest_id do
      nil -> {:error, :failed}
      guest_id -> {:ok, guest_id}
    end
  end

  defp get_playing_game(guest_id) do
    case GuestRegistry.get_playing_game(guest_id) do
      nil ->
        {:error, :failed}
      game_id ->
        {:ok, game_id}
    end
  end
end