defmodule OxptWeb.AccountChannel do
  use Phoenix.Channel

  alias Oxpt.RoomRegistry

  @max_age 2 * 7 * 24 * 60 * 60

  def join("account:guest", _payload, socket) do
    {:ok, socket}
  end

  def join(_topic, _params, _socket) do
    {:error, %{reason: :not_found}}
  end

  def handle_in("sign_up", %{"password" => password, "username" => username}, socket) do
    player_uuid = socket.assigns.player_uuid
    case Oxpt.Accounts.register_user(%{username: username, password: password}) do
      {:ok, user} ->
        account_token = Phoenix.Token.sign(OxptWeb.Endpoint, "account_socket", user.uuid, max_age: @max_age)
        socket =
          socket
          |> assign(:account_uuid, user.uuid)

        case Oxpt.Email.sign_up_email(player_uuid, user.uuid, username) |> Oxpt.Mailer.deliver_now(response: true) do
          {:ok, _, _} -> 

            Oxpt.GuestTokenRegistry.get_all({:host, player_uuid})
            |> Enum.each(fn room -> 
              RoomRegistry.delete_room({:host, player_uuid}, room)
              RoomRegistry.add_room({:host, user.uuid}, room)
            end)

            {:reply, {:ok, %{account_token: account_token}}, socket}

          {:error, error} -> 
            {:reply, {:error, %{error: error}}, socket}
        end

      {:error, %Ecto.Changeset{} = changeset} ->
        reason = 
          case changeset.errors do
            [username: {"has already been taken", _}] -> :username_already_taken
            [username: {"should be at least %{count} character(s)", _}] -> :username_too_short
            [username: {"should be at most %{count} character(s)", _}] -> :username_too_long
            [username: {"can't be blank", _}] -> :username_blank
            [password: {"should be at least %{count} character(s)", _}] -> :password_too_short
            [password: {"should be at most %{count} character(s)", _}] -> :password_too_long
            [password: {"can't be blank", _}] -> :password_blank
            _ -> :ecto
          end
        socket =
          socket
          |> assign(:account_uuid, nil)
        {:reply, {:error, %{reason: reason}}, socket}

      {:error, _} ->
        socket =
          socket
          |> assign(:account_uuid, nil)
        {:reply, {:error, %{reason: :register_user}}, socket}
    end
  end

  def handle_in("sign_in", %{"password" => password, "username" => username}, socket) do
    player_uuid = socket.assigns.player_uuid
    case Oxpt.Accounts.authenticate_by_username_and_pass(username, password) do
      {:ok, user} ->
        account_token = Phoenix.Token.sign(OxptWeb.Endpoint, "account_socket", user.uuid, max_age: @max_age)
        socket =
          socket
          |> assign(:account_uuid, user.uuid)

          rooms = Oxpt.GuestTokenRegistry.get_all({:host, player_uuid})
          IO.inspect(rooms)
          Oxpt.GuestTokenRegistry.get_all({:host, player_uuid})
          |> Enum.each(fn room -> 
            RoomRegistry.delete_room({:host, player_uuid}, room)
            RoomRegistry.add_room({:host, user.uuid}, room)
          end)
          rooms = Oxpt.GuestTokenRegistry.get_all({:host, user.uuid})
          IO.inspect(rooms)
        {:reply, {:ok, %{account_token: account_token}}, socket}

      {:error, reason} ->
        socket =
          socket
          |> assign(:account_uuid, nil)
        {:reply, {:error, %{reason: reason}}, socket}
    end
  end

  def handle_in("delete_account", _, socket) do
    player_uuid = socket.assigns.player_uuid
    account_uuid = socket.assigns.account_uuid
    case account_uuid do
      nil ->
        {:reply, {:error, %{reason: :account_not_found}}, socket}
      account_uuid ->
        Oxpt.Accounts.delete_user(account_uuid)
        socket =
          socket
          |> assign(:account_uuid, nil)

        case Oxpt.Email.delete_account_email(player_uuid, account_uuid) |> Oxpt.Mailer.deliver_now(response: true) do
          {:ok, _, _} -> 
            {:reply, {:ok, %{account_token: nil}}, socket}
          {:error, error} -> 
            {:reply, {:error, %{error: error}}, socket}
        end
    end
  end
end