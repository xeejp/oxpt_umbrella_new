defmodule OxptWeb.GameChannel do
  use OxptWeb, :channel

  use Cizen.Effects
  use Cizen.Effectful

  alias Cizen.CizenSagaRegistry
  alias Cizen.Filter
  alias Cizen.Dispatcher
  alias Oxpt.Game.JoinGuest
  alias Oxpt.Player.{Input, Output, Request}

  require Filter

  def join("game:" <> value, _, socket) do
    [guest_id, game_id] = value |> String.split(":")

    case CizenSagaRegistry.get_saga(game_id) do
      {:ok, %game_module{}} ->
        Dispatcher.listen(
          Filter.new(fn %Output{game_id: ^game_id, guest_id: ^guest_id} -> true end)
        )

        %JoinGuest.Joined{game_id: game_id} =
          game_module.join_guest(%JoinGuest{game_id: game_id, guest_id: guest_id})

        {:ok,
         socket
         |> assign(:guest_id, guest_id)
         |> assign(:game_id, game_id)
         |> assign(:game_module, game_module)}

      _ ->
        {:error, :invalid_game_id_when_join}
    end
  end

  def handle_in("input", %{"event" => event, "payload" => payload}, socket) do
    socket.assigns[:game_module].input(%Input{
      game_id: socket.assigns[:game_id],
      guest_id: socket.assigns[:guest_id],
      event: event,
      payload: payload
    })

    {:reply, :ok, socket}
  end

  def handle_in(
        "request",
        %{"event" => event, "payload" => payload, "timeout" => _timeout},
        socket
      ) do
    response =
      case socket.assigns[:game_module].request(%Request{
             game_id: socket.assigns[:game_id],
             guest_id: socket.assigns[:guest_id],
             event: event,
             payload: payload
           }) do
        :timeout ->
          {:timeout, %{}}

        {:ok, payload} ->
          {:ok, payload}

        {:error, payload} ->
          {:error, payload}
      end

    {:reply, response, socket}
  end

  def handle_in(_, _, socket) do
    {:reply, :error, socket}
  end

  def handle_info(%Output{event: event, payload: payload}, socket) do
    push(socket, event, payload)
    {:noreply, socket}
  end
end
