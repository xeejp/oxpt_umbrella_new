defmodule OxptWeb.ContactChannel do
  use Phoenix.Channel

  def join("contact:guest", _payload, socket) do
    {:ok, socket}
  end

  def join(_topic, _params, _socket) do
    {:error, %{reason: "not_found"}}
  end

  def handle_in("contact", %{"name" => name, "email" => email, "subject" => subject, "body" => body}, socket) do
    player_uuid = socket.assigns.player_uuid
    account_uuid = socket.assigns.account_uuid

    username =
      case account_uuid do
        nil -> nil
        account_uuid ->
          case Oxpt.Accounts.get_user_by(uuid: account_uuid) do
            nil -> nil
            user -> user.username
          end
      end

    case Oxpt.Email.contact_email(player_uuid, account_uuid, username, name, email, subject, body) |> Oxpt.Mailer.deliver_now(response: true) do
      {:ok, _, _} -> 
        {:reply, {:ok, %{response: "ok"}}, socket}
      {:error, error} -> 
        {:reply, {:error, %{error: error}}, socket}
    end
  end

  def handle_info({:delivered_email, _}, socket) do
    {:noreply, socket}
  end
end