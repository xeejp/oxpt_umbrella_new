defmodule OxptWeb.GameSocket do
  use Phoenix.Socket

  ## Channels
  channel "game:*", OxptWeb.GameChannel
  channel "guest:*", OxptWeb.GuestChannel

  @max_age 2 * 7 * 24 * 60 * 60

  def connect(_params, socket, _connect_info) do
    {:ok, socket}
  end

  def connect(_params, _socket, _connect_info), do: :error

  def id(_socket), do: nil
end