defmodule OxptWeb.GuestChannel do
  use Phoenix.Channel

  use Cizen.Effectful
  use Cizen.Effects
  alias Cizen.{Dispatcher, Filter}

  alias Oxpt.JoinGame
  alias Oxpt.GuestRegistry

  def join("guest:" <> guest_id, _params, socket) do
    socket = socket |> assign(:guest_id, guest_id)
    Dispatcher.listen(Filter.new(fn %JoinGame{guest_id: ^guest_id} -> true end))
    send(self(), :after_join)
    {:ok, socket}
  end

  def handle_info(:after_join, socket) do
    game_id = GuestRegistry.get_playing_game(socket.assigns.guest_id)

    if game_id do
      push_game_id(socket, game_id)
    else
      Process.send_after(self(), :after_join, 20)
    end

    {:noreply, socket}
  end

  def handle_info(%JoinGame{game_id: game_id}, socket) do
    push_game_id(socket, game_id)
    {:noreply, socket}
  end

  defp push_game_id(socket, game_id) do
    push(socket, "join_game", %{game: game_id})
  end
end
