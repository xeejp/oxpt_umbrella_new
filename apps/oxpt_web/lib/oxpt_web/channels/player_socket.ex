defmodule OxptWeb.PlayerSocket do
  use Phoenix.Socket

  ## Channels
  channel "room:*", OxptWeb.RoomChannel

  @max_age 2 * 7 * 24 * 60 * 60

  def connect(%{"playerToken" => player_token, "accountToken" => account_token}, socket, _connect_info) do
    player_uuid = 
      case player_token do
        nil -> 
          nil

        "" ->
          nil

        player_token ->
          case Phoenix.Token.verify(OxptWeb.Endpoint, "player_socket", player_token, max_age: @max_age) do
            {:ok, player_uuid} ->
              player_uuid

            {:error, _reason} ->
              nil
          end
      end

    account_uuid =
      case account_token do
        nil -> 
          nil

        "" ->
          nil

        account_token ->
          case Phoenix.Token.verify(OxptWeb.Endpoint, "account_socket", account_token, max_age: @max_age) do
            {:ok, account_uuid} ->
              case Oxpt.Accounts.get_user_by(uuid: account_uuid) do
                nil -> nil
                user -> user.uuid
              end
            {:error, _reason} ->
              nil
          end
      end

    case {player_uuid, account_uuid} do
      {nil, _} -> 
        :error

      {player_uuid, account_uuid} ->
        socket =
          socket
          |> assign(:player_uuid, player_uuid)
          |> assign(:account_uuid, account_uuid)

        {:ok, socket}
    end
  end

  def connect(_params, _socket, _connect_info), do: :error

  def id(_socket), do: nil
end