defmodule OxptWeb.PageController do
  use OxptWeb, :controller

  @max_age 2 * 7 * 24 * 60 * 60

  def index(conn, _params) do 
    player_token =
      case get_session(conn, :player_token) do
        nil ->
          player_uuid = Ecto.UUID.generate()
          player_token = Phoenix.Token.sign(OxptWeb.Endpoint, "player_socket", player_uuid, max_age: @max_age)
          player_token
        player_token ->
          case Phoenix.Token.verify(OxptWeb.Endpoint, "player_socket", player_token, max_age: @max_age) do
            {:ok, _player_uuid} ->
              player_token
            {:error, _} ->
              player_uuid = Ecto.UUID.generate()
              player_token = Phoenix.Token.sign(OxptWeb.Endpoint, "player_socket", player_uuid, max_age: @max_age)
              player_token
          end
      end

    {account_token, username} = 
      case get_session(conn, :account_token) do
        nil ->
          {nil, nil}
        account_token ->
          case Phoenix.Token.verify(OxptWeb.Endpoint, "account_socket", account_token, max_age: @max_age) do
            {:ok, account_uuid} ->
              case Oxpt.Accounts.get_user_by(uuid: account_uuid) do
                nil -> {nil, nil}
                user -> 
                  {account_token, user.username}
              end
                    
            {:error, _} ->
              {nil, nil}
          end
      end

    conn
    |> put_session(:player_token, player_token)
    |> put_session(:account_token, account_token)
    |> assign(:player_token, player_token)
    |> assign(:account_token, account_token)
    |> assign(:username, username)
    |> render("index.html")
  end

  def delete_player_token(conn, _params) do
    player_uuid = Ecto.UUID.generate()
    player_token = Phoenix.Token.sign(OxptWeb.Endpoint, "player_socket", player_uuid, max_age: @max_age)

    {account_token, username} = 
      case get_session(conn, :account_token) do
        nil ->
          {nil, nil}
        account_token ->
          case Phoenix.Token.verify(OxptWeb.Endpoint, "account_socket", account_token, max_age: @max_age) do
            {:ok, account_uuid} ->
              case Oxpt.Accounts.get_user_by(uuid: account_uuid) do
                nil -> {nil, nil}
                user -> 
                  {account_token, user.username}
              end
                    
            {:error, _} ->
              {nil, nil}
          end
      end

    conn
    |> put_session(:player_token, player_token)
    |> put_session(:account_token, account_token)
    |> put_session(:username, username)
    |> assign(:player_token, nil)
    |> assign(:account_token, account_token)
    |> assign(:username, username)
    |> redirect(to: "/")
  end

  def delete_account_token(conn, _params) do
    player_token =
      case get_session(conn, :player_token) do
        nil ->
          player_uuid = Ecto.UUID.generate()
          player_token = Phoenix.Token.sign(OxptWeb.Endpoint, "player_socket", player_uuid, max_age: @max_age)
          player_token
        player_token ->
          case Phoenix.Token.verify(OxptWeb.Endpoint, "player_socket", player_token, max_age: @max_age) do
            {:ok, _player_uuid} ->
              player_token
            {:error, _} ->
              player_uuid = Ecto.UUID.generate()
              player_token = Phoenix.Token.sign(OxptWeb.Endpoint, "player_socket", player_uuid, max_age: @max_age)
              player_token
          end
      end

    conn
    |> put_session(:player_token, player_token)
    |> put_session(:account_token, nil)
    |> put_session(:username, nil)
    |> assign(:player_token, player_token)
    |> assign(:account_token, nil)
    |> assign(:username, nil)
    |> redirect(to: "/")
  end

  def delete_all(conn, _params) do
    player_uuid = Ecto.UUID.generate()
    player_token = Phoenix.Token.sign(OxptWeb.Endpoint, "player_socket", player_uuid, max_age: @max_age)

    conn
    |> assign(:account_token, nil)
    |> assign(:username, nil)
    |> configure_session(drop: true)
    |> put_session(:player_token, player_token)
    |> assign(:player_token, player_token)
    |> redirect(to: "/")
  end

  def put_player_token(conn, %{"player_token" => player_token, "redirect_url" => redirect_url}) do
    player_token =
      case player_token do
        nil ->
          player_uuid = Ecto.UUID.generate()
          player_token = Phoenix.Token.sign(OxptWeb.Endpoint, "player_socket", player_uuid, max_age: @max_age)
          player_token
        player_token ->
          case Phoenix.Token.verify(OxptWeb.Endpoint, "player_socket", player_token, max_age: @max_age) do
            {:ok, _player_uuid} ->
              player_token
            {:error, _} ->
              player_uuid = Ecto.UUID.generate()
              player_token = Phoenix.Token.sign(OxptWeb.Endpoint, "player_socket", player_uuid, max_age: @max_age)
              player_token
          end
      end

    {account_token, username} = 
      case get_session(conn, :account_token) do
        nil ->
          {nil, nil}
        account_token ->
          case Phoenix.Token.verify(OxptWeb.Endpoint, "account_socket", account_token, max_age: @max_age) do
            {:ok, account_uuid} ->
              case Oxpt.Accounts.get_user_by(uuid: account_uuid) do
                nil -> {nil, nil}
                user -> 
                  {account_token, user.username}
              end
                    
            {:error, _} ->
              {nil, nil}
          end
      end

    conn
    |> put_session(:player_token, player_token)
    |> put_session(:account_token, account_token)
    |> assign(:player_token, player_token)
    |> assign(:account_token, account_token)
    |> assign(:username, username)
    |> redirect(to: "/#{redirect_url}")
  end

  def put_account_token(conn, %{"account_token" => account_token, "redirect_url" => redirect_url}) do
    player_token =
      case get_session(conn, :player_token) do
        nil ->
          player_uuid = Ecto.UUID.generate()
          player_token = Phoenix.Token.sign(OxptWeb.Endpoint, "player_socket", player_uuid, max_age: @max_age)
          player_token
        player_token ->
          case Phoenix.Token.verify(OxptWeb.Endpoint, "player_socket", player_token, max_age: @max_age) do
            {:ok, _player_uuid} ->
              player_token
            {:error, _} ->
              player_uuid = Ecto.UUID.generate()
              player_token = Phoenix.Token.sign(OxptWeb.Endpoint, "player_socket", player_uuid, max_age: @max_age)
              player_token
          end
      end

    {account_token, username} = 
      case account_token do
        nil ->
          {nil, nil}
        account_token ->
          case Phoenix.Token.verify(OxptWeb.Endpoint, "account_socket", account_token, max_age: @max_age) do
            {:ok, account_uuid} ->
              case Oxpt.Accounts.get_user_by(uuid: account_uuid) do
                nil -> {nil, nil}
                user -> 
                  {account_token, user.username}
              end
                    
            {:error, _} ->
              {nil, nil}
          end
      end

    conn
    |> put_session(:player_token, player_token)
    |> put_session(:account_token, account_token)
    |> assign(:player_token, player_token)
    |> assign(:account_token, account_token)
    |> assign(:username, username)
    |> redirect(to: "/#{redirect_url}")
  end
end
