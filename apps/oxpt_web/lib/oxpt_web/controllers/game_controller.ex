defmodule OxptWeb.GameController do
  use OxptWeb, :controller

  @max_age 2 * 7 * 24 * 60 * 60

  plug :put_layout, "game.html"

  use Cizen.Effectful
  use Cizen.Effects
  alias Cizen.CizenSagaRegistry
  alias Oxpt.GuestTokenRegistry
  alias OxptWeb.{Games, Endpoint}

  def error(conn, %{"reason" => reason} = _params) do
    render(conn, "error.json", reason: reason)
  end

  def index(conn, _params) do
    games =
      with player_token when not is_nil(player_token) <- get_session(conn, :player_token),
           {:ok, uuid} <- Phoenix.Token.verify(Endpoint, "player_socket", player_token, max_age: @max_age) do
        GuestTokenRegistry.get_all({:host, uuid})
        |> Enum.map(fn {r_id, g_id} -> {r_id, {:host, g_id}} end)
        |> Enum.concat(GuestTokenRegistry.get_all(uuid))
      else
        _ -> []
      end

    render(conn, "index.json", games: games)
  end

  def show(conn, %{"guest_id" => guest_id, "game_id" => game_id}) do
    case CizenSagaRegistry.get_saga(game_id) do
      {:ok, game} ->
        script = Games.get_game_url(game)
        conn
          |> render("index.html",
            guest_id: guest_id,
            game_id: game_id,
            script: script
          )
      _ ->
        render(conn, "error.json", reason: :invalid_game_id_when_show)
    end
  end

end