defmodule OxptWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :oxpt_web

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  @session_options [
    store: :cookie,
    key: "_oxpt_web_key",
    signing_salt: "NPCQbxE8"
  ]

  socket "/player_socket", OxptWeb.PlayerSocket,
    websocket: true,
    longpoll: false
  
  socket "/game_socket", OxptWeb.GameSocket,
    websocket: true,
    longpoll: false

  socket "/account_socket", OxptWeb.AccountSocket,
    websocket: true,
    longpoll: false

  socket "/live", Phoenix.LiveView.Socket, websocket: [connect_info: [session: @session_options]]

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :oxpt_web,
    gzip: false,
    headers: %{"Access-Control-Allow-Origin" => "*"},
    only: ~w(css fonts images js favicon.ico robots.txt games manifest.json)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
    plug Phoenix.Ecto.CheckRepoStatus, otp_app: :oxpt_web
  end

  plug Phoenix.LiveDashboard.RequestLogger,
    param_key: "request_logger",
    cookie_key: "request_logger"

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head
  plug Plug.Session, @session_options
  plug CORSPlug
  plug OxptWeb.Router
end
