defmodule OxptWeb.Games do
  def get_game_url(game) do
    {name, _} =
      Application.get_env(:oxpt_web, :games)
      |> Enum.find(fn {_name, {module, _params}} ->
        module == game.__struct__
      end)

    string_name = to_string(name)
    "/games/#{string_name}/index.js"
  end
end
