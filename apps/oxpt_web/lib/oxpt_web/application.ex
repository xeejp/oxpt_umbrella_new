defmodule OxptWeb.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      OxptWeb.Telemetry,
      # Start the Endpoint (http/https)
      OxptWeb.Endpoint
      # Start a worker by calling: OxptWeb.Worker.start_link(arg)
      # {OxptWeb.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: OxptWeb.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def start_phase(:restore_rooms, _start_type, _args) do
    Oxpt.Persistence.Restorer.run()
    :ok
  end
  
  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    OxptWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
