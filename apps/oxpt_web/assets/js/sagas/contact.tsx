import { put, takeEvery } from 'redux-saga/effects';
import { Socket } from 'phoenix';
import * as Actions from '../redux/actions';

const setErrorMessage = (errorcode) => {
  let errorMessage;
  switch (errorcode) {
    case 'not_found':
      errorMessage = 'infoSnackbar.webSocket.error.notFound';
      break;
    case 'connection_error':
      errorMessage = 'infoSnackbar.webSocket.error.connectionError';
      break;
    case 'connection_dropped':
      errorMessage = 'infoSnackbar.webSocket.error.connectionDropped';
      break;
    case 'contact_channel_error':
      errorMessage = 'infoSnackbar.webSocket.error.channelError';
      break;
    default:
      errorMessage = 'infoSnackbar.webSocket.error.default';
  }
  return errorMessage;
};

const socket = new Socket('/account_socket', {
  params: {
    playerToken: window.playerToken || '',
    accountToken: window.accountToken || '',
  },
});

function* connectAndJoin(topic, params, postfix) {
  const contactChannel = socket.channel('contact:guest');

  try {
    if (socket.isConnected() === false) {
      socket.connect();
      socket.onError(() => put(Actions.setSnackbar({
        snackbarTitle: `contact.${postfix}`,
        snackbarMessage: setErrorMessage('connection_error'),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      })));
      socket.onClose(() => put(Actions.setSnackbar({
        snackbarTitle: `contact.${postfix}`,
        snackbarMessage: setErrorMessage('connection_dropped'),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      })));
    }

    const { joinError } = yield new Promise((resolve) => {
      contactChannel.join()
        .receive('ok', () => {
          resolve({ joinStatus: 'success' });
        })
        .receive('error', (resp) => {
          resolve({ joinError: resp.reason });
        });
    });

    if (joinError) {
      yield put(Actions.setSnackbar({
        snackbarTitle: `contact.${postfix}`,
        snackbarMessage: setErrorMessage(joinError),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
    } else {
      const { ok, topicError } = yield new Promise((resolve) => {
        contactChannel.push(topic, params)
          .receive('ok', () => {
            resolve({ ok: 'ok' });
          })
          .receive('error', (resp) => {
            resolve({ topicError: resp.reason });
          });
      });

      return ({ ok, topicError });
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: `contact.${postfix}`,
      snackbarMessage: setErrorMessage('contact_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  } finally {
    contactChannel.leave().receive('ok', () => { });
  }
  return true;
}

// contactボタンが押された時
function* tryRequestContact(action) {
  const detail = {
    name: action.payload.name,
    email: action.payload.email,
    subject: action.payload.subject,
    body: action.payload.body,
  };

  try {
    const join = yield connectAndJoin('contact', detail, 'contact');
    if (typeof join.topicError !== 'undefined') {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.contact.title',
        snackbarMessage: setErrorMessage(join.topicError),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
    } else {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.contact.title',
        snackbarMessage: 'infoSnackbar.contact.success',
        snackbarSeverity: 'success',
        snackbarOpen: true,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'success' }));
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: 'infoSnackbar.contact.title',
      snackbarMessage: setErrorMessage('contact_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  }
}

function* contact() {
  yield takeEvery('REQUEST_CONTACT', tryRequestContact);
}

export default contact;
