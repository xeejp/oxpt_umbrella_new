import { put, takeEvery } from 'redux-saga/effects';
import { Socket } from 'phoenix';
import * as Actions from '../redux/actions';

const setErrorMessage = (errorcode) => {
  let errorMessage;
  switch (errorcode) {
    case 'not_found':
      errorMessage = 'infoSnackbar.webSocket.error.notFound';
      break;
    case 'connection_error':
      errorMessage = 'infoSnackbar.webSocket.error.connectionError';
      break;
    case 'connection_dropped':
      errorMessage = 'infoSnackbar.webSocket.error.connectionDropped';
      break;
    case 'account_channel_error':
      errorMessage = 'infoSnackbar.webSocket.error.channelError';
      break;
    case 'username_already_taken':
      errorMessage = 'infoSnackbar.account.signUp.error.usernameAlreadyTaken';
      break;
    case 'username_too_short':
      errorMessage = 'infoSnackbar.account.signUp.error.usernameTooShort';
      break;
    case 'username_too_long':
      errorMessage = 'infoSnackbar.account.signUp.error.usernameTooLong';
      break;
    case 'username_blank':
      errorMessage = 'infoSnackbar.account.signUp.error.usernameBlank';
      break;
    case 'password_too_short':
      errorMessage = 'infoSnackbar.account.signUp.error.passwordTooShort';
      break;
    case 'password_too_long':
      errorMessage = 'infoSnackbar.account.signUp.error.passwordTooLong';
      break;
    case 'password_blank':
      errorMessage = 'infoSnackbar.account.signUp.error.passwordBlank';
      break;
    case 'ecto':
      errorMessage = 'infoSnackbar.account.signUp.error.ecto';
      break;
    case 'register_user':
      errorMessage = 'infoSnackbar.account.signUp.error.registerUser';
      break;
    case 'user_unauthorized':
      errorMessage = 'infoSnackbar.account.signIn.error.userUnauthorized';
      break;
    case 'user_not_found':
      errorMessage = 'infoSnackbar.account.signIn.error.userNotFound';
      break;
    case 'account_not_found':
      errorMessage = 'infoSnackbar.account.deleteAccount.error.accountNotFound';
      break;
    default:
      errorMessage = 'infoSnackbar.webSocket.error.default';
  }
  return errorMessage;
};

const socket = new Socket('/account_socket', {
  params: {
    playerToken: window.playerToken || '',
    accountToken: window.accountToken || '',
  },
});

function* connectAndJoin(topic, params, component) {
  const accountChannel = socket.channel('account:guest');

  try {
    if (socket.isConnected() === false) {
      socket.connect();
      socket.onError(() => put(Actions.setSnackbar({
        snackbarTitle: `infoSnackbar.account.${component}.title`,
        snackbarMessage: setErrorMessage('connection_error'),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      })));
      socket.onClose(() => put(Actions.setSnackbar({
        snackbarTitle: `infoSnackbar.account.${component}.title`,
        snackbarMessage: setErrorMessage('connection_dropped'),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      })));
    }

    const { joinError } = yield new Promise((resolve) => {
      accountChannel.join()
        .receive('ok', () => {
          resolve({ joinStatus: 'success' });
        })
        .receive('error', (resp) => {
          resolve({ joinError: resp.reason });
        });
    });

    if (joinError) {
      yield put(Actions.setSnackbar({
        snackbarTitle: `infoSnackbar.account.${component}.title`,
        snackbarMessage: setErrorMessage(joinError),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
    } else {
      const { accountToken, topicError } = yield new Promise((resolve) => {
        accountChannel.push(topic, params)
          .receive('ok', (resp) => {
            resolve({ accountToken: resp.account_token });
          })
          .receive('error', (resp) => {
            resolve({ topicError: resp.reason });
          });
      });

      return ({ accountToken, topicError });
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: `infoSnackbar.account.${component}.title`,
      snackbarMessage: setErrorMessage('account_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  } finally {
    accountChannel.leave().receive('ok', () => { });
  }
  return true;
}

// signUpボタンが押された時
function* tryRequestSignUp(action) {
  const user = {
    username: action.payload.username,
    password: action.payload.password,
  };

  try {
    const join = yield connectAndJoin('sign_up', user, 'signUp');
    if (typeof join.accountToken === 'undefined' || join.accountToken === '') {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.account.signUp.title',
        snackbarMessage: setErrorMessage(join.topicError),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
    } else {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.account.signUp.title',
        snackbarMessage: 'infoSnackbar.account.signUp.success',
        snackbarSeverity: 'success',
        snackbarOpen: true,
      }));
      yield put(Actions.setAccountToken({ accountToken: join.accountToken }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'success' }));
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: 'infoSnackbar.account.signUp.title',
      snackbarMessage: setErrorMessage('account_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  }
}

// signInボタンが押された時
function* tryRequestSignIn(action) {
  const user = {
    username: action.payload.username,
    password: action.payload.password,
  };

  try {
    const join = yield connectAndJoin('sign_in', user, 'signIn');
    if (typeof join.accountToken === 'undefined' || join.accountToken === '') {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.account.signIn.title',
        snackbarMessage: setErrorMessage(join.topicError),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
    } else {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.account.signIn.title',
        snackbarMessage: 'infoSnackbar.account.signIn.success',
        snackbarSeverity: 'success',
        snackbarOpen: true,
      }));
      yield put(Actions.setAccountToken({ accountToken: join.accountToken }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'success' }));
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: 'infoSnackbar.account.signIn.title',
      snackbarMessage: setErrorMessage('account_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  }
}

// deleteボタンが押された時
function* tryRequestDeleteAccount() {
  try {
    const join = yield connectAndJoin('delete_account', null, 'deleteAccount');
    if (typeof join.topicError !== 'undefined') {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.account.deleteAccount.title',
        snackbarMessage: setErrorMessage(join.topicError),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
    } else {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.account.deleteAccount.title',
        snackbarMessage: 'infoSnackbar.account.deleteAccount.success',
        snackbarSeverity: 'success',
        snackbarOpen: true,
      }));
      yield put(Actions.setAccountToken({ accountToken: '' }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'success' }));
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: 'infoSnackbar.account.deleteAccount.title',
      snackbarMessage: setErrorMessage('account_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  }
}

function* account() {
  yield takeEvery('REQUEST_SIGN_UP', tryRequestSignUp);
  yield takeEvery('REQUEST_SIGN_IN', tryRequestSignIn);
  yield takeEvery('REQUEST_DELETE_ACCOUNT', tryRequestDeleteAccount);
}

export default account;
