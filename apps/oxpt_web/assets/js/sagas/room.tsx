import { put, takeEvery } from 'redux-saga/effects';
import { Socket } from 'phoenix';
import * as Actions from '../redux/actions';

const setErrorMessage = (errorcode) => {
  let errorMessage;
  switch (errorcode) {
    case 'not_found':
      errorMessage = 'infoSnackbar.webSocket.error.notFound';
      break;
    case 'connection_error':
      errorMessage = 'infoSnackbar.webSocket.error.connectionError';
      break;
    case 'connection_dropped':
      errorMessage = 'infoSnackbar.webSocket.error.connectionDropped';
      break;
    case 'room_channel_error':
      errorMessage = 'infoSnackbar.webSocket.error.channelError';
      break;
    case 'get_game_id_failed_createRoom':
      errorMessage = 'infoSnackbar.createRoom.error.getGameIdFailed';
      break;
    case 'enter_room_failed_createRoom':
      errorMessage = 'infoSnackbar.createRoom.error.enterRoomFailed';
      break;
    case 'already_exist_createRoom':
      errorMessage = 'infoSnackbar.createRoom.error.alreadyExist';
      break;
    case 'invalid_request_createRoom':
      errorMessage = 'infoSnackbar.createRoom.error.invalidRequest';
      break;
    case 'unknown_error_createRoom':
      errorMessage = 'infoSnackbar.createRoom.error.unknownError';
      break;
    case 'not_exist_joinRoomAsGuest':
      errorMessage = 'infoSnackbar.joinRoomAsGuest.error.notExist';
      break;
    case 'not_exist_joinRoomAsHost':
      errorMessage = 'infoSnackbar.joinRoomAsHost.error.notExist';
      break;
    default:
      errorMessage = 'infoSnackbar.webSocket.error.default';
      break;
  }
  return errorMessage;
};

const socket = new Socket('/player_socket', {
  params: {
    playerToken: window.playerToken || '',
    accountToken: window.accountToken || '',
  },
});

function* connectAndJoinHost(topic, params, component) {
  const roomChannel = socket.channel('room:host');

  try {
    if (socket.isConnected() === false) {
      socket.connect();
      socket.onError(() => put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.webSocket.title',
        snackbarMessage: setErrorMessage('connection_error'),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      })));
      socket.onClose(() => put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.webSocket.title',
        snackbarMessage: setErrorMessage('connection_dropped'),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      })));
    }

    const { joinError } = yield new Promise((resolve) => {
      roomChannel.join()
        .receive('ok', () => {
          resolve({ joinStatus: 'success' });
        })
        .receive('error', (resp) => {
          resolve({ joinError: resp.reason });
        });
    });

    if (joinError) {
      yield put(Actions.setSnackbar({
        snackbarTitle: `infoSnackbar.${component}.title`,
        snackbarMessage: setErrorMessage(`${joinError}_${component}`),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
    }
    const { topicStatus, topicError } = yield new Promise((resolve) => {
      roomChannel.push(topic, params)
        .receive('ok', (resp) => {
          resolve({ topicStatus: resp });
        })
        .receive('error', (resp) => {
          resolve({ topicError: resp.reason });
        });
    });

    return ({ topicStatus, topicError });
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: 'infoSnackbar.webSocket.title',
      snackbarMessage: setErrorMessage('room_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  }
  return true;
}

function* connectAndJoinGuest(topic, params, component) {
  const roomChannel = socket.channel('room:guest');

  try {
    if (socket.isConnected() === false) {
      socket.connect();
      socket.onError(() => put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.webSocket.title',
        snackbarMessage: setErrorMessage('connection_error'),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      })));
      socket.onClose(() => put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.webSocket.title',
        snackbarMessage: setErrorMessage('connection_dropped'),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      })));
    }

    const { joinError } = yield new Promise((resolve) => {
      roomChannel.join()
        .receive('ok', () => {
          resolve({ joinStatus: 'success' });
        })
        .receive('error', (resp) => {
          resolve({ joinError: resp.reason });
        });
    });

    if (joinError) {
      yield put(Actions.setSnackbar({
        snackbarTitle: `infoSnackbar.${component}.title`,
        snackbarMessage: setErrorMessage(`${joinError}_${component}`),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
    } else {
      const { topicStatus, topicError } = yield new Promise((resolve) => {
        roomChannel.push(topic, params)
          .receive('ok', (resp) => {
            resolve({ topicStatus: resp });
          })
          .receive('error', (resp) => {
            resolve({ topicError: resp.reason });
          });
      });

      return ({ topicStatus, topicError });
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: 'infoSnackbar.webSocket.title',
      snackbarMessage: setErrorMessage('room_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  }
  return true;
}

// create_roomボタンが押された時
function* tryRequestCreateRoom(action) {
  const detail = {
    roomKey: action.payload.roomKey,
    persists: action.payload.persists,
  };
  try {
    const join = yield connectAndJoinHost('create_room', detail, 'createRoom');
    if (typeof join.topicError !== 'undefined') {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.createRoom.title',
        snackbarMessage: setErrorMessage(`${join.topicError}_createRoom`),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
      yield put(Actions.setGameInfo({
        gameId: '',
        guestId: '',
      }));
    } else {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.createRoom.title',
        snackbarMessage: 'infoSnackbar.createRoom.success',
        snackbarSeverity: 'success',
        snackbarOpen: true,
      }));
      yield put(Actions.setGameInfo({
        gameId: join.topicStatus.game_id,
        guestId: join.topicStatus.guest_id,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'success' }));
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: 'infoSnackbar.createRoom.title',
      snackbarMessage: setErrorMessage('room_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  }
}

// request_host_room_listボタンが押された時
function* tryRequestHostRoomList() {
  try {
    const join = yield connectAndJoinHost('get_host_room_list', null, 'getHostRoomList');
    if (typeof join.topicError !== 'undefined') {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.getHostRoomList.title',
        snackbarMessage: setErrorMessage(`${join.topicError}_getHostRoomList`),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
    } else {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.getHostRoomList.title',
        snackbarMessage: 'infoSnackbar.getHostRoomList.success',
        snackbarSeverity: 'success',
        snackbarOpen: true,
      }));
      yield put(Actions.setHostRoomList({ hostRoomList: join.topicStatus }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'success' }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: 'infoSnackbar.getHostRoomList.title',
      snackbarMessage: setErrorMessage('room_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  }
}

// request_guest_room_listボタンが押された時
function* tryRequestGuestRoomList() {
  try {
    const join = yield connectAndJoinGuest('get_guest_room_list', null, 'getGuestRoomList');
    if (typeof join.topicError !== 'undefined') {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.getGuestRoomList.title',
        snackbarMessage: setErrorMessage(`${join.topicError}_getGuestRoomList`),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
    } else {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.getGuestRoomList.title',
        snackbarMessage: 'infoSnackbar.getGuestRoomList.success',
        snackbarSeverity: 'success',
        snackbarOpen: true,
      }));
      yield put(Actions.setGuestRoomList({ guestRoomList: join.topicStatus }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'success' }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: 'infoSnackbar.getGuestRoomList.title',
      snackbarMessage: setErrorMessage('room_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  }
}

// join_room_as_hostボタンが押された時
function* tryJoinRoomAsHost(action) {
  const detail = {
    roomId: action.payload.roomId,
  };
  try {
    const join = yield connectAndJoinHost('join_room_as_host', detail, 'joinRoomAsHost');
    if (typeof join.topicError !== 'undefined') {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.joinRoomAsHost.title',
        snackbarMessage: setErrorMessage(`${join.topicError}_joinRoomAsHost`),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
      yield put(Actions.setGameInfo({
        gameId: '',
        guestId: '',
      }));
    } else {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.joinRoomAsHost.title',
        snackbarMessage: 'infoSnackbar.joinRoomAsHost.success',
        snackbarSeverity: 'success',
        snackbarOpen: true,
      }));
      yield put(Actions.setGameInfo({
        gameId: join.topicStatus.game_id,
        guestId: join.topicStatus.guest_id,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'success' }));
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: 'infoSnackbar.joinRoomAsHost.title',
      snackbarMessage: setErrorMessage('room_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  }
}

// join_room_as_guestボタンが押された時
function* tryJoinRoomAsGuest(action) {
  const detail = {
    roomKey: action.payload.roomKey,
  };
  try {
    const join = yield connectAndJoinGuest('join_room_as_guest', detail, 'joinRoomAsGuest');
    if (typeof join.topicError !== 'undefined') {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.joinRoomAsGuest.title',
        snackbarMessage: setErrorMessage(`${join.topicError}_joinRoomAsGuest`),
        snackbarSeverity: 'error',
        snackbarOpen: true,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'error' }));
      yield put(Actions.setInputDisabled({ inputDisabled: false }));
      yield put(Actions.setGameInfo({
        gameId: '',
        guestId: '',
      }));
    } else {
      yield put(Actions.setSnackbar({
        snackbarTitle: 'infoSnackbar.joinRoomAsGuest.title',
        snackbarMessage: 'infoSnackbar.joinRoomAsGuest.success',
        snackbarSeverity: 'success',
        snackbarOpen: true,
      }));
      yield put(Actions.setGameInfo({
        gameId: join.topicStatus.game_id,
        guestId: join.topicStatus.guest_id,
      }));
      yield put(Actions.setNetworkStatus({ networkStatus: 'success' }));
    }
  } catch (e) {
    put(Actions.setSnackbar({
      snackbarTitle: 'infoSnackbar.joinRoomAsGuest.title',
      snackbarMessage: setErrorMessage('room_channel_error'),
      snackbarSeverity: 'error',
      snackbarOpen: true,
    }));
  }
}

function* room() {
  yield takeEvery('REQUEST_CREATE_ROOM', tryRequestCreateRoom);
  yield takeEvery('REQUEST_HOST_ROOM_LIST', tryRequestHostRoomList);
  yield takeEvery('REQUEST_GUEST_ROOM_LIST', tryRequestGuestRoomList);
  yield takeEvery('JOIN_ROOM_AS_HOST', tryJoinRoomAsHost);
  yield takeEvery('JOIN_ROOM_AS_GUEST', tryJoinRoomAsGuest);
}

export default room;
