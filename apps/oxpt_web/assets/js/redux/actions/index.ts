import { createActions, handleActions } from 'redux-actions';

const defaultState = {
  username: null,
  password: null,
  snackbarTitle: '',
  snackbarMessage: '',
  snackbarSeverity: 'info',
  snackbarOpen: false,
  menuOpen: false,
  inputDisabled: false,
  networkStatus: 'default',
  callback: null,
  name: '',
  email: '',
  subject: '',
  body: '',
  roomKey: '',
  persists: false,
  gameId: '',
  guestId: '',
  hostRoomList: [],
  guestRoomList: [],
  accountToken: '',
};

export const {
  requestSignUp,
  requestSignIn,
  requestDeleteAccount,
  requestContact,
  requestCreateRoom,
  setSnackbar,
  setMenuOpen,
  setInputDisabled,
  setNetworkStatus,
  setGameInfo,
  requestHostRoomList,
  setHostRoomList,
  joinRoomAsHost,
  requestGuestRoomList,
  setGuestRoomList,
  joinRoomAsGuest,
  setAccountToken,
} = createActions({
  REQUEST_SIGN_UP: ({
    username,
    password,
  }) => ({
    username,
    password,
  }),
  REQUEST_SIGN_IN: ({
    username,
    password,
  }) => ({
    username,
    password,
  }),
  REQUEST_DELETE_ACCOUNT: () => ({}),
  REQUEST_CONTACT: ({
    name, email, subject, body,
  }) => ({
    name,
    email,
    subject,
    body,
  }),
  REQUEST_CREATE_ROOM: ({
    roomKey,
    persists,
  }) => ({
    roomKey,
    persists,
  }),
  SET_SNACKBAR: ({
    snackbarTitle,
    snackbarMessage,
    snackbarSeverity,
    snackbarOpen,
  }) => ({
    snackbarTitle,
    snackbarMessage,
    snackbarSeverity,
    snackbarOpen,
  }),
  SET_MENU_OPEN: ({ menuOpen }) => ({ menuOpen }),
  SET_INPUT_DISABLED: ({ inputDisabled }) => ({ inputDisabled }),
  SET_NETWORK_STATUS: ({ networkStatus }) => ({ networkStatus }),
  SET_GAME_INFO: ({
    gameId,
    guestId,
  }) => ({
    gameId,
    guestId,
  }),
  SET_MAIN_HEIGHT: ({ mainHeight }) => ({ mainHeight }),
  REQUEST_HOST_ROOM_LIST: () => ({}),
  SET_HOST_ROOM_LIST: ({ hostRoomList }) => ({ hostRoomList }),
  JOIN_ROOM_AS_HOST: ({ roomId }) => ({ roomId }),
  REQUEST_GUEST_ROOM_LIST: () => ({}),
  SET_GUEST_ROOM_LIST: ({ guestRoomList }) => ({ guestRoomList }),
  JOIN_ROOM_AS_GUEST: ({ roomKey }) => ({ roomKey }),
  SET_ACCOUNT_TOKEN: ({ accountToken }) => ({ accountToken }),
});

export const reducer = handleActions({
  REQUEST_SIGN_UP: (state, action) => ({
    ...state,
    username: action.payload.username,
    password: action.payload.password,
  }),
  REQUEST_SIGN_IN: (state, action) => ({
    ...state,
    username: action.payload.username,
    password: action.payload.password,
  }),
  REQUEST_DELETE_ACCOUNT: (state) => ({ ...state }),
  REQUEST_CONTACT: (state, action) => ({
    ...state,
    name: action.payload.name,
    email: action.payload.email,
    subject: action.payload.subject,
    body: action.payload.body,
  }),
  REQUEST_CREATE_ROOM: (state, action) => ({
    ...state,
    roomKey: action.payload.roomKey,
    persists: action.payload.persists,
  }),
  SET_SNACKBAR: (state, action) => ({
    ...state,
    snackbarTitle: action.payload.snackbarTitle,
    snackbarMessage: action.payload.snackbarMessage,
    snackbarSeverity: action.payload.snackbarSeverity,
    snackbarOpen: action.payload.snackbarOpen,
  }),
  SET_MENU_OPEN: (state, action) => ({
    ...state,
    menuOpen: action.payload.menuOpen,
  }),
  SET_INPUT_DISABLED: (state, action) => ({
    ...state,
    inputDisabled: action.payload.inputDisabled,
  }),
  SET_NETWORK_STATUS: (state, action) => ({
    ...state,
    networkStatus: action.payload.networkStatus,
  }),
  SET_GAME_INFO: (state, action) => ({
    ...state,
    gameId: action.payload.gameId,
    guestId: action.payload.guestId,
  }),
  REQUEST_HOST_ROOM_LIST: (state) => ({ ...state }),
  SET_HOST_ROOM_LIST: (state, action) => ({
    ...state,
    hostRoomList: action.payload.hostRoomList,
  }),
  JOIN_ROOM_AS_HOST: (state, action) => ({
    ...state,
    roomId: action.payload.roomId,
  }),
  REQUEST_GUEST_ROOM_LIST: (state) => ({ ...state }),
  SET_GUEST_ROOM_LIST: (state, action) => ({
    ...state,
    guestRoomList: action.payload.guestRoomList,
  }),
  JOIN_ROOM_AS_GUEST: (state, action) => ({
    ...state,
    roomKey: action.payload.roomKey,
  }),
  SET_ACCOUNT_TOKEN: (state, action) => ({
    ...state,
    accountToken: action.payload.accountToken,
  }),
}, defaultState);
