import { combineReducers } from 'redux';
import { reducer } from '../actions/index';

const rootReducer = combineReducers({ reducer });

export default rootReducer;
