import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { all, fork } from 'redux-saga/effects';
import rootReducer from './reducers';

import account from '../sagas/account';
import contact from '../sagas/contact';
import room from '../sagas/room';

const SagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(SagaMiddleware),
));

function* rootSaga() {
  yield all([
    fork(account),
    fork(contact),
    fork(room),
  ]);
}

SagaMiddleware.run(
  rootSaga,
);

export default store;
