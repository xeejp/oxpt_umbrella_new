import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SnackbarInfo from '../components/SnackbarInfo';
import * as Actions from '../redux/actions';

const mapStateToProps = ({ reducer }) => ({
  snackbarTitle: reducer.snackbarTitle,
  snackbarMessage: reducer.snackbarMessage,
  snackbarSeverity: reducer.snackbarSeverity,
  snackbarOpen: reducer.snackbarOpen,
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators(Actions, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(SnackbarInfo);
