import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import RoomManagement from '../components/RoomManagement';
import * as Actions from '../redux/actions';

const mapStateToProps = ({ reducer }) => ({
  hostRoomList: reducer.hostRoomList,
  networkStatus: reducer.networkStatus,
  inputDisabled: reducer.inputDisabled,
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators(Actions, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(RoomManagement);
