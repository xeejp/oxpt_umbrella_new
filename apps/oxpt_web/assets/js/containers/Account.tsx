import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Account from '../components/Account';
import * as Actions from '../redux/actions';

const mapStateToProps = ({ reducer }) => ({
  inputDisabled: reducer.inputDisabled,
  networkStatus: reducer.networkStatus,
  accountToken: reducer.accountToken,
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators(Actions, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Account);
