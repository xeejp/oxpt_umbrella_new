import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import App from '../components/App';
import * as Actions from '../redux/actions';

const mapStateToProps = ({ reducer }) => ({
  menuOpen: reducer.menuOpen,
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators(Actions, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(App);
