import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Top from '../components/Top';
import * as Actions from '../redux/actions';

const mapStateToProps = ({ reducer }) => ({
  networkStatus: reducer.networkStatus,
  inputDisabled: reducer.inputDisabled,
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators(Actions, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Top);
