import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import RoomList from '../components/RoomList';
import * as Actions from '../redux/actions';

const mapStateToProps = ({ reducer }) => ({
  guestRoomList: reducer.guestRoomList,
  networkStatus: reducer.networkStatus,
  inputDisabled: reducer.inputDisabled,
  gameId: reducer.gameId,
  guestId: reducer.guestId,
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators(Actions, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(RoomList);
