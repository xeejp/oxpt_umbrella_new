import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import CreateRoom from '../components/CreateRoom';
import * as Actions from '../redux/actions';

const mapStateToProps = ({ reducer }) => ({
  inputDisabled: reducer.inputDisabled,
  networkStatus: reducer.networkStatus,
  gameId: reducer.gameId,
  guestId: reducer.guestId,
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators(Actions, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(CreateRoom);
