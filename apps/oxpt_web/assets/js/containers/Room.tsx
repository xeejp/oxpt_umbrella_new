import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Room from '../components/Room';
import * as Actions from '../redux/actions';

const mapStateToProps = ({ reducer }) => ({
  gameId: reducer.gameId,
  guestId: reducer.guestId,
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators(Actions, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Room);
