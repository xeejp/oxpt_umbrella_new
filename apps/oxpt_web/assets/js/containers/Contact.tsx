import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Contact from '../components/Contact';
import * as Actions from '../redux/actions';

const mapStateToProps = ({ reducer }) => ({
  inputDisabled: reducer.inputDisabled,
  networkStatus: reducer.networkStatus,
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators(Actions, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Contact);
