export const jaTermsOfService = [
  {
    title: 'Cookie利用方針',
    subSession: [
      {
        subtitle: '',
        content: [
          'XEEサイトを正常に動作させるために、私たちはあなたのデバイスにcookieと呼ばれる小さなデータを保存することがあります。',
        ],
      },
    ],
  },
  {
    title: 'Cookieとは何か?',
    subSession: [
      {
        subtitle: '',
        content: [
          'Cookieとは小さなテキストファイルで、ウェブサイトを訪れたときにあなたのコンピューターやモバイル・デバイスに保存されます。',
          '<ul><li>ファースト・パーティーCookieは当サイトが設定するCookieです。当サイトだけがその内容を読み取ることができます。加えて、ウェブサイトは潜在的に外部サービスを利用することがあり、それらのサービスでもサード・パーティーCookieと呼ばれるCookieを設定することがあります。</li><li>持続的なCookieはあなたのコンピューターに保存され、あなたがブラウザを閉じたときでも自動的に削除されないCookieです。セッションCookieはあなたがブラウザを閉じたときに削除されるCookieです。</li></ul>',
          'あなたがこのサイトを訪れるたびに、あなたはCookieを受け入れるか拒否するかを選択するように促されます。',
          'その目的は、このサイトがあなたの好み(例えばユーザ名、言語など)を覚えておくためです。',
          'この方法により、あなたはこのサイトを利用する際に何度も同じ情報を入力させられることを避けることができます。',
          'Cookieはこのサイトの利便性を向上させるために匿名の統計資料として利用されることもあります。',
        ],
      },
    ],
  },
  {
    title: 'Cookieはどのように使われるのか?',
    subSession: [
      {
        subtitle: '',
        content: [
          'XEEサイトは主にファースト・パーティーCookieを利用します。これらのCookieはXEEプロジェクトチームによって設定され、管理され、どの外部組織にも漏洩することはありません。',
          'しかし、いくつかのページを利用するためには、外部組織のCookieを受け入れる必要があります。',
        ],
      },
    ],
  },
  {
    title: 'ファースト・パーティーCookie',
    subSession: [
      {
        subtitle: '必須 (3)',
        content: [
          'これらのCookieは、XEEが機能するために必要であり、システムで無効にすることはできません。これらは通常、ログインやXEEのフォームへの入力など、情報またはサービスの要求に相当するユーザーのアクションに応じてのみ設定されます。これらのCookieをブロックまたは警告するようにブラウザを設定できますが、Webサイトの一部が機能しなくなります。これらのCookieには、個人を特定できる情報は保存されません。',
          '<div style="padding: 8px; text-align: justify; text-indent: 0; font-size: 50%;"><table border="1" style="border-collapse: collapse;" align="center"><tr><th style="padding: 8px;">名前</th><th style="padding: 8px;">プロバイダー</th><th style="padding: 8px;">目的</th><th style="padding: 8px;">有効期限</th><th style="padding: 8px;">種類</th></tr><tr><td style="padding: 8px;">CookieConsent</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">ユーザーのCookie内容を現在のドメインに保存します。</td><td style="padding: 8px;">1年間</td><td style="padding: 8px;">HTTP</td></tr><tr><td style="padding: 8px;">persist:root</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">このCookieは、訪問者が「ログイン状態を維持する」ボタンを受け入れた場合に、訪問者が再入力時にログイン状態を維持できるように、訪問者の資格情報を暗号化されたCookieに保存します。</td><td style="padding: 8px;">永久</td><td style="padding: 8px;">HTML</td></tr><tr><td style="padding: 8px;">_oxpt_web_key</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">システムはこのCookieを使用して、訪問者に一意のシンボルを提供し、ページの遷移後も同じ訪問者であることを認識します。シンボルには個人情報は含まれていません。</td><td style="padding: 8px;">セッション</td><td style="padding: 8px;">HTTP</td></tr></table></div>',
        ],
      },
      {
        subtitle: '選好 (1)',
        content: [
          'これらのCookieは、訪問者がXEEをどのように使用するかに関する情報（たとえば、訪問数、訪問者が最も頻繁にアクセスするページ、およびWebページからエラーメッセージを受け取ったかどうか）を収集します。これらのCookieは、訪問者を識別する情報を収集しません。これらのCookieを使用すると、訪問数とトラフィック資源をカウントできるため、サイトのパフォーマンスを測定して改善できます。これらのCookieを許可しない場合は、いつXEEにアクセスしたかがわからないため、そのパフォーマンスを監視できません。',
          '<div style="padding: 8px; text-align: justify; text-indent: 0; font-size: 50%;"><table border="1" style="border-collapse: collapse;" align="center"><tr><th style="padding: 8px;">名前</th><th style="padding: 8px;">プロバイダー</th><th style="padding: 8px;">目的</th><th style="padding: 8px;">有効期限</th><th style="padding: 8px;">種類</th></tr><tr><td style="padding: 8px;">i18nextLng</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">訪問者の優先言語を決定します。訪問者の再入時にウェブサイトが優先言語を設定できるようにします。</td><td style="padding: 8px;">永久</td><td style="padding: 8px;">HTTP</td></tr></table></div>',
        ],
      },
    ],
  },
  {
    title: 'サード・パーティーのCookie利用',
    subSession: [
      {
        subtitle: '',
        content: [
          'XEEのいくつかのページでは外部プロバイダーのコンテンツ(例えばYouTube、Facebook、Twitterなど)を表示します。',
          'これらのサード・パーティーコンテンツを見るためには、あなたは最初に彼らの設定する規約と条件を受け入れる必要があります。これにはCookie方針も含まれ、XEEプロジェクトチームはそれを管理することはできません。',
          'しかし、あなたがそのコンテンツを見なければ、サード・パーティーのCookieはあなたのデバイスにインストールされません。',
        ],
      },
      {
        subtitle: 'XEEサイトにおけるサード・パーティー・プロバイダー',
        content: [
          '<ul><li><a href="https://www.youtube.com/t/terms">YouTube</a></li><li><a href="https://twitter.com/en/tos#intlTerms">Twitter</a></li><li><a href="https://www.facebook.com/legal/terms">Facebook</a></li><li><a href="https://about.gitlab.com/privacy/cookies/">GitLab</a></li></ul>',
          'これらのサード・パーティー・サービスはXEEプロジェクトチームの管理外です。プロバイダーは、いつでも、彼らのサービス規約を変更し、Cookieの利用目的なども変更するかもしれません。',
        ],
      },
    ],
  },
  {
    title: 'Cookiesの管理方法は?',
    subSession: [
      {
        subtitle: '',
        content: [
          'あなたはあなたの望むようにCookieを管理したり削除したりできます。詳細は<a href="https://www.aboutcookies.org/">aboutcookies.org</a>をご覧ください。',
        ],
      },
      {
        subtitle: 'あなたのデバイスからCookieを削除する',
        content: [
          'あなたはブラウザの履歴を削除することで、あなたのデバイスにすでに存在するCookieを削除することができます。この操作はあなたがこれまでに訪問した全てのサイトのCookieを削除します。',
          '保存されている情報(例えばログイン情報、サイトの設定など)が失われるかもしれないことにご留意ください。',
        ],
      },
      {
        subtitle: '当サイトのCookieを管理する',
        content: [
          '特定のサイトのCookieを管理する方法の詳細は、あなたの利用するブラウザのプライバシーとCookieの設定をご確認ください。',
        ],
      },
      {
        subtitle: 'Cookieをブロックする',
        content: [
          'モダン・ブラウザでは、あなたのデバイスにCookieを持続的に保存するか設定することができますが、あなたは自分でそれぞれのページやサイトのCookie設定を調整する必要があるかもしれません。そしてサービスによっては提供する機能が利用できなくなることがあります(たとえばログインなど)。',
        ],
      },
      {
        subtitle: 'Cookie情報を更新する',
        content: [
          'サーバーに保存されているあなたの追跡記号(UUID)をこのデバイスから削除し、新しい追跡記号(UUID)に更新するには、<a href="/delete_all">UUID削除</a>を訪問してください。デバイスに保存されているCookie情報が削除され、新しい追跡記号(UUID)が割り当てられます。',
          '追跡記号(UUID)を削除すると、あなたはそれまでに参加していたルームやゲームの情報へアクセスできなくなります。',
        ],
      },
    ],
  },
  {
    title: '連絡先',
    subSession: [
      {
        subtitle: '',
        content: [
          'ご不明な点がありましたら、<a href="/contact">お問い合わせ</a>からXEEプロジェクトチームまでご連絡ください。',
        ],
      },
    ],
  },
];

export const enTermsOfService = [
  {
    title: 'Cookies policy',
    subSession: [
      {
        subtitle: '',
        content: [
          'We store in your device small text files called cookies to ensure the proper functioning of XEE websites.',
        ],
      },
    ],
  },
  {
    title: 'What are cookies?',
    subSession: [
      {
        subtitle: '',
        content: [
          'A cookie is a small text file that a website stores on your computer or mobile device when you visit it.',
          '<ul><li>First party cookies are cookies set by the website you’re visiting. Only that website can read them. In addition, a website might potentially use external services, which also set their own cookies, known as third-party cookies.</li><li>Persistent cookies are cookies saved on your computer and that are not deleted automatically when you close your browser, unlike session cookies, which get deleted when you close it.</li></ul>',
          'Every time you visit this website, you will be prompted to accept or refuse cookies.',
          'The purpose is to enable the site to remember your preferences (such as user name, language, etc.) for a certain period of time.',
          'That way, you don’t have to re-enter them when browsing around the site during the same visit.',
          'Cookies can also be used to obtain anonymised statistics about your browsing experience on our sites for the purpose of making improvements.',
        ],
      },
    ],
  },
  {
    title: 'How do we use cookies?',
    subSession: [
      {
        subtitle: '',
        content: [
          'XEE websites mostly use first party cookies. These are cookies set and managed by the XEE project team, and are not shared with any external organization.',
          'However, you are required to accept cookies from external organisations to view some of our pages.',
        ],
      },
    ],
  },
  {
    title: 'First-party cookies',
    subSession: [
      {
        subtitle: 'Required (3)',
        content: [
          'These cookies are necessary for the Website to function and cannot be turned off. They are usually only set in response to actions made by you which amount to a request for information or services, such as logging in or filling in forms on our Website. You can set your browser to block or alert you about these cookies, but some parts of the Website may not work properly. These cookies do not store any information that could be used reveal your identity.',
          '<div style="padding: 8px; text-align: justify; text-indent: 0; font-size: 50%;"><table border="1" style="border-collapse: collapse;" align="center"><tr><th style="padding: 8px;">Name</th><th style="padding: 8px;">Provider</th><th style="padding: 8px;">Purpose</th><th style="padding: 8px;">Expiry</th><th style="padding: 8px;">Type</th></tr><tr><td style="padding: 8px;">CookieConsent</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">Stores the user\'s cookie consent state for the current domain</td><td style="padding: 8px;">1 year</td><td style="padding: 8px;">HTTP</td></tr><tr><td style="padding: 8px;">persist:root</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">This cookie stores encrypted credentials. It allows the visitor to stay logged in on reentry if the visitor uses the \'stay logged in\' setting.</td><td style="padding: 8px;">Persistent</td><td style="padding: 8px;">HTML</td></tr><tr><td style="padding: 8px;">_oxpt_web_key</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">This cookie is used to uniquely identify visitors even after page transitions, and does not contain any personal information.</td><td style="padding: 8px;">Session</td><td style="padding: 8px;">HTTP</td></tr></table></div>',
        ],
      },
      {
        subtitle: 'Optional (1)',
        content: [
          'These cookies collect usage metrics about our website, such as the number of visits, pages that are viewed most often and information about error messages. These metrics are used for measuring and improving the performance of our website, and do not contain any personal information of the visitor. By disabling them you prevent us from making such improvements.',
          '<div style="padding: 8px; text-align: justify; text-indent: 0; font-size: 50%;"><table border="1" style="border-collapse: collapse;" align="center"><tr><th style="padding: 8px;">Name</th><th style="padding: 8px;">Provider</th><th style="padding: 8px;">Purpose</th><th style="padding: 8px;">Expiry</th><th style="padding: 8px;">Type</th></tr><tr><td style="padding: 8px;">i18nextLng</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">Stores the visitor\'s preferred language so that the website can automatically set it for you when you come back.</td><td style="padding: 8px;">Persistent</td><td style="padding: 8px;">HTTP</td></tr></table></div>',
        ],
      },
    ],
  },
  {
    title: 'Usage of Third-party cookies',
    subSession: [
      {
        subtitle: '',
        content: [
          'Some of our pages display content from external providers, e.g. YouTube, Facebook and Twitter.',
          'To view this third-party content, you first have to accept their specific terms and conditions. This includes their cookie policies, which we have no control of.',
          'No third-party cookies are installed on your device if you do not view this content.',
        ],
      },
      {
        subtitle: 'Third-party providers on XEE websites',
        content: [
          '<ul><li><a href="https://www.youtube.com/t/terms">YouTube</a></li><li><a href="https://twitter.com/en/tos#intlTerms">Twitter</a></li><li><a href="https://www.facebook.com/legal/terms">Facebook</a></li><li><a href="https://about.gitlab.com/privacy/cookies/">GitLab</a></li></ul>',
          'These third-party services are outside of the control of the XEE project team. Providers may, at any time, change their terms of service, purpose and use of cookies, etc.',
        ],
      },
    ],
  },
  {
    title: 'How can you manage cookies?',
    subSession: [
      {
        subtitle: '',
        content: [
          'You can manage/delete cookies as you wish. See <a href="https://www.aboutcookies.org/">aboutcookies.org</a> for details.',
        ],
      },
      {
        subtitle: 'Removing cookies from your device',
        content: [
          'You can delete all cookies stored on your device by clearing your browsing history. This will remove all cookies from all websites you have visited.',
          'Be aware though that you may also lose some saved information (e.g. login details, site preferences).',
        ],
      },
      {
        subtitle: 'Managing site-specific cookies',
        content: [
          'For more detailed control over site-specific cookies, check the privacy and cookie settings in your browser',
        ],
      },
      {
        subtitle: 'Blocking cookies',
        content: [
          'Most modern browsers let you prevent any cookies from being placed on your device. This may however be inconvenient, as you will have to manually adjust some preferences every time you visit a site/page. Furthermore, some services and features may not work properly or at all (e.g. profile logging-in).',
        ],
      },
    ],
  },
  {
    title: 'Contact',
    subSession: [
      {
        subtitle: '',
        content: [
          'If you have any questions, please contact the XEE project team via <a href="/contact">contact</a>.',
        ],
      },
    ],
  },
];

export const esTermsOfService = [
  {
    title: 'Política de cookies',
    subSession: [
      {
        subtitle: '',
        content: [
          'Los sitios de XEE guardan pequeños archivos de texto llamados cookies en tu dispositivo para que el sitio funcione apropiadamente.',
        ],
      },
    ],
  },
  {
    title: '¿Qué son las cookies?',
    subSession: [
      {
        subtitle: '',
        content: [
          'Una cookie es un pequeño archivo de texto que algunos sitios web guardan en tu computadora o dispositivo móvil cuando lo visitas.',
          '<ul><li>Cookies de origen son cookies administradas por el sitio web que estás visitando, y solo pueden ser leídas por dicho sitio. Un sitio web puede utilizar servicios de terceros, los cuales guardan sus propias cookies. A estas se les llaman cookies de terceros.</li><li>Cookies permanentes son cookies que se almacenan en tu dispositivo y no son automáticamente eliminadas cuando cierras tu navegador. Son lo contrario de cookies de sesión, las cuales sí se eliminan cuando cierras el navegador.</li></ul>',
          'Cada vez que visites este sitio web se te preguntará si aceptas o rechazas el uso de cookies.',
          'El propósito de su uso es para permitir que el sitio web recuerde tus preferencias (tu nombre de usuario, lenguaje preferido, etc.) por un periodo de tiempo.',
          'De esa forma puedes visitar nuestro sitio sin tener que ajustar tus preferencias cada vez que cambias de página.',
          'Las cookies también pueden ser utilizadas para obtener datos anónimos acerca de tu experiencia de uso con el propósito de hacer mejoras.',
        ],
      },
    ],
  },
  {
    title: '¿Cómo utilizamos las cookies?',
    subSession: [
      {
        subtitle: '',
        content: [
          'Los sitios de XEE utilizan principalmente cookies de origen. Estas cookies son administradas por el equipo del proyecto XEE, y no se comparten con ninguna organización externa.',
          'Sin embargo, aprobar el uso de cookies de terceros puede ser necesario para poder utilizar algunos de nuestros sitios.',
        ],
      },
    ],
  },
  {
    title: 'Cookies de origen',
    subSession: [
      {
        subtitle: 'Requeridas (3)',
        content: [
          'Estas cookies son necesarias para el correcto funcionamiento de nuestro sitio web, y no deben ser desactivadas. Normalmente se almacenan en respuesta a alguna acción de tu parte, por ejemplo cuando ingresas al sitio o cuando llenas un formulario. Es posible configurar tu navegador para que bloquee el uso de cookies o te alerte sobre su uso, pero, pero esto puede causar que algunas partes de nuestro sitio no funcione correctamente. Estas cookies no contienen información acerca de tu identidad.',
          '<div style="padding: 8px; text-align: justify; text-indent: 0; font-size: 50%;"><table border="1" style="border-collapse: collapse;" align="center"><tr><th style="padding: 8px;">Nombre</th><th style="padding: 8px;">Proveedor</th><th style="padding: 8px;">Propósito</th><th style="padding: 8px;">Expiración</th><th style="padding: 8px;">Tipo</th></tr><tr><td style="padding: 8px;">CookieConsent</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">Almacena datos sobre la aprobación del usuario sobre el uso de cookies para este dominio</td><td style="padding: 8px;">1 año</td><td style="padding: 8px;">HTTP</td></tr><tr><td style="padding: 8px;">persist:root</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">Esta cookie almacena credenciales encriptadas. Permite mantener activa la sesión del visitante si este usa la funcionalidad de \'mantenerse conectado\'.</td><td style="padding: 8px;">Permanente</td><td style="padding: 8px;">HTML</td></tr><tr><td style="padding: 8px;">_oxpt_web_key</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">Esta cookie se usa para identificar visitantes del sitio mientras visitan diferentes páginas, y no contienen ninguna información personal.</td><td style="padding: 8px;">Sesión</td><td style="padding: 8px;">HTTP</td></tr></table></div>',
        ],
      },
      {
        subtitle: 'Opcionales (1)',
        content: [
          'Estas cookies permiten recolectar estadísticas de uso de nuestro sitio web, tales como el número de visitas, las páginas que se visitan más frecuentemente, información sobre errores, etc. Estas estadísticas son usadas para medir y mejorar el rendimiento de nuestro sitio, y no contienen información personal del visitante. Si las desactivas no podemos recolectar información para mejorar tu experiencia de usuario.',
          '<div style="padding: 8px; text-align: justify; text-indent: 0; font-size: 50%;"><table border="1" style="border-collapse: collapse;" align="center"><tr><th style="padding: 8px;">Nombre</th><th style="padding: 8px;">Proveedor</th><th style="padding: 8px;">Propósito</th><th style="padding: 8px;">Expiración</th><th style="padding: 8px;">Tipo</th></tr><tr><td style="padding: 8px;">i18nextLng</td><td style="padding: 8px;">xee.jp</td><td style="padding: 8px;">Almacena el lenguaje preferido del visitante para que no tengas que configurarlo cada vez que regreses a nuestro sitio.</td><td style="padding: 8px;">Permanente</td><td style="padding: 8px;">HTTP</td></tr></table></div>',
        ],
      },
    ],
  },
  {
    title: 'Acerca del uso de cookies de terceros',
    subSession: [
      {
        subtitle: '',
        content: [
          'Algunas de las páginas en nuestro sitio utilizan contenido de proveedores externos, como YouTube, Facebook y Twitter.',
          'Es necesario que aceptes los términos de uso de dichos servicios para poder ver su contenido, incluyendo sus políticas de cookies, de las cuales no tenemos ningún control.',
          'Ninguna cookie de terceros se almacena en tu dispositivo si no haces uso de ese tipo de contenido.',
        ],
      },
      {
        subtitle: 'Proveedores externos en sitios de XEE',
        content: [
          '<ul><li><a href="https://www.youtube.com/t/terms">YouTube</a></li><li><a href="https://twitter.com/en/tos#intlTerms">Twitter</a></li><li><a href="https://www.facebook.com/legal/terms">Facebook</a></li><li><a href="https://about.gitlab.com/privacy/cookies/">GitLab</a></li></ul>',
          'Estos servicios de terceros están fuera del control del equipo de XEE. Dichos proveedores pueden cambiar en cualquier momento sus términos de uso, políticas de cookies, etc.',
        ],
      },
    ],
  },
  {
    title: 'Configurando el uso de cookies',
    subSession: [
      {
        subtitle: '',
        content: [
          'Puedes administrar y eliminar cookies como prefieras. Visita <a href="https://www.aboutcookies.org/">aboutcookies.org</a> para aprender acerca de esto en más detalle.',
        ],
      },
      {
        subtitle: 'Eliminar las cookies de tu dispositivo',
        content: [
          'Puedes remover todas las cookies en tu dispositivo eliminando tu historial de búsqueda. Esto elimina las cookies de todos los sitios que has visitado.',
          'Toma en cuenta que esto puede resultar en pérdida de información almacenada en cookies, (por ejemplo: detalles de tu sesión y preferencias del sitio).',
        ],
      },
      {
        subtitle: 'Administrar cookies de sitios específicos',
        content: [
          'Para controlar el uso de cookies de algún sitio en específico, consulta la configuración de uso de cookies y privacidad en tu navegador.',
        ],
      },
      {
        subtitle: 'Bloquear el uso de cookies',
        content: [
          'La mayoría de los navegadores modernos permiten prevenir que algunos sitios web almacenen cookies en tu dispositivo. Esto puede resultar en algunas inconveniencias, incluyendo el tener que configurar tus preferencias de uso cada vez que visitas un sitio web. Además, algunos servicios pueden dejar de funcionar correctamente o del todo (por ejemplo, iniciar sesión).',
        ],
      },
    ],
  },
  {
    title: 'Contacto',
    subSession: [
      {
        subtitle: '',
        content: [
          'Haz llegar cualquier consulta al equipo de XEE a través de nuestra <a href="/contact">página de contacto</a>.',
        ],
      },
    ],
  },
];
