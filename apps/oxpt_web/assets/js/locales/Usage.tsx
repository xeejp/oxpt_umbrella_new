import Fig1 from '../images/usage/jaFigH1P.png';
import Fig2 from '../images/usage/jaFigH2P.png';
import Fig3 from '../images/usage/jaFigH3P.png';
import Fig4 from '../images/usage/jaFigH4P.png';
import Fig5 from '../images/usage/jaFigH5P.png';
import Fig6 from '../images/usage/jaFigH6P.png';
import Fig7 from '../images/usage/jaFigG1P.png';
import Fig8 from '../images/usage/jaFigG2P.png';
import Fig9 from '../images/usage/jaFigG3P.png';
import Fig10 from '../images/usage/jaFigG4P.png';
import Fig11 from '../images/usage/jaFigG5P.png';

export const jaUsage = [
  {
    title: '基本的な使い方',
    subSession: [
      {
        subtitle: 'ホストとゲスト',
        content: [
          'XEEシステムは、経済実験を実施する実験者(ホスト)と被験者(ゲスト)で異なる表示がされます。',
          'ホストは、<ol><li>ルームを作成する。</li><li>ルームの中にゲームを配置する。</li><li>必要に応じてゲームの詳細を設定する(設定しなくても、初期設定で典型的な経済実験が実施できます)。</li><li>ゲストにルーム名を伝えて入室させる。</li><li>ゲームを実施する。</li><li>ゲームが終了したら次のゲームに遷移させる。</li><li>全てのゲームが終了したら実験ログやグラフ画像をダウンロードする。</li></ol>など、ルームを管理する権限を持っています。ホストには、ルームを管理したり、ゲストの状況を把握するための様々な機能が提供されます。',
          'ゲストは、<ol><li>ホストから伝えられたルーム名で入室する。</li><li>ルーム内で行われるゲームに参加する。</li></ol>ことができます。ゲストには、ゲームに参加する上で必要な機能と、言語設定機能だけが提供され、最低限の操作だけでゲームに参加できるようになっています。',
        ],
        image: [
        ],
      },
      {
        subtitle: '権限と料金',
        content: [
          'ホストとしてルームを開設したり、ゲストとしてゲームに参加するために、アカウントを登録する必要はありません。どなたでもすぐにルームを開設し、ゲームに参加できます。',
          'アカウント登録のメリットは、永続ルームを開設できることです。通常のルームは活動ログをメモリ上に配置しているため、サーバーの再起動とともにログが消失します。サーバーの再起動は1日1回程度自動的に行われており、再起動とログ消失によってデータの漏洩の危険性を最小限にしています。一方で、永続ルームは活動ログをデータベース上に保存し、サーバーが再起動した後でも活動ログが消失しないように設定されたルームです。永続ルームはほとんどの利用者にとって必要ありません。永続ルームは、2日以上に渡って継続的に実施する必要のあるルームや、教育・研究上の必要から長期間活動ログを維持しておく必要のあるルームで利用されます。',
          'XEEシステムの利用に料金はかかりません。どなたでも無料でお使いいただけます。',
        ],
        image: [
        ],
      },
    ],
  },
  {
    title: 'ホストの機能',
    subSession: [
      {
        subtitle: 'トップページ',
        content: [
          '【画像1枚目】',
          'A: メニューボタン(ルーム一覧、ルーム開設、アカウント管理ページへのリンクを表示します。)',
          'B: 言語選択・Cookie利用同意ボタン',
          'C: サイト共通ページ(トップページで下スクロールし続けると表示されます)',
          'D: ルーム一覧リンク(ホスト・ゲストとして参加しているルームの一覧を表示します。)',
          '【画像2枚目】',
          'E: ルーム開設ページリンク(クリックするとルーム開設ページを表示します。)',
        ],
        image: [
          {
            label: 'トップページ',
            src: Fig1,
          },
          {
            label: 'メニュー',
            src: Fig2,
          },
        ],
      },
      {
        subtitle: 'ルーム開設ページ',
        content: [
          'A: ルーム名入力テキストエリア(お好きなルーム名を入力してください。ここで入力したルーム名をゲストに伝えることで、ゲストを入室させることができるようになります。すでに使われているルーム名の場合は、エラーが表示されます。混乱を避けるために、ひらがなや漢字など、表記揺れしない文字列で設定することをお勧めします。数字(半角・全角がある)やアルファベット(半角・全角・大文字・小文字がある)の使用はお勧めしません。)',
          'B: 永続ルームチェックボックス(サインイン済みユーザーのみ表示／永続ルームは活動ログをデータベース上に保存し、サーバーが再起動した後でも活動ログが消失しないように設定されたルームです。永続ルームはほとんどの利用者にとって必要ありません。永続ルームは、2日以上に渡って継続的に実施する必要のあるルームや、教育・研究上の必要から長期間活動ログを維持しておく必要のあるルームで利用されます。)',
          'C: 開設ボタン(ルームを開設します。)',
        ],
        image: [
          {
            label: 'ルーム開設ページ',
            src: Fig3,
          },
        ],
      },
      {
        subtitle: 'ラウンジホストページ',
        content: [
          'A: ゲーム作成ボタン(ゲーム作成ダイアログを表示して、実施するゲームを設置できます。ゲームは設置した順に実施されます。)',
          'B: 次のゲームボタン(ゲストが入室した後に、ゲームを遷移させるために使用します。ボタンをクリックすると、ホストの画面もラウンジホストページからゲームホストページへ遷移します。ゲームが開始された後に入室してきたゲストはラウンジゲスト画面で待機させられ、実施中のゲームに参加することはできません。次のゲームからは参加できます。)',
          'C: 前のゲームボタン(誤ってゲームを終了してしまった時などに、1つ前のゲームを再開するために使用します。)',
          'D: ログダウンロードボタン(ゲーム終了後に活動ログをcsv形式でダウンロードします。活動ログにはサーバーとゲーム参加者の通信履歴が全て含まれます。ゲーム実施前や実施中には無効になっています。)',
          'E: 振り返りボタン(ホストが終了したゲームの状況を振り返ることができます。)',
        ],
        image: [
          {
            label: 'ラウンジホストページ',
            src: Fig4,
          },
        ],
      },
      {
        subtitle: 'ゲームホストページ(例)',
        content: [
          '【画像1枚目】',
          'A: ゲーム設定項目(ゲームの詳細を設定します。初期設定で典型的な経済実験が実施できるため、通常は変更する必要はありません。)',
          'B: 言語設定項目(説明文や実験中の文言を設定します。通常は変更する必要はありません。)',
          '【画像2枚目】',
          'C: モード選択(説明→実験→結果の順にゲームを進行させていきます。説明→実験のタイミングで、グループ分けや役割決定などが行われます。また、説明→実験のタイミングでゲストの実験データが初期化されます。「次へ」「戻る」ボタンをクリックして順番に遷移する方法以外に、「説明」「実験」「結果」のアイコンをクリックすることでそのモードに直接遷移することもできます。)',
          'D: ゲストテーブル(ゲストの状況が表示されます。テーブルのヘッダーをクリックすることで並び替えをすることができます。また、IDヘッダーの欄に参加しているゲストの数が表示されます。)',
          'E: なりすましリンク(ゲストIDをクリックすると新しいウィンドウが立ち上がり、ホストがそのゲストになりすましてゲームを進行させることができます。実験を放棄しているゲストや技術的なトラブルが発生した際に利用します。また、ゲストの画面がどのように表示されているかを確認するためにも利用できます。ホストがなりすましている間は、ゲストも同時に操作できます。)',
          'F: 設定ボタン(設定画面を開きます。説明モードの時にのみ有効になっており、それ以外のモードでは無効になります。)',
          'G: ダミーゲスト作成ボタン(このゲームのみに参加する一時的なゲストを追加できます。グループ分けするゲームで人数が足りない場合や、実験実施準備中にゲスト画面の挙動を確認するために利用します。ダミーゲストはゲストテーブルで灰色に表示されます。)',
          'H: ラウンジボタン(ゲームを継続したまま、ホストだけラウンジホストページに戻ります。)',
          'I: 終了ボタン(ゲームを終了し、ゲストをラウンジゲストページに遷移させます。ホストはラウンジホストページに戻ります。)',
        ],
        image: [
          {
            label: 'ゲームホストページ(設定画面)',
            src: Fig5,
          },
          {
            label: 'ゲームホストページ(管理画面)',
            src: Fig6,
          },
        ],
      },
    ],
  },
  {
    title: 'ゲストの機能',
    subSession: [
      {
        subtitle: 'トップページ',
        content: [
          'A: ルーム名入力テキストエリア(ホストから伝えられたルーム名を入力します。)',
          'B: 参加ボタン(ゲストとしてルームに入室します。)',
          'C: ルーム一覧リンク(ホスト・ゲストとして参加しているルームの一覧を表示します。)',
          'D: 言語選択・Cookie利用同意ボタン',
        ],
        image: [
          {
            label: 'トップページ',
            src: Fig7,
          },
        ],
      },
      {
        subtitle: 'ラウンジゲストページ',
        content: [
          'A: 先読みダウンロード進捗バー(この後に実施するゲームに必要なファイルの先読みダウンロードの進捗状況を表示します。先読みダウンロードが終了すると、ゲーム遷移が高速で行え、ゲーム中の操作に伴う通信も安定します。ダウンロードが終了する前にホストがゲームを開始すると、ゲーム画面で実施中のゲームのファイルだけがダウンロードされます。)',
        ],
        image: [
          {
            label: 'ラウンジゲストページ',
            src: Fig8,
          },
        ],
      },
      {
        subtitle: 'ゲームページ',
        content: [
          '【画像1枚目】',
          'A: 説明文(ゲームの説明文が表示されます。)',
          'B: 次へボタン(クリックすると次の説明文が表示されます。最後まで表示すると、ホストに「読了」のステータスが通知されます。)',
          'C: 前へボタン(クリックすると前の説明文が表示されます。)',
          '【画像2枚目】',
          'D: 実験画面(ホストが実験モードに遷移させると、ゲストに実験画面が表示されます。表示内容は実験によって異なります。)',
          '【画像3枚目】',
          'E: 結果画面(ホストが結果モードに遷移させると、ゲストに結果画面が表示されます。)',
          'F: 結果グラフ(実験結果を解説しやすいように、結果グラフを表示します。表示内容は実験によって異なります。結果グラフを画像として保存することもできます。)',
          'G: ランキング等(補足的な情報を表示します。)',
        ],
        image: [
          {
            label: '説明モード',
            src: Fig9,
          },
          {
            label: '実験モード',
            src: Fig10,
          },
          {
            label: '結果モード',
            src: Fig11,
          },
        ],
      },
    ],
  },
];

export const enUsage = [
  {
    title: 'Basic usage',
    subSession: [
      {
        subtitle: 'Host and Guest',
        content: [
          'The XEE system is displayed differently for the experimenter (host) and the participant (guest) who carry out the economic experiment.',
          'The host has the authority to manage the room.<ol><li>The host creates an room.</li><li>Place the game in the room.</li><li>Set the game settings as needed (without setting, you can do typical economic experiments by default).</li><li>Tell the guest the room name and let them enter.</li><li>Play the game.</li><li>When the game is over, transition to the next game.</li><li>After all the games are over, download the experiment log and graph ../images.</li></ol>The host is provided with various functions for managing the room and keeping track of the guest status.',
          '<ol><li>The guest enters with the room name told by the host.</li><li>Participate in games that take place in the room.</li></ol>Guests are provided with only the necessary functions to participate in the game and the language setting function, so that they can participate in the game with minimal operation.',
        ],
        image: [
        ],
      },
      {
        subtitle: 'Authority and fee',
        content: [
          '<font color ="#f50057"><b>You don\'t need to register an account to open a room as a host or join a game as a guest. Anyone can immediately open a room and join the game.',
          'The advantage of account registration is that you can open a permanent room. Since a normal room has an activity log in memory, the log disappears when the server is restarted. The server is automatically restarted once a day, which minimizes the risk of data leakage due to restarts and log loss. A persistent room, on the other hand, is a room configured to store its activity log in the database and not lose it even after the server is restarted. Persistent room is not needed for most users. Permanent rooms are used for rooms that need to be carried out continuously over 2 days or for rooms that require activity logs to be maintained for a long time due to educational and research needs.',
          'There is no charge for using the XEE system. Anyone can use it for free.',
        ],
        image: [
        ],
      },
    ],
  },
  {
    title: 'The functions for the host',
    subSession: [
      {
        subtitle: 'Top page',
        content: [
          '[The 1st picture]',
          'A: Menu button(Displays pages of Room list, room create, account management pages.)',
          'B: Language selecter・Cookie agreement button',
          'C: Common pages(It will be displayed if you continue scrolling down on the top page.)',
          'D: Room list link(Displays a list of rooms participating as host / guest.)',
          '[The 2nd picture]',
          'E: Create Room link(Click to display the room opening page.)',
        ],
        image: [
          {
            label: 'Top page',
            src: Fig1,
          },
          {
            label: 'Menu',
            src: Fig2,
          },
        ],
      },
      {
        subtitle: 'Create room page',
        content: [
          'A: Room name textarea(Please enter the room name you like. By telling the guest the room name entered here, you will be able to enter the guest. If the room name is already in use, an error will be displayed.)',
          'B: Persistent room check box(Displays only signed-in user / Persistent room is a room that saves the activity log in the database and does not lose the activity log even after the server is restarted. Persistent room is not needed for most users. Persistent rooms are used for rooms that need to be carried out continuously over 2 days or for rooms that require activity logs to be maintained for a long time due to educational and research needs.)',
          'C: Create button(Click to create room.)',
        ],
        image: [
          {
            label: 'Create room page',
            src: Fig3,
          },
        ],
      },
      {
        subtitle: 'Lounge host page',
        content: [
          'A: Add game button(You can display the game addition dialog and set the game to be executed. The games will be played in the order they are added.)',
          'B: Next game button(It is used to transition the game after the guest enters the room. When you click the button, the host screen will also change from the lounge host page to the game host page. Guests who enter the room after the game has started will be put on standby at the lounge guest screen and cannot join the game in progress. You can join from the following games.)',
          'C: Previous game button(Use this to restart the previous game when you accidentally end the game.)',
          'D: Log download button(Download the activity log in csv format after the game ends. The activity log includes all communication history between the server and game participants. It is disabled before and during the game.)',
          'E: Playback button(You can play back on the situation of the game that the host finished.)',
        ],
        image: [
          {
            label: 'Loung host page',
            src: Fig4,
          },
        ],
      },
      {
        subtitle: 'Game host page (example)',
        content: [
          '[The 1st picture]',
          'A: Game settings(Set the game details. Normally you do not need to change, as the default settings allow you to carry out typical economic experiments.)',
          'B: Words settings(Set the description and the text during the experiment. Normally you do not need to change.)',
          '[The 2nd picture]',
          'C: Mode selection(The game proceeds in the order of explanation → experiment → result. Explanation → Grouping and role determination will be done at the timing of the experiment. Also, the guest\'s experimental data will be initialized at the time of the explanation → experiment. In addition to clicking the "Next" and "Back" buttons to transition in order, you can also directly transition to that mode by clicking the "Explanation", "Experiment", and "Result" icons.)',
          'D: Guest table(The guest status is displayed. You can sort by clicking the table header. In addition, the number of participating guests is displayed in the column of the ID header.)',
          'E: Spoofing links(Clicking on the guest ID will bring up a new window where the host can impersonate the guest and proceed with the game. It is used when a guest who has abandoned the experiment or technical trouble occurs. You can also use it to see what your guest\'s screen looks like. Guests can operate at the same time as the host is impersonating.)',
          'F: Setting button(Open the setting screen. Enabled only in description mode, disabled in all other modes.)',
          'G: Dummy guest add button(You can add temporary guests to join this game only. It is used to check the behavior of the guest screen when there are not enough people in the game to be divided into groups or when preparing for the experiment. Dummy guests are grayed out in the guest table.)',
          'H: Lounge button(Only the host will return to the lounge host page while continuing the game.)',
          'I: Finish button(Exit the game and transition the guest to the lounge guest page. The host will return to the lounge host page.)',
        ],
        image: [
          {
            label: 'Game host page (settings)',
            src: Fig5,
          },
          {
            label: 'Game host page (management)',
            src: Fig6,
          },
        ],
      },
    ],
  },
  {
    title: 'The functions for the guest',
    subSession: [
      {
        subtitle: 'Top page',
        content: [
          'A: Room name textarea(Enter the room name told to you by your host.)',
          'B: Join button(Enter the room as a guest.)',
          'C: Room name link(Displays a list of rooms participating as host / guest.)',
          'D: Language selecter/ Cookie agreement button',
        ],
        image: [
          {
            label: 'Top page',
            src: Fig7,
          },
        ],
      },
      {
        subtitle: 'Lounge guest page',
        content: [
          'A: Proactivev download progress bar(Shows the progress of the prefetch download of the files required for the game to be executed after this. When the prefetch download is completed, the game transition can be performed at high speed and the communication accompanying the operation during the game is stable. If the host starts the game before the download is complete, only the files for the game currently running on the game screen will be downloaded.)',
        ],
        image: [
          {
            label: 'Lounge guest page',
            src: Fig8,
          },
        ],
      },
      {
        subtitle: 'Game page',
        content: [
          '[The 1st picture]',
          'A: Instruction(The description of the game is displayed.)',
          'B: Next button(Click to display the following explanation. When displayed to the end, the host is notified of a "read" status.)',
          'C: Back button(Click to display the previous description.)',
          '[The 2nd picture]',
          'D: Experiment(When the host switches to experimental mode, the guest will see the experimental screen. The displayed contents differ depending on the experiment.)',
          '[The 3rd picture]',
          'E: Result(When the host transitions to result mode, the guest will see the results screen.)',
          'F: Result charts(A result graph is displayed so that you can easily explain the experimental results. The displayed contents differ depending on the experiment. You can also save the result graph as an image.)',
          'G: Ranking etc.(Display supplemental information.)',
        ],
        image: [
          {
            label: 'Instruction mode',
            src: Fig9,
          },
          {
            label: 'Experiment mode',
            src: Fig10,
          },
          {
            label: 'Result mode',
            src: Fig11,
          },
        ],
      },
    ],
  },
];
