import HayashiF from '../images/HayashiF.png';
import KonagayoshiF from '../images/KonagayoshiF.png';
import IkedaF from '../images/IkedaF.png';
import HashiguchiF from '../images/HashiguchiF.png';
import MatsumotoF from '../images/MatsumotoF.png';
import YunoueF from '../images/YunoueF.png';
import IchikawaF from '../images/IchikawaF.png';
import HigashiF from '../images/HigashiF.png';

export const jaMembers = [
  {
    name: '林 良平',
    image: HayashiF,
    link: 'https://ryohei.info',
    role: ['PI', 'UI', 'Translate'],
    description: '博士（経済学）。鹿児島高専准教授（2012年〜2017年）を経て東海大学政治経済学部講師。専門分野は行動経済学、実験経済学、応用ミクロ経済学。データを駆使して、周囲の人の能力が自分の能力を向上させるピア効果の実証研究を行なっている。主な業績に、<a href="https://doi.org/10.1111/sjoe.12124">"Peer Effects among Swimmers"</a> (Yamane and Hayashi, 2015, Scand. J. of Economics)。',
  },
  {
    name: '小永吉 健太',
    image: KonagayoshiF,
    link: 'mailto: konagayoshi.kenta@inf.kyushu-u.ac.jp',
    role: ['UI'],
    description: '機械学習の基礎理論について九州大学で研究をしております。よろしくお願いします。',
  },
  {
    name: '池田 伊織',
    image: IkedaF,
    link: 'https://twitter.com/notfounds8080',
    role: ['Expt'],
    description: 'Webエンジニアです。XEEの開発には第3世代から携わっています。マイクロサービスアーキテクチャやコンテナオーケストレーションについて勉強しています。',
  },
  {
    name: '橋口 遼',
    image: HashiguchiF,
    link: null,
    role: ['Core'],
    description: '第３世代XEEに引き続き、第４世代でもシステム全体の設計を担当し、開発に参加しました。',
  },
  {
    name: '松元 翔矢',
    image: MatsumotoF,
    link: 'https://www.m47ch4n.net',
    role: ['Core', 'Expt'],
    description: 'CSを専攻している学生です。最近は、様々なプログラミング言語のパラダイムに触れてみたいと思いClojure、rustなどを勉強しています。イラストは「まつも」くんです。グッズも売っています!<a href="https://suzuri.jp/m47ch4n">https://suzuri.jp/m47ch4n</a>',
  },
  {
    name: '湯之上 航',
    image: YunoueF,
    link: null,
    role: ['Core', 'Expt'],
    description: '鹿児島高専卒。2021年現在は九州大学大学院数理学府に在籍。XEEでは様々な実験の内部処理やUIなどを作成しました。今はより一般的な動画を生成できるようなGANについて研究をしています。',
  },
  {
    name: '市川 知春',
    image: IchikawaF,
    link: 'https://github.com/skytomo221',
    role: ['UI'],
    description: "鹿児島工業高等専門学校 情報工学科（2015年～）。卒業研究では Python で自然言語処理を用いて Amazon などのレビューを評価するアルゴリズムを作っています。2018年と2019年に VR ワークショップ（Unity や Unreal Engine などを使って色々なオブジェクトを作ったり、バーチャル空間内で触ったりする体験）でスタッフとして参加しました。2017年には、海外異文化研修でスウェーデンに1週間滞在しました。好きなプログラミング言語は C# です。英語は得意ではないですが、言語には興味があり、 Duolingo でスウェーデン語をやっています。趣味は Lojban を始めとした人工言語や Conway's Game of Life です。よろしくお願いします。",
  },
  {
    name: '東 誠太',
    image: HigashiF,
    link: 'https://github.com/SeitaHigashi',
    role: ['Core', 'Expt'],
    description: '東誠太です。大規模なプログラムより小規模なプログラムをたくさんつくるのが好きです。よろしくお願いします。',
  },
];

export const enMembers = [
  {
    name: 'Ryohei HAYASHI',
    image: HayashiF,
    link: 'https://ryohei.info',
    role: ['PI', 'UI', 'Translate'],
    description: 'Ph.D. in Economics. After working as an associate professor at Kagoshima National College of Technology (2012-2017), he became a lecturer at the School of Political Science and Economics at Tokai University. Research mejor is in behavioral economics, experimental economics, and applied microeconomics, especially about peer effects. Major achievements include <a href="https://doi.org/10.1111/sjoe.12124"> "Peer Effects among Swimmers"</a> (Yamane and Hayashi, 2015, Scand. J. of Economics).',
  },
  {
    name: 'Kenta KONAGAYOSHI',
    image: KonagayoshiF,
    link: 'mailto: konagayoshi.kenta@inf.kyushu-u.ac.jp',
    role: ['UI'],
    description: 'I am researching the basic theory of machine learning at Kyushu University. Thank you.',
  },
  {
    name: 'Iori Ikeda',
    image: IkedaF,
    link: 'https://twitter.com/notfounds8080',
    role: ['Expt'],
    description: "I'm a web engineer. I have been involved in the development of XEE since the 3rd generation. I am studying microservices architecture and container orchestration.",
  },
  {
    name: 'Ryo HASHIGUCHI',
    image: HashiguchiF,
    link: null,
    role: ['Core'],
    description: 'Continuing from the 3rd generation XEE, I was in charge of the entire system design for the 4th generation and participated in the development.',
  },
  {
    name: 'Shoya MATSUMOTO',
    image: MatsumotoF,
    link: 'https://www.m47ch4n.net',
    role: ['Core', 'Expt'],
    description: "I am a student majoring in computer science. Recently, I'm studying Clojure, rust, etc., because I want to touch the paradigms of various programming languages. The illustration is \"Matsumo\". Goods are also on sale!<a href=\"https://suzuri.jp/m47ch4n\">https://suzuri.jp/m47ch4n</a>",
  },
  {
    name: 'Wataru YUNOUE',
    image: YunoueF,
    link: null,
    role: ['Core', 'Expt'],
    description: 'I am a graduate of National Institute of Technology, Kagoshima College. I am studying machine learning.',
  },
  {
    name: 'Tomoharu ICHIKAWA',
    image: IchikawaF,
    link: 'https://github.com/skytomo221',
    role: ['UI'],
    description: "Student of Department of Information Engineering, National Institute of Technology, Kagoshima College (2015-). In my graduation research, I am making an algorithm that evaluates reviews such as Amazon using natural language processing in Python. In 2018 and 2019, I participated as a staff member in a VR workshop (experience of making various objects using Unity, Unreal Engine, etc. and touching them in a virtual space). In 2017, I spent a week in Sweden studying abroad in a different culture. My favorite programming language is C #. I'm not good at English, but I'm interested in languages ​​and I'm doing Swedish at Duolingo. My hobbies are artificial languages ​​such as Lojban and Conway's Game of Life. Thank you.",
  },
  {
    name: 'Seita HIGASHI',
    image: HigashiF,
    link: 'https://github.com/SeitaHigashi',
    role: ['Core', 'Expt'],
    description: 'I am Seita Higashi. I like creating many smaller programs than larger ones. Thank you.',
  },
];

export const esMembers = [
  {
    name: 'Ryohei HAYASHI',
    image: HayashiF,
    link: 'https://ryohei.info',
    role: ['PI', 'UI', 'Translate'],
    description: 'Ph.D. in Economics. After working as an associate professor at Kagoshima National College of Technology (2012-2017), he became a lecturer at the School of Political Science and Economics at Tokai University. Research mejor is in behavioral economics, experimental economics, and applied microeconomics, especially about peer effects. Major achievements include <a href="https://doi.org/10.1111/sjoe.12124"> "Peer Effects among Swimmers"</a> (Yamane and Hayashi, 2015, Scand. J. of Economics).',
  },
  {
    name: 'Kenta KONAGAYOSHI',
    image: KonagayoshiF,
    link: 'mailto: konagayoshi.kenta@inf.kyushu-u.ac.jp',
    role: ['UI'],
    description: 'I am researching the basic theory of machine learning at Kyushu University. Thank you.',
  },
  {
    name: 'Iori Ikeda',
    image: IkedaF,
    link: 'https://twitter.com/notfounds8080',
    role: ['Expt'],
    description: "I'm a web engineer. I have been involved in the development of XEE since the 3rd generation. I am studying microservices architecture and container orchestration.",
  },
  {
    name: 'Ryo HASHIGUCHI',
    image: HashiguchiF,
    link: null,
    role: ['Core'],
    description: 'Continuing from the 3rd generation XEE, I was in charge of the entire system design for the 4th generation and participated in the development.',
  },
  {
    name: 'Shoya MATSUMOTO',
    image: MatsumotoF,
    link: 'https://www.m47ch4n.net',
    role: ['Core', 'Expt'],
    description: "I am a student majoring in computer science. Recently, I'm studying Clojure, rust, etc., because I want to touch the paradigms of various programming languages. The illustration is \"Matsumo\". Goods are also on sale!<a href=\"https://suzuri.jp/m47ch4n\">https://suzuri.jp/m47ch4n</a>",
  },
  {
    name: 'Wataru YUNOUE',
    image: YunoueF,
    link: null,
    role: ['Core', 'Expt'],
    description: 'I am a graduate of National Institute of Technology, Kagoshima College. I am studying machine learning.',
  },
  {
    name: 'Tomoharu ICHIKAWA',
    image: IchikawaF,
    link: 'https://github.com/skytomo221',
    role: ['UI'],
    description: "Student of Department of Information Engineering, National Institute of Technology, Kagoshima College (2015-). In my graduation research, I am making an algorithm that evaluates reviews such as Amazon using natural language processing in Python. In 2018 and 2019, I participated as a staff member in a VR workshop (experience of making various objects using Unity, Unreal Engine, etc. and touching them in a virtual space). In 2017, I spent a week in Sweden studying abroad in a different culture. My favorite programming language is C #. I'm not good at English, but I'm interested in languages ​​and I'm doing Swedish at Duolingo. My hobbies are artificial languages ​​such as Lojban and Conway's Game of Life. Thank you.",
  },
  {
    name: 'Seita HIGASHI',
    image: HigashiF,
    link: 'https://github.com/SeitaHigashi',
    role: ['Core', 'Expt'],
    description: 'I am Seita Higashi. I like creating many smaller programs than larger ones. Thank you.',
  },
];
