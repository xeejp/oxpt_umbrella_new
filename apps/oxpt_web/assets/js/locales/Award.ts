export const jaAwards = [
  {
    year: 2015,
    type: 'grant',
    title: '科学研究費補助金（若手研究(B))',
    titleUrl: 'https://www.jsps.go.jp/j-grantsinaid/03_keikaku/index.html',
    date: '2015/04 - 2018/03',
    description: '「オンライン経済実験教材の開発と新しい経済学教授法の提案」',
    descriptionUrl: 'https://kaken.nii.ac.jp/ja/grant/KAKENHI-PROJECT-15K16268/',
    author: '林　良平（鹿児島工業高等専門学校）',
    authorUrl: '/developer',
  },
  {
    year: 2016,
    type: 'award',
    title: '行動経済学会第10回記念大会奨励賞',
    titleUrl: 'http://www.abef.jp/prize/award/',
    date: '2016/12/04',
    description: '「オンライン経済実験教材の開発」',
    descriptionUrl: 'https://doi.org/10.11167/jbef.9.122',
    author: '林　良平（鹿児島工業高等専門学校）',
    authorUrl: '/developer',
  },
  {
    year: 2017,
    type: 'grant',
    title: '電気通信普及財団研究助成',
    titleUrl: 'https://www.taf.or.jp/grant-a/',
    date: '2017/04 - 2019/03',
    description: '「遠隔地相互作用経済実験システムの開発」',
    descriptionUrl: '',
    author: '林　良平（東海大学）',
    authorUrl: '/developer',
  },
  {
    year: 2018,
    type: 'grant',
    title: '科学研究費補助金（基盤研究(C))',
    titleUrl: 'https://www.jsps.go.jp/j-grantsinaid/03_keikaku/index.html',
    date: '2018/04 - 2023/03',
    description: '「遠隔地相互作用実験を実現する経済実験システムの開発」',
    descriptionUrl: 'https://kaken.nii.ac.jp/ja/grant/KAKENHI-PROJECT-18K01517/',
    author: '林　良平（東海大学）',
    authorUrl: '/developer',
  },
];

export const enAwards = [
  {
    year: 2015,
    type: 'grant',
    title: 'Grant-in-Aid for Young Scientists (B)',
    titleUrl: 'https://www.jsps.go.jp/english/e-grants/grants01.html',
    date: '2015/04 - 2018/03',
    description: '"Development of Online Experiment System for Economic Education and Proposal of New Economics Teaching Methods"',
    descriptionUrl: 'https://kaken.nii.ac.jp/en/grant/KAKENHI-PROJECT-15K16268/',
    author: 'Ryohei Hayashi (National Institute of Technology, Kagoshima College)',
    authorUrl: '/developer',
  },
  {
    year: 2016,
    type: 'award',
    title: 'Association of Behavioral Economics and Finance, Best Presentation Award',
    titleUrl: 'http://www.abef.jp/prize/award/',
    date: '2016/12/04',
    description: '”Online Experiment System for Economic Education."',
    descriptionUrl: 'https://doi.org/10.11167/jbef.9.122',
    author: 'Ryohei Hayashi (National Institute of Technology, Kagoshima College)',
    authorUrl: '/developer',
  },
  {
    year: 2017,
    type: 'grant',
    title: 'Grant-in-Aid by the Telecomunications Advancement Fundation',
    titleUrl: 'https://www.taf.or.jp/grant-a/',
    date: '2017/04 - 2019/03',
    description: '”Development of New Economic Experiment System for Remote Interaction Experiments"',
    descriptionUrl: '',
    author: 'Ryohei Hayashi (Tokai University)',
    authorUrl: '/developer',
  },
  {
    year: 2018,
    type: 'grant',
    title: 'Grant-in-Aid for Scientific Research (C)',
    titleUrl: 'https://www.jsps.go.jp/english/e-grants/grants01.html',
    date: '2018/04 - 2023/03',
    description: '”Development of New Economic Experiment System That Realizes Remote Interaction Experiments"',
    descriptionUrl: 'https://kaken.nii.ac.jp/en/grant/KAKENHI-PROJECT-18K01517/',
    author: 'Ryohei Hayashi (Tokai University)',
    authorUrl: '/developer',
  },
];

export const esAwards = [
  {
    year: 2015,
    type: 'grant',
    title: 'Fondo para Investigadores Jóvenes (B)',
    titleUrl: 'https://www.jsps.go.jp/english/e-grants/grants01.html',
    date: '2015/04 - 2018/03',
    description: '"Desarrollo de un sistema en línea de experimentación para la educación y el desarrollo de nuevos métodos de enseñanza en Economía."',
    descriptionUrl: 'https://kaken.nii.ac.jp/en/grant/KAKENHI-PROJECT-15K16268/',
    author: 'Ryohei Hayashi (Instituto Nacional de Tecnología, Universidad de Kagoshima)',
    authorUrl: '/developer',
  },
  {
    year: 2016,
    type: 'award',
    title: 'Asociación de Economía Conductual y Finanzas, Premio a la Mejor Presentación',
    titleUrl: 'http://www.abef.jp/prize/award/',
    date: '2016/12/04',
    description: '”Sistema en línea de experimentación para la educación en Economía."',
    descriptionUrl: 'https://doi.org/10.11167/jbef.9.122',
    author: 'Ryohei Hayashi (Instituto Nacional de Tecnología, Universidad de Kagoshima)',
    authorUrl: '/developer',
  },
  {
    year: 2017,
    type: 'grant',
    title: 'Grant-in-Aid by the Telecomunications Advancement Fundation',
    titleUrl: 'https://www.taf.or.jp/grant-a/',
    date: '2017/04 - 2019/03',
    description: '”Desarrollo de un nuevo sistema de experimentación en Economía para experimentos con interacción remota"',
    descriptionUrl: '',
    author: 'Ryohei Hayashi (Universidad de Tokai)',
    authorUrl: '/developer',
  },
  {
    year: 2018,
    type: 'grant',
    title: 'Fondos para la Investigación Científica (C)',
    titleUrl: 'https://www.jsps.go.jp/english/e-grants/grants01.html',
    date: '2018/04 - 2023/03',
    description: '”Desarrollo de un nuevo sistema de experimentación en Economía utilizando experimentos con interacción remota"',
    descriptionUrl: 'https://kaken.nii.ac.jp/en/grant/KAKENHI-PROJECT-18K01517/',
    author: 'Ryohei Hayashi (Universidad de Tokai)',
    authorUrl: '/developer',
  },
];
