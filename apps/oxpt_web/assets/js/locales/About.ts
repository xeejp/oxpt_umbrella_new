export const jaAbout = [
  {
    title: '使い方',
    subSession: [
      {
        subtitle: '操作方法',
        content: [
          'XEEの操作方法は<a href="/usage">使い方</a>のページをご覧ください。',
        ],
      },
      {
        subtitle: 'ゲームの種類',
        content: [
          '実装されているゲームの種類は<a href="/gameList">ゲーム一覧</a>をご覧ください。また、ルーム作成後のホストページでも確認できます。',
        ],
      },
      {
        subtitle: 'ゲームの解説',
        content: [
          'ゲームの理論的背景や実施方法、グラフの読み方、解説の方法などについては、雑誌「<a href="https://www.nippyo.co.jp/shop/magazines/latest/3.html">経済セミナー</a>」（日本評論社）で解説しています。',
          '<a href="https://www.nippyo.co.jp/shop/magazine/8249.html">2020年4・5月号　通巻 713号</a> (pp.75-84)：「実験経済への招待」（最後通牒ゲーム）',
          '<a href="https://www.nippyo.co.jp/shop/magazine/8300.html">2020年6・7月号　通巻 714号</a> (pp.84-93)：「市場取引を体験する」（ダブルオークション）',
          '<a href="https://www.nippyo.co.jp/shop/magazine/8341.html">2020年8・9月号　通巻 715号</a> (pp.97-108)：「不完全競争市場を体験する」（ベルトラン競争、クールノー競争、シュタッケルベルク競争　＊未実装）',
          '<a href="https://www.nippyo.co.jp/shop/magazine/8370.html">2020年10・11月号　通巻 716号</a> (pp.89-98)：「ゲーム理論の基本を体験する」（囚人のジレンマゲーム　＊未実装）',
          '<a href="https://www.nippyo.co.jp/shop/magazine/8405.html">2020年12月・2021年1月号　通巻 717号</a> (pp.105-114)：「公共財ゲームを体験する」（公共財ゲーム）',
          'また、<a href="https://sites.google.com/view/keisemi-hskexpecon/">連載サポートページ</a>から詳細な補助資料を入手できます。',
        ],
      },
    ],
  },
  {
    title: '特長',
    subSession: [
      {
        subtitle: '',
        content: [
          'XEEシステムは教室での実験に特化してデザインされており、無料で利用できます。教室実験のために、次のような点が重視されています。',
        ],
      },
      {
        subtitle: '簡単ですばやい設定',
        content: [
          '先生が思いついた時にすぐに実験できるように、セットアップはできるだけ簡単にしました。授業中に混乱することがないように、最初の設定で代表的な条件の実験ができるようにしています。先生は、ただボタンを押すだけで実験を進行させられます。',
        ],
      },
      {
        subtitle: '軽量・高速な通信',
        content: [
          'XEEシステムはスマートフォンからも利用できます。システムが使用する通信量は非常にわずかです。そのため、通信制限中の学生でも快適にプレイでき、通信料が心配な学生にとっても負担は少なくて済みます。これは、ページを再読み込みすることなしに、データだけをやりとりすることで実現してます。',
        ],
      },
      {
        subtitle: '安定して確実に動く',
        content: [
          'XEEシステムは<a href="https://elixir-lang.org/">Elixir</a>というプログラミング言語で動いていますので、計算プロセスは非常にシンプルです。そのため、1,000人を超える人数で同時に実験したとしても、遅延が起こることはありません。',
        ],
      },
    ],
  },
  {
    title: '開発の背景',
    subSession: [
      {
        subtitle: '教室実験',
        content: [
          '経済実験は研究目的のためにラボで行われるだけではなく、教育目的で教室で行われることもあります。教育成果の向上を目的に教室で行われる実験のことをclassroom experimentsと呼びます。',
          '先生は、教室実験の際に挙手、紙とペン、カード、サイコロ、付箋紙などのアナログな道具を用います。また、コンピュータやスマートフォンなどのデジタル・ツールを使用することもあります。',
          '実験は楽しいので、先生も学生も大好きです。学生は、講義形式の授業を退屈に感じます。反対に、実験がある授業には熱中して取り組みます。',
          '加えて、先生は実験を通じて学生が授業の内容や科学的な実証方法に対する理解を深めることを期待しています。また、実験は授業の導入やアイスブレイクとして用いられることもあります。',
        ],
      },
      {
        subtitle: '教室実験の課題',
        content: [
          'しかしながら、先生にとって教室で実験をすることは簡単ではありません。',
          '第一の問題はコストです。実験を準備したり、片付けたり、結果を集計したりする作業は骨が折れます。先生は授業の前に質問紙を印刷したり、カードを購入したり、コンピュータを導入する必要があります。そして、授業中にも実験のセットアップをする必要があります。授業中に実験のために時間を使ってしまうと、その分講義をする時間が減ってしまいます。さらに先生は授業後も、学生が回答した内容を鍵のかかるロッカーに保管して秘密保持に努めなければなりません。したがって、先生にとっては実験を実施するよりも、普段通り講義をしていた方が楽なのです。',
          '教室実験は厳密な統制に関しても問題に直面しています。先生は、学生同士が好きなものとペアになって実験結果を偏らせることがないように、ランダムにペアを決めなければなりません。しかしそうするためには、乱数を生成する道具が必要になります。また、教室は学生にとっては開放的な空間であるため、意味もなく叫ぶ学生もいます。こうした学生に対処し、私的情報の条件を満たすために、先生は気を揉まなくてはいけません。',
          'これらのことが解決されたとしても、学生が実験に真剣かつ全力で取り組むインセンティブがないという問題は依然横たわっています。',
        ],
      },
      {
        subtitle: 'コンピュータを使った教室実験の課題',
        content: [
          'コンピュータを導入することで、教室実験の課題の大半は解決されます。しかしながら、新たな問題も発生します。',
          'コンピュータ実験を実施するためには、コンピュータとソフトウェアが必要です。',
          '世の中にはコンピュータ実験用の素晴らしいソフトウェアがいくつかあります。しかしながら、これらを利用するためには作者と契約を結ぶ必要があったり、支払いが発生したりします。ソフトウェアが無料であったとしても、自前のサーバーや学生用のコンピュータを用意しなければなりません。',
          'さらに、実験条件を設定するためにプログラミング言語を学ばなければならないかもしれません。',
        ],
      },
    ],
  },
  {
    title: '開発チーム',
    subSession: [
      {
        subtitle: 'チームメンバー',
        content: [
          'XEEシステムは、研究代表者の林良平が2012年に第1世代を開発しました。その後、<a href="http://www.kagoshima-ct.ac.jp/">鹿児島工業高等専門学校</a>の学生（当時）が中心となって汎用化、頑健化、高速化が進められました。現在は、高専を卒業したOBの学生により開発が進められています。',
          '開発チームのメンバーは<a href="/developer">開発チーム</a>から確認できます。非常に生産性の高いプログラマですので、有能なスタッフが欲しい企業はぜひスカウトしてください。',
        ],
      },
      {
        subtitle: '開発原資',
        content: [
          '第1世代の開発にあたっては、<a href="https://www.kindai.ac.jp/economics/">近畿大学経済学部</a>様から開発資金のご提供をいただきました。',
          'その後の本格的な開発に必要な資金は科学研究費補助金や民間の研究助成金によって賄われています。これまでにご支援いただいた資金は<a href="/award">受賞歴と研究助成</a>をご覧ください。',
          'なお、ご支援いただける機会がありましたらぜひ<a href="/contact">お問い合わせ</a>よりご提案いただきますと幸いです。',
        ],
      },
      {
        subtitle: '外部協力者',
        content: [
          'システムの正常な動作を確認したり、大人数での実験を想定した負荷テストなどを実施する際に、以下の方々のご協力をいただいております。(第4世代のみ掲載)',
          '<ul><li>佐々木俊一郎氏（近畿大学）</li><li>山根承子氏（株式会社パパラカ研究所）</li><li>林寛平氏（信州大学）</li><li>大垣昌夫氏（慶應大学）</li></ul>',
        ],
      },
    ],
  },
  {
    title: '第4世代',
    subSession: [
      {
        subtitle: 'What’s new',
        content: [
          '<ul><li>デバイスに最適な表示に自動調整(レスポンシブ・デザイン)</li><li>多言語対応</li><li>デザインの改善(UI)</li><li>複数実験の実施が容易に</li></ul>',
        ],
      },
      {
        subtitle: '何ができるようになるか',
        content: [
          '<ul><li>クロス・カルチャーでの相互作用実験</li><li>日常生活場面での野外実験</li><li>発展途上国における経済学教育</li>',
        ],
      },
    ],
  },
  {
    title: '協力者募集',
    subSession: [
      {
        subtitle: '翻訳者',
        content: [
          'XEEシステムをあなたの言語に翻訳してくれる協力者を募集しています。翻訳は、英語のテキストファイルをもとにあなたの言語に翻訳するだけです。非常に簡単です。',
          '翻訳者に報酬を支払うことはできませんが、XEEの<a href="/developer">開発チーム</a>ページに翻訳者として名前を載せられます。',
        ],
      },
      {
        subtitle: 'テスター',
        content: [
          'XEEシステムを教室やラボ、野外などで実際に利用して、不具合がありましたらご報告ください。',
          '不具合を見つけられた方は、<a href="/contact">お問い合わせ</a>を通じてご連絡いただければ、開発チームが調査して解決します。',
        ],
      },
    ],
  },
];

export const enAbout = [
  {
    title: 'Usage',
    subSession: [
      {
        subtitle: 'Operation',
        content: [
          'See the <a href="/usage">Usage</a> page for how to use XEE.',
        ],
      },
      {
        subtitle: 'Games',
        content: [
          'See <a href="/gameList">Games</a> for the kinds of games available. You can also check it on the host page after creating a room.',
        ],
      },
    ],
  },
  {
    title: 'Advantages of our system',
    subSession: [
      {
        subtitle: '',
        content: [
          'Our system has been designed for use in class, and it\'s free to use. We highlight the following benefits:',
        ],
      },
      {
        subtitle: 'Quick and easy setup',
        content: [
          'We made it possible to set up the system within one minute so that teachers can start experimenting while concentrating on their class. To prevent distractions for teachers, typical experiments can be performed using the default settings. Teachers only need to press the button to proceed with the experiment.',
        ],
      },
      {
        subtitle: 'Efficient data transmission',
        content: [
          'This system can be used through smartphones and requires very little network traffic; therefore, the burden on the students is small enough, and even a limited or slow network can be used to conduct the experiment comfortably. This is achieved by updating only the data instead of reloading the page.',
        ],
      },
      {
        subtitle: 'Stability and speed',
        content: [
          'Because we use <a href="https://elixir-lang.org/">Elixir</a> as a server-side language, the calculation process is very simple and there is no delay even if over 1,000 students use the system at the same time.',
        ],
      },
    ],
  },
  {
    title: 'Background',
    subSession: [
      {
        subtitle: 'Classroom Experiment',
        content: [
          'Economic experiments are conducted in not only the laboratory for research purposes but also classrooms during classes for pedagogical purposes. Economic experiments conducted with the aim of improving educational results are called classroom experiments.',
          'Teachers use analog tools such as raised hands, paper and pens, cards, dices, tag papers, and so on. Teachers also use digital tools such as computers, laptops, tablets, smartphones, and so on.',
          'Teachers and students really love experiments because they are fun. Especially, students like experiments because the chalk-and-talk style lectures are too boring for them. In contrast, classes that involve experiments are exciting.',
          'In addition, teachers expect experiments to help students’ understanding of current topics, provide a way to demonstrate scientific facts. Experiments are also used as ice-breaking exercises or as an introduction when classes start.',
        ],
      },
      {
        subtitle: 'Problems in employing experiments in the classroom',
        content: [
          'However, using experiments in class is not so easy for teachers, as there is some difficulty to doing it.',
          'The first factor is the costs. Preparation and set up require some effort. Teachers need to print questionnaire sheets or buy cards, dice, computers, and so on before classes start. Moreover, teachers need to set up the experiment during the class. If teachers have to spend more time to set up the experiment, they will spend less time on the class. In addition, teachers must keep the students’ responses in a locked safe to keep them secret. Thus, the usual chalk-and-talk of teaching style is easier for teachers, as they feel it is expensive to conduct experiments.',
          'Classroom experiments also face some problems regarding their implementation accuracy. Teachers need to divide students into random pairs to avoid self-selection bias, but to do so, they need to use certain tools. The classroom is an open space for students, and their behavior may make it difficult for teachers to keep private information from spreading.',
          'Even if these problems can be addressed, there is no guarantee that the students will be actively involved in the experiment and pay their effort to do their best.',
        ],
      },
      {
        subtitle: 'Problems when implementing a computer experiment in the classroom',
        content: [
          'Employing a computer solves most of the problems of classroom experiments. However, other problems emerge.',
          'To employ computer experiments, you need computers and software.',
          'There are various wonderful software for computer experiments. However to use them, some systems require paid subscriptions, or entering into contracts with the authors. Even if the software is free to use, you need to set up your own server and the students’ client computers.',
          'Moreover, you must learn a programming language to configure the experiments.',
        ],
      },
    ],
  },
  {
    title: 'Developer team',
    subSession: [
      {
        subtitle: 'Team members',
        content: [
          'Dr. Ryohei Hayashi developed the 1st generation of the XEE system in 2012. After that, students of <a href="http://www.kagoshima-ct.ac.jp/english/">National Institute of Technology Kagoshima College</a> (at that time) played a central role in improving the generalization, robustness, and speed of the system. Currently, it is being developed by alumni.',
          'Members of the development team can be found at <a href="/developer">Developers</a>. They are very productive programmers, so if your company is interested in hiring highly skilled engineers feel free to contact them.',
        ],
      },
      {
        subtitle: 'Development funds',
        content: [
          '<a href="https://www.kindai.ac.jp/economics/">Faculty of Economics, Kindai University</a> provided development funds for the 1st generation development.',
          'The funds necessary for full-scale development thereafter are covered by scientific research grants and private research grants. Please see <a href="/award">Awards and Grants</a> for the funding received so far.',
          'If you\'re interested in supporting us, contact us at <a href="/contact">contact</a>. We are more than happy to know about your suggestions and ideas.',
        ],
      },
      {
        subtitle: 'External contributors',
        content: [
          'The following people cooperate in evaluating the operation of the system and conducting load tests assuming large-scale experiments. (4th generation only)',
          '<ul><li>Prof. Shunichiro Sasaki（Kindai University）</li><li>Dr. Shoko Yamane（Papalaca Research Institute Co., Ltd.）</li><li>Mr. Kampei Hayashi（Shinshu University）</li><li>Prof. Masao Ogaki（Keio University）</li></ul>',
        ],
      },
    ],
  },
  {
    title: '4th generation',
    subSession: [
      {
        subtitle: 'What’s new',
        content: [
          '<ul><li>Support for different devices (responsive web design)</li><li>Multi-language environment</li><li>Improved user interface (UI).</li><li>Easier to conduct multiple experiments</li></ul>',
        ],
      },
      {
        subtitle: 'What you can do',
        content: [
          '<ul><li>Cross-cultural interactive experiments</li><li>Outdoor experiments in everyday situations</li><li>Economic experiments and education in developing countries</li>',
        ],
      },
    ],
  },
  {
    title: 'Recruiting contributors',
    subSession: [
      {
        subtitle: 'Translators',
        content: [
          'We need contributors who help us localise the system. Translating only text files in English into your language. This is really easy.',
          'Translators are not rewarded, but display their names on the <a href="/developer">developers</a> page as contributors.',
        ],
      },
      {
        subtitle: 'Tester',
        content: [
          'We need a tester who will report usage problems under typical situations such as in your class, the lab, or outdoors.',
          'If you find any problem, please feel free to <a href="/contact">contact</a> us. We will fix it.',
        ],
      },
    ],
  },
];

export const esAbout = [
  {
    title: 'Instrucciones',
    subSession: [
      {
        subtitle: 'Forma de uso',
        content: [
          'Visita la página de <a href="/usage">Instrucciones</a> para aprender acerca de XEE.',
        ],
      },
      {
        subtitle: 'Juegos',
        content: [
          'Puedes ver la lista de juegos en el sitio de <a href="/gameList">Juegos Disponibles</a>. También puedes consultar esta lista al entrar a un cuarto como anfitrión.',
        ],
      },
    ],
  },
  {
    title: 'Ventajas de nuestra plataforma',
    subSession: [
      {
        subtitle: '',
        content: [
          'XEE ha sido diseñado para ser utilizado en la clase y es totalmente gratuito. Sus beneficios incluyen:',
        ],
      },
      {
        subtitle: 'Facilidad de uso',
        content: [
          'Iniciar un experimento requiere menos de un minuto, y la forma de uso es fácil de entender. Normalmente la configuración inicial de la mayoría de los juegos es suficiente. Los profesores pueden iniciar un experimento con solo presionar un botón, sin causar distracciones.',
        ],
      },
      {
        subtitle: 'Transmisión eficiente de datos',
        content: [
          'XEE puede ser utilizado en smartphones, y requiere muy poco ancho de banda. Gracias a esto, los experimentos pueden realizarse con conectividad limitada o lenta, lo cual se traduce en un menor costo para los estudiantes. Logramos esto actualizando solo los datos necesarios en lugar de recargar la página completa.',
        ],
      },
      {
        subtitle: 'Estabilidad y velocidad',
        content: [
          'Gracias a que usamos <a href="https://elixir-lang.org/">Elixir</a> como lenguaje del lado del servidor, los cálculos son más simples y eficientes, y no hay problemas de rendimiento incluso si hay más de 1,000 estudiantes usando el sistema simultáneamente.',
        ],
      },
    ],
  },
  {
    title: 'Contexto',
    subSession: [
      {
        subtitle: 'Experimentos educativos',
        content: [
          'La experimentación en Economía se utiliza no solo en el laboratorio por propósitos de investigación, sino también en salones de clase por propósitos pedagógicos. Los experimentos que se realizan con el propósito de mejorar la calidad del aprendizaje se llaman experimentos educativos.',
          'Los profesores utilizan normalmente herramientas análogas, como el levantar la mano, papel y lápiz, tarjetas, dados, etiquetas, etc. Herramientas digitales, tales como computadoras portátiles o de escritorio, tabletas, teléfonos inteligentes, etc. son utilizados también.',
          'Los profesores y estudiantes aman realizar experimentos porque son divertidos. Especialmente los estudiantes prefieren los experimentos porque las clases típicas son muy aburridas. En contraste, las clases que incluyen experimentos son emocionantes.',
          'Además, los profesores utilizan los experimentos para mejorar el entendimiento de los estudiantes sobre los temas de la clase, o para ayudar a demostrar hechos científicos. Los experimentos son también utilizados para romper el hielo al inicio de la clase.',
        ],
      },
      {
        subtitle: 'Problemas cuando se realizan experimentos educativos',
        content: [
          'Realizar experimentos en el salón de clase no es fácil para los profesores. Existe varias complicaciones.',
          'Primero hay que considerar el costo de preparar los experimentos. Los profesores tienen que imprimir cuestionarios, y conseguir cartas, dados y computadoras antes de la clase. Además los profesores deben organizar la ejecución de los experimentos. El tiempo que se dedica a estas actividades es tiempo que los profesores no utilizan en la clase. Además es necesario mantener en secreto las respuestas de los estudiantes. Por esta razón, los profesores prefieren el formato tradicional de clase en lugar de los experimentos.',
          'También existe la complicación de ejecutar los experimentos correctamente. Es necesario dividir a los estudiantes en grupos aleatorios para evitar el sesgo de auto-selección, pero para esto se necesitan herramientas. El salón de clase es un espacio libre para los estudiantes, lo que puede llevar a un ambiente desorganizado y hace difícil para los profesores el mantener en secreto la información privada.',
          'Incluso si estos problemas se resuelven, no existe garantía que los estudiantes participarán activamente en el experimento y harán su mejor esfuerzo.',
        ],
      },
      {
        subtitle: 'Problemas al llevar a cabo experimentos educativos utilizando computadoras',
        content: [
          'Utilizar computadoras ayuda a resolver la mayoría de los problemas al realizar experimentos educativos. Sin embargo, también conlleva otras complicaciones.',
          'Para realizar experimentos con computadoras, necesitas dispositivos y software.',
          'Existen varias servicios de software para experimentos. Sin embargo, algunos de ellos requieren una subscripción pagada o entrar en un contrato con los autores. Incluso si el software es gratuito, existe la dificultad de provisionar las computadoras de los estudiantes y el servidor.',
          'Además, se necesita cierto nivel de conocimiento sobre programación para configurar los experimentos.',
        ],
      },
    ],
  },
  {
    title: 'Equipo de desarrollo',
    subSession: [
      {
        subtitle: 'Miembros del equipo',
        content: [
          'El Dr. Ryohei Hayashi desarrolló la primera versión del sistema XEE en 2012. Después de esto, estudiantes del  <a href="http://www.kagoshima-ct.ac.jp/english/">Instituto Nacional de Tecnología de Kagoshima</a> jugaron un rol importante al mejorar la generalidad, robustez y el rendimiento del sistema. En la actualidad el desarrollo está en manos de ex-alumnos del Instituto.',
          'Puedes conocer más acerca de los miembros del equipo de desarrollo en la sección de <a href="/developer">Desarrolladores</a>. Si tu compañía está en busca de programadores capaces, siéntete en la libertad de contactarlos.',
        ],
      },
      {
        subtitle: 'Fondos de desarrollo',
        content: [
          'Fondos para el desarrollo de la primera versión fueron provistos por la <a href="https://www.kindai.ac.jp/economics/">Facultad de Economía de la Universidad Kindai</a>.',
          'Los fondos posteriores para el desarrollo provienen de premios a la investigación científica y fondos privados de investigación. Puedes visitar la sección de <a href="/award">Premios y fondos de investigación</a> para conocer más.',
          'Si te interesa apoyarnos, estaremos más que felices de escuchar tus ideas y propuestas a través de la página de <a href="/contact">contacto</a>.',
        ],
      },
      {
        subtitle: 'Colaboradores externos',
        content: [
          'Las siguientes personas nos hay ayudado en la evaluación de la operación del sistema y desarrollando pruebas de rendimiento en casos de experimentos a gran escala (solamente la cuarta generation del sistema)',
          '<ul><li>Prof. Shunichiro Sasaki（Universidad Kindai）</li><li>Dr. Shoko Yamane（Papalaka Research Institute Co., Ltd.）</li><li>Mr. Kampei Hayashi（Universidad Shinshu）</li><li>Prof. Masao Ogaki（Universidad Keio）</li></ul>',
        ],
      },
    ],
  },
  {
    title: 'La cuarta generación',
    subSession: [
      {
        subtitle: 'Qué hay de nuevo',
        content: [
          '<ul><li>Diseño web adaptable</li><li>Soporte para varios idiomas</li><li>Mejoras en la interfaz de usuario (UI).</li><li>Facilidad para conducir múltiples experimentos</li></ul>',
        ],
      },
      {
        subtitle: 'Lo que puedes hacer',
        content: [
          '<ul><li>Experimentos interactivos multiculturales</li><li>Experimentos al aire libre en situaciones de la vida cotidiana</li><li>Experimentos económicos y educativos en países en desarrollo</li>',
        ],
      },
    ],
  },
  {
    title: 'Reclutamiento de colaboradores',
    subSession: [
      {
        subtitle: 'Traductores',
        content: [
          'Necesitamos colaboradores que nos ayuden a adaptar el sistema a otros lenguajes. Es muy fácil contribuir traduciendo simples archivos de texto a tu lenguaje.',
          'Los traductores no reciben una paga, pero sus nombres se muestran en la página de <a href="/developer">desarrolladores</a> como colaboradores.',
        ],
      },
      {
        subtitle: 'Desarrollo de pruebas',
        content: [
          'Necesitamos desarrolladores para ejecutar pruebas y reportar problemas al utilizar el sistema en situaciones reales, tales como en tu clase, laboratorio o al aire libre.',
          'Si encuentras algún problema, por favor <a href="/contact">contáctanos</a> y lo arreglaremos.',
        ],
      },
    ],
  },
];
