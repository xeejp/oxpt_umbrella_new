export const jaGameList = [
  {
    name: '美人投票ゲーム',
    category: '行動経済学',
    guestAmount: '2-',
    updateDate: '2020年5月6日',
    repository: 'https://gitlab.com/xeejp/oxpt_beauty_contest',
  },
  {
    name: 'ボイラープレート',
    category: 'その他',
    guestAmount: '2-',
    updateDate: '2020年5月6日',
    repository: 'https://gitlab.com/xeejp/oxpt_boilerplate',
  },
  {
    name: 'チャット',
    category: 'その他',
    guestAmount: '1-',
    updateDate: '2020年5月6日',
    repository: 'https://gitlab.com/xeejp/oxpt_chat',
  },
  {
    name: 'ダブルオークション',
    category: '市場メカニズム',
    guestAmount: '2-',
    updateDate: '2020年5月6日',
    repository: 'https://gitlab.com/xeejp/oxpt_doubleauction',
  },
  {
    name: '公共財ゲーム',
    category: 'ゲーム理論',
    guestAmount: '4-',
    updateDate: '2020年5月6日',
    repository: 'https://gitlab.com/xeejp/oxpt_public_goods',
  },
  {
    name: '最後通牒ゲーム',
    category: '行動経済学',
    guestAmount: '2-',
    updateDate: '2020年5月6日',
    repository: 'https://gitlab.com/xeejp/oxpt_ultimatum_game',
  },
];

export const enGameList = [
  {
    name: 'Beauty Contest Game',
    category: 'Behavioral economics',
    guestAmount: '2-',
    updateDate: 'May 6, 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_beauty_contest',
  },
  {
    name: 'Boilerplate',
    category: 'Other',
    guestAmount: '2-',
    updateDate: 'May 6, 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_boilerplate',
  },
  {
    name: 'Chat',
    category: 'Other',
    guestAmount: '1-',
    updateDate: 'May 6, 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_chat',
  },
  {
    name: 'Double Auction',
    category: 'Market mechanisms',
    guestAmount: '2-',
    updateDate: 'May 6, 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_doubleauction',
  },
  {
    name: 'Public Goods',
    category: 'Game theory',
    guestAmount: '4-',
    updateDate: 'May 6, 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_public_goods',
  },
  {
    name: 'Ultimatum Game',
    category: 'Behavioral economics',
    guestAmount: '2-',
    updateDate: 'May 6, 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_ultimatum_game',
  },
];

export const esGameList = [
  {
    name: 'Concurso de Belleza',
    category: 'Economía Conductual',
    guestAmount: '2-',
    updateDate: '6 de mayo de 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_beauty_contest',
  },
  {
    name: 'Plantilla',
    category: 'Otros',
    guestAmount: '2-',
    updateDate: '6 de mayo de 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_boilerplate',
  },
  {
    name: 'Conversación',
    category: 'Otros',
    guestAmount: '1-',
    updateDate: '6 de mayo de 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_chat',
  },
  {
    name: 'Doble Subasta',
    category: 'Mecanismos de Mercado',
    guestAmount: '2-',
    updateDate: '6 de mayo de 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_doubleauction',
  },
  {
    name: 'Bienes Públicos',
    category: 'Teoría de Juegos',
    guestAmount: '4-',
    updateDate: '6 de mayo de 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_public_goods',
  },
  {
    name: 'Juego del Ultimátum',
    category: 'Behavioral economics',
    guestAmount: '2-',
    updateDate: '6 de mayo de 2020',
    repository: 'https://gitlab.com/xeejp/oxpt_ultimatum_game',
  },
];
