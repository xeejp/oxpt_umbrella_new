export const jaCommon = {
  loading: '読み込み中 . . .',
  title: 'XEE.JP',
};

export const enCommon = {
  loading: 'Loading . . .',
  title: 'XEE.JP',
};

export const esCommon = {
  loading: 'Cargando . . .',
  title: 'XEE.JP',
};
