import i18n from 'i18next';
import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

import { enCommon, jaCommon, esCommon } from './locales/Common';
import { enComponents, jaComponents, esComponents } from './locales/Components';

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources: {
      en: {
        common: enCommon,
        components: enComponents,
      },
      ja: {
        common: jaCommon,
        components: jaComponents,
      },
      es: {
        common: esCommon,
        components: esComponents,
      },
    },

    fallbackLng: ['ja'],
    interpolation: {
      escapeValue: false, // react already safes from xss
    },

    // have a common namespace used around the full app
    ns: ['common', 'components'],
    defaultNS: 'components',

    debug: true,

    react: {
      useSuspense: false,
    },
  });

export default i18n;
