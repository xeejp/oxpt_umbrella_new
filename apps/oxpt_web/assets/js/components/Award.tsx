import React from 'react';
import { useTranslation } from 'react-i18next';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';

import Timeline from '@material-ui/lab/Timeline';
import TimelineItem from '@material-ui/lab/TimelineItem';
import TimelineSeparator from '@material-ui/lab/TimelineSeparator';
import TimelineConnector from '@material-ui/lab/TimelineConnector';
import TimelineContent from '@material-ui/lab/TimelineContent';
import TimelineDot from '@material-ui/lab/TimelineDot';
import TimelineOppositeContent from '@material-ui/lab/TimelineOppositeContent';
import LinkIcon from '@material-ui/icons/Link';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import SchoolIcon from '@material-ui/icons/School';
import { jaAwards, enAwards, esAwards } from '../locales/Award';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
  awardTitle: {
    padding: theme.spacing(1),
    display: 'inline-block',
  },
  timelineContentLeft: {
    marginRight: theme.spacing(2),
    maxWidth: '62.07px',
  },
  timelineContentRight: {
    padding: theme.spacing(1),
  },
  description1: {
    paddingLeft: theme.spacing(4),
  },
  description2: {
    paddingLeft: theme.spacing(4),
    marginBottom: theme.spacing(4),
  },
}));

function Award() {
  const classes = useStyles();
  const { t, i18n } = useTranslation('components');

  let awardContent = jaAwards;
  switch (i18n.language) {
    case 'ja':
      awardContent = jaAwards;
      break;
    case 'en':
      awardContent = enAwards;
      break;
    case 'es':
      awardContent = esAwards;
      break;
    default:
      break;
  }

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={8}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
        >
          <Typography variant="h3" gutterBottom className={classes.title}>{t('award.title')}</Typography>
          <Divider className={classes.divider} />
          <Typography variant="h5" gutterBottom className={classes.title}>{t('award.subtitle')}</Typography>
          <Timeline className={classes.awardTimeline}>
            {awardContent.map((value) => (
              <TimelineItem key={`award-${value.title}`}>
                <TimelineOppositeContent color="textSecondary" className={classes.timelineContentLeft}>
                  <Typography variant="h6" className={classes.awardTitle}>{value.year}</Typography>
                </TimelineOppositeContent>
                <TimelineSeparator>
                  {(value.type === 'grant') && (
                  <TimelineDot color="primary">
                    <AttachMoneyIcon />
                  </TimelineDot>
                  )}
                  {(value.type === 'award') && (
                  <TimelineDot color="secondary">
                    <SchoolIcon />
                  </TimelineDot>
                  )}
                  <TimelineConnector />
                </TimelineSeparator>
                <TimelineContent className={classes.timelineContentRight}>
                  <Typography variant="h6" className={classes.awardTitle} dangerouslySetInnerHTML={{ __html: value.title.toString() }} />
                  <IconButton aria-label="title-link" href={value.titleUrl}>
                    <LinkIcon fontSize="small" />
                  </IconButton>
                  <Typography className={classes.description1} variant="body1">
                    {value.description}
                    {' '}
                    (
                    {value.date}
                    )
                    <IconButton aria-label="title-link" href={value.descriptionUrl}>
                      <LinkIcon fontSize="small" />
                    </IconButton>
                  </Typography>
                  <Typography className={classes.description2} variant="body1">{value.author}</Typography>
                </TimelineContent>
              </TimelineItem>
            ))}
          </Timeline>
        </Grid>
      </Grid>
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

export default Award;
