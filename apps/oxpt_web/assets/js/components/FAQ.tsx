import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import 'animate.css';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { jaFAQ, enFAQ, esFAQ } from '../locales/FAQ';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: 0,
  },
  titleGrid: {
    padding: theme.spacing(1),
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
  accordion: {
    padding: theme.spacing(1),
  },
  answer: {
    textIndent: '1em',
    color: theme.palette.text.secondary,
  },
}));

function FAQ(props) {
  const classes = useStyles();
  const { t, i18n } = useTranslation('components');
  const { filter } = props;

  let faqContent = jaFAQ;
  switch (i18n.language) {
    case 'ja':
      faqContent = jaFAQ;
      break;
    case 'en':
      faqContent = enFAQ;
      break;
    case 'es':
      faqContent = esFAQ;
      break;
    default:
      break;
  }

  faqContent = faqContent.filter((element) => (element.filter.includes(filter)));

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Grid
        container
        direction="column"
        justifyContent="flex-start"
        alignItems="stretch"
        className={classes.titleGrid}
      >
        <Typography variant="h3" gutterBottom component="div" className={classes.title}>{t('faq.title')}</Typography>
        <Divider className={classes.divider} />
        <Typography variant="h5" gutterBottom component="div" className={classes.title}>{t('faq.subtitle')}</Typography>
      </Grid>
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="stretch"
        className={classes.accordion}
      >
        {faqContent.map((value) => (
          <Accordion key={value.title}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography>{value.title}</Typography>
            </AccordionSummary>
            <Divider />
            <AccordionDetails>
              {value.answer.map((subValue) => <Typography key={subValue.description} variant="body1" className={classes.answer} dangerouslySetInnerHTML={{ __html: subValue.description.toString() }} />)}
            </AccordionDetails>
          </Accordion>
        ))}
      </Grid>
    </Grid>
  );
}

FAQ.propTypes = {
  filter: PropTypes.string.isRequired,
};

export default FAQ;
