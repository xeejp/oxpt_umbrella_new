import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Link, useLocation } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import Chip from '@material-ui/core/Chip';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import Avatar from '@material-ui/core/Avatar';

import ListIcon from '@material-ui/icons/List';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import PlaylistPlayIcon from '@material-ui/icons/PlaylistPlay';

import LogoRight from '../images/logo_right.png';

const useStyles = makeStyles((theme) => ({
  root: {
    overflow: 'auto',
  },
  list: {
    width: 250,
  },
  listLink: {
    color: theme.palette.text.primary,
    textDecoration: 'none',
  },
  divider: {
    margin: theme.spacing(2),
  },
}));

function Menu(props) {
  const classes = useStyles();
  const { t } = useTranslation('components');
  const { setMenuOpen, menuOpen } = props;

  const ListItemLink = (state) => {
    const location = useLocation();
    const { to, icon, text } = state;

    return (
      <Link to={to} className={classes.listLink} onClick={() => setMenuOpen({ menuOpen: false })}>
        <ListItem button key={text} selected={location.pathname === to}>
          <ListItemIcon>{icon}</ListItemIcon>
          <ListItemText primary={text} />
        </ListItem>
      </Link>
    );
  };

  const MenuList = () => (
    <div
      className={classes.list}
      role="presentation"
    >
      <List>
        <ListItemLink to="/" text={t('menu.top')} icon={<Avatar alt={t('menu.top')} src={LogoRight} />} />
        <div className={classes.divider} />
        <Divider textAlign="left">
          <Chip label={t('menu.forGuest')} variant="contained" color="primary" size="small" />
        </Divider>
        <ListItemLink to="/room_list" text={t('menu.roomList')} icon={<ListIcon />} />
        <div className={classes.divider} />
        <Divider textAlign="left">
          <Chip label={t('menu.forHost')} variant="contained" color="secondary" size="small" />
        </Divider>
        <ListItemLink to="/create_room" text={t('menu.createRoom')} icon={<PlaylistAddIcon />} />
        <ListItemLink to="/room_management" text={t('menu.roomManagement')} icon={<PlaylistPlayIcon />} />
        <div className={classes.divider} />
        <Divider textAlign="left">
          <Chip label={t('menu.other')} variant="outlined" size="small" />
        </Divider>
        <ListItemLink to="/contact" text={t('menu.contact')} icon={<MailOutlineIcon />} />
      </List>
    </div>
  );

  const handleMenuOpen = () => {
    setMenuOpen({ menuOpen: true });
  };

  const handleMenuClose = () => {
    setMenuOpen({ menuOpen: false });
  };

  return (
    <div className={classes.root}>
      <SwipeableDrawer
        anchor="left"
        open={menuOpen}
        onClose={handleMenuClose}
        onOpen={handleMenuOpen}
      >
        <MenuList />
      </SwipeableDrawer>
    </div>
  );
}

Menu.propTypes = {
  setMenuOpen: PropTypes.func.isRequired,
  menuOpen: PropTypes.bool.isRequired,
};

export default Menu;
