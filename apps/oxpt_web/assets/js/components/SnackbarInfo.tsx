import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/core/Alert';
import AlertTitle from '@material-ui/core/AlertTitle';
import Slide from '@material-ui/core/Slide';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/core/styles';

const Alert = React.forwardRef((props, ref) => <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />);

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
  alert: {
    width: '100%',
  },
}));

function TransitionLeft(props) {
  return <Slide {...props} direction="left" />;
}

function SnackbarInfo(props) {
  const {
    snackbarTitle, snackbarMessage, snackbarSeverity, snackbarOpen, setSnackbar,
  } = props;
  const classes = useStyles();
  const { t } = useTranslation('components');

  const initialState = {
    snackbarTitle,
    snackbarMessage,
    snackbarSeverity,
    snackbarOpen: false,
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSnackbar(initialState);
  };

  const action = (
    <>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </>
  );

  return (
    <div className={classes.root}>
      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        open={snackbarOpen}
        autoHideDuration={2000}
        onClose={handleClose}
        action={action}
        TransitionComponent={TransitionLeft}
      >
        <Alert severity={snackbarSeverity} className={classes.alert}>
          <AlertTitle>{snackbarTitle !== '' && t(snackbarTitle)}</AlertTitle>
          {snackbarMessage !== '' && t(snackbarMessage)}
        </Alert>
      </Snackbar>
    </div>
  );
}

SnackbarInfo.propTypes = {
  snackbarTitle: PropTypes.string.isRequired,
  snackbarMessage: PropTypes.string.isRequired,
  snackbarSeverity: PropTypes.oneOf(['error', 'info', 'success', 'warning']).isRequired,
  snackbarOpen: PropTypes.bool.isRequired,
  setSnackbar: PropTypes.func.isRequired,
};

export default SnackbarInfo;
