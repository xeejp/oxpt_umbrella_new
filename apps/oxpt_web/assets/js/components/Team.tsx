import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import clsx from 'clsx';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Collapse from '@material-ui/core/Collapse';
import Button from '@material-ui/core/Button';

import School from '@material-ui/icons/School';
import Keyboard from '@material-ui/icons/Keyboard';
import Devices from '@material-ui/icons/Devices';
import Translate from '@material-ui/icons/Translate';
import BarChart from '@material-ui/icons/BarChart';
import Tooltip from '@material-ui/core/Tooltip';
import Paper from '@material-ui/core/Paper';
import Papalaka from '../images/papalaka.png';
import { jaMembers, enMembers, esMembers } from '../locales/Team';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
  teamTitle: {
    marginTop: theme.spacing(4),
  },
  teamDivider: {
    marginBottom: theme.spacing(1),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  member: {
    padding: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    textAlign: 'center',
  },
  icons: {
    padding: theme.spacing(1),
  },
  avatar: {
    margin: theme.spacing(1),
    padding: theme.spacing(2),
    width: 180,
    height: 180,
    backgroundColor: theme.palette.background.paper,
    '&:hover': {
      backgroundColor: theme.palette.secondary.light,
    },
  },
  memberDescription: {
    textIndent: '1em',
  },
  papalakaPaper: {
    padding: theme.spacing(1),
    justifyContent: 'center',
    textAlign: 'center',
    '&:hover': {
      backgroundColor: '#98ca98',
      color: theme.palette.secondary.contrastText,
    },
  },
  papalakaImage: {
    width: '100%',
  },
  papalakaDivider: {
    padding: theme.spacing(1),
    paddingTop: 0,
  },
  papalakaCaption: {
    padding: theme.spacing(1),
    paddingBottom: 0,
  },
  papalakaLink: {
    textDecoration: 'none',
    color: theme.palette.text.primary,
  },
}));

const Member = (props) => {
  const {
    index, image, link, name, role, description,
  } = props;
  const { t } = useTranslation('components');
  const classes = useStyles();
  const [expanded, setExpanded] = useState([]);

  const handleExpandClick = (i) => {
    const expandedCopy = expanded.slice();
    expandedCopy[i] = !expandedCopy[i];
    setExpanded(expandedCopy);
  };

  return (
    <Grid item xs={12} sm={3} className={classes.member}>
      <center><Avatar alt={name} src={image} className={classes.avatar} variant="outlined" /></center>
      <Typography variant="h6" gutterBottom noWrap>
        {name}
      </Typography>
      <IconButton
        className={clsx(classes.expand, {
          [classes.expandOpen]: expanded,
        })}
        onClick={() => handleExpandClick(index)}
        aria-expanded={expanded}
        aria-label="Show more"
      >
        <ExpandMoreIcon />
      </IconButton>
      <Collapse in={expanded[index]} timeout="auto" unmountOnExit>
        <Divider />
        {role.includes('PI')
        && (
        <Tooltip title={t('team.pi')} placement="top">
          <IconButton aria-label={t('team.pi')}>
            <School />
          </IconButton>
        </Tooltip>
        )}
        {role.includes('Core')
        && (
        <Tooltip title={t('team.core')} placement="top">
          <IconButton aria-label={t('team.core')}>
            <Keyboard />
          </IconButton>
        </Tooltip>
        )}
        {role.includes('UI')
        && (
        <Tooltip title={t('team.ui')} placement="top">
          <IconButton aria-label={t('team.ui')}>
            <Devices />
          </IconButton>
        </Tooltip>
        )}
        {role.includes('Expt')
        && (
        <Tooltip title={t('team.expt')} placement="top">
          <IconButton aria-label={t('team.expt')}>
            <BarChart />
          </IconButton>
        </Tooltip>
        )}
        {role.includes('Translate')
        && (
        <Tooltip title={t('team.translate')} placement="top">
          <IconButton aria-label={t('team.translate')}>
            <Translate />
          </IconButton>
        </Tooltip>
        )}
        {link && <Button variant="outlined" onClick={() => window.open(link)}>{t('team.link')}</Button>}
        <Typography align="justify" variant="body1" gutterBottom dangerouslySetInnerHTML={{ __html: description }} className={classes.memberDescription} />
      </Collapse>
    </Grid>
  );
};

function Team() {
  const classes = useStyles();
  const { t, i18n } = useTranslation('components');

  let memberContent = jaMembers;
  switch (i18n.language) {
    case 'ja':
      memberContent = jaMembers;
      break;
    case 'en':
      memberContent = enMembers;
      break;
    case 'es':
      memberContent = esMembers;
      break;
    default:
      break;
  }

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={8}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
        >
          <Typography variant="h3" gutterBottom className={classes.title}>{t('team.title')}</Typography>
          <Divider className={classes.divider} />
          <Typography variant="h5" gutterBottom className={classes.title}>{t('team.subtitle')}</Typography>
          <Typography variant="h4" gutterBottom className={classes.teamTitle}>{t('team.systemTeam')}</Typography>
          <Divider className={classes.teamDivider} />
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="flex-start"
          >
            {memberContent.map((members, index) => (
              <Member
                key={members.name}
                index={index}
                image={members.image}
                name={members.name}
                link={members.link}
                classes={classes}
                role={members.role}
                description={members.description}
              />
            ))}
          </Grid>
          <Typography variant="h4" gutterBottom className={classes.teamTitle}>{t('team.translateTeam')}</Typography>
          <Divider className={classes.teamDivider} />
          <Grid
            container
            direction="row"
            justifyContent="center"
            alignItems="flex-start"
          >
            <Grid item xs={12} sm={3}>
              <Paper className={classes.papalakaPaper} elevation={0}>
                <a href="http://papalaka.com/" className={classes.papalakaLink}>
                  <img src={Papalaka} alt={t('team.papalaka')} className={classes.papalakaImage} />
                  <Divider className={classes.papalakaDivider} />
                  <Typography variant="body1" gutterBottom className={classes.papalakaCaption}>{t('team.papalakaCaption')}</Typography>
                </a>
              </Paper>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

Member.propTypes = {
  index: PropTypes.number.isRequired,
  image: PropTypes.string,
  link: PropTypes.string,
  name: PropTypes.string,
  role: PropTypes.arrayOf(
    PropTypes.string,
  ),
  description: PropTypes.string,
};

Member.defaultProps = {
  image: '',
  link: '',
  name: '',
  role: [],
  description: '',
};

export default Team;
