import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Link, useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TranslateIcon from '@material-ui/icons/Translate';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import Tooltip from '@material-ui/core/Tooltip';
import Slide from '@material-ui/core/Slide';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import { Divider } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    zIndex: theme.zIndex.drawer + 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  accountButtonOut: {
    color: theme.palette.primary.light,
  },
  accountButtonIn: {
    color: theme.palette.primary.contrastText,
  },
  title: {
    flexGrow: 1,
  },
  titleLink: {
    color: theme.palette.primary.contrastText,
    textDecoration: 'none',
  },
}));

function HideOnScroll(props) {
  const { children } = props;
  const trigger = useScrollTrigger();

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

function Header(props) {
  const classes = useStyles();
  const history = useHistory();
  const {
    setMenuOpen, inputDisabled, setSnackbar, setNetworkStatus, setInputDisabled,
  } = props;
  const { t, i18n } = useTranslation('components');
  const [anchorAccount, setAnchorAccount] = useState(null);
  const [anchorTranslate, setAnchorTranslate] = useState(null);
  const timerRef = useRef();

  const handleMenuOpen = () => {
    if (!inputDisabled) {
      setMenuOpen({ menuOpen: true });
    }
  };

  const handleAccountOpen = (event) => {
    if (!inputDisabled) {
      setAnchorAccount(event.currentTarget);
    }
  };

  const handleAccountClose = () => {
    setAnchorAccount(null);
  };

  const handleSignUp = () => {
    history.push('/account');
    setAnchorAccount(null);
  };

  const handleSignOut = () => {
    setSnackbar({
      snackbarTitle: 'infoSnackbar.signOut.title',
      snackbarMessage: 'infoSnackbar.signOut.success',
      snackbarSeverity: 'success',
      snackbarOpen: true,
    });
    timerRef.current = window.setTimeout(() => {
      setNetworkStatus({ networkStatus: 'default' });
      setInputDisabled({ inputDisabled: false });
      history.push('/delete_account_token');
      window.location.reload(false);
    }, 2300);
    setAnchorAccount(null);
  };

  const handleDeleteAccount = () => {
    history.push('/delete_account');
    setAnchorAccount(null);
  };

  const handleTranslateOpen = (event) => {
    if (!inputDisabled) {
      setAnchorTranslate(event.currentTarget);
    }
  };

  const handleTranslate = (lang) => {
    i18n
      .changeLanguage(lang)
      .then(() => {
        setAnchorTranslate(null);
      });
  };

  const handleTranslateClose = () => {
    setAnchorTranslate(null);
  };

  return (
    <HideOnScroll {...props}>
      <AppBar position="static" className={classes.root}>
        <Toolbar>
          <Tooltip title={t('header.menu')} arrow placement="right">
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
              aria-label={t('header.menu')}
              onClick={handleMenuOpen}
            >
              <MenuIcon />
            </IconButton>
          </Tooltip>
          <Typography variant="h6" className={classes.title} component="div">
            <Link to="/" className={classes.titleLink}>XEE.JP</Link>
          </Typography>
          <Tooltip title={t('header.langSelect')} arrow placement="left">
            <Button
              edge="end"
              color="inherit"
              aria-label={t('header.langSelect')}
              onClick={handleTranslateOpen}
              startIcon={<TranslateIcon />}
              endIcon={<KeyboardArrowDownIcon />}

            >
              {i18n.language === 'ja' && t('header.japanese')}
              {i18n.language === 'en' && t('header.english')}
              {i18n.language === 'es' && t('header.spanish')}

            </Button>
          </Tooltip>
          <Tooltip title={t('header.account')} arrow placement="left">
            <IconButton
              edge="end"
              color="inherit"
              onClick={handleAccountOpen}
              aria-label="account"
            >
              {(window.username !== '') ? <AccountCircleIcon className={classes.accountButtonIn} id="signed"/> : <AccountCircleIcon className={classes.accountButtonOut} id="not-signed"/>}
            </IconButton>
          </Tooltip>
          <Menu
            id="translate-items"
            anchorEl={anchorTranslate}
            keepMounted
            open={Boolean(anchorTranslate)}
            onClose={handleTranslateClose}
            getContentAnchorEl={null}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
            <MenuItem onClick={() => handleTranslate('ja')}>{t('header.japanese')}</MenuItem>
            <MenuItem onClick={() => handleTranslate('en')}>{t('header.english')}</MenuItem>
            <MenuItem onClick={() => handleTranslate('es')}>{t('header.spanish')}</MenuItem>
          </Menu>
          <Menu
            id="account-items"
            anchorEl={anchorAccount}
            keepMounted
            open={Boolean(anchorAccount)}
            onClose={handleAccountClose}
            getContentAnchorEl={null}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
          >
            {(window.username !== '')
              ? (
                <div>
                  <MenuItem onClick={handleDeleteAccount}>{window.username}</MenuItem>
                  <Divider />
                  <MenuItem onClick={handleSignOut}>{t('header.signOut')}</MenuItem>
                </div>
              )
              : (
                <MenuItem onClick={handleSignUp}>
                  {t('header.signUp')}
                  {' '}
                  /
                  {' '}
                  {t('header.signIn')}
                </MenuItem>
              )}
          </Menu>
        </Toolbar>
      </AppBar>
    </HideOnScroll>
  );
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
};

Header.propTypes = {
  setMenuOpen: PropTypes.func.isRequired,
  inputDisabled: PropTypes.bool.isRequired,
  setSnackbar: PropTypes.func.isRequired,
  setNetworkStatus: PropTypes.func.isRequired,
  setInputDisabled: PropTypes.func.isRequired,
};

export default Header;
