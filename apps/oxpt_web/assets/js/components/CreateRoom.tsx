import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import clsx from 'clsx';
import 'animate.css';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import LoadingButton from '@material-ui/lab/LoadingButton';
import InputAdornment from '@material-ui/core/InputAdornment';

import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import ErrorIcon from '@material-ui/icons/Error';
import CheckIcon from '@material-ui/icons/Check';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CircularProgress from '@material-ui/core/CircularProgress';
import FAQ from '../containers/FAQ';

const inputContent = {
  default: { status: 'success', icon: null, helperText: 'top.inputDefault' },
  success: { status: 'success', icon: <CheckCircleIcon />, helperText: 'top.inputSuccess' },
  error: { status: 'error', icon: <ErrorIcon />, helperText: 'top.inputError' },
  loading: { status: 'loading', icon: <CheckIcon />, helperText: 'top.inputLoading' },
  checked: { status: 'checked', icon: <CheckIcon />, helperText: 'top.inputChecked' },
};

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: 0,
  },
  titleGrid: {
    padding: theme.spacing(1),
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
  buttonItem: {
    padding: theme.spacing(2),
    margin: theme.spacing(1),
  },
  formItem: {
    margin: theme.spacing(1),
  },
  iconChecked: {
    color: theme.palette.text.primary,
  },
  iconSuccess: {
    color: theme.palette.success.main,
  },
  iconError: {
    color: theme.palette.error.main,
  },
}));

function CreateRoom(props) {
  const classes = useStyles();
  const { t } = useTranslation('components');
  const [localRoomKey, setLocalRoomKey] = useState('');
  const [roomKeyStatus, setRoomKeyStatus] = useState('default');
  const [localPersists, setLocalPersists] = useState(false);
  const {
    setSnackbar,
    requestCreateRoom,
    inputDisabled,
    setInputDisabled,
    networkStatus,
    setNetworkStatus,
  } = props;
  const history = useHistory();
  const timerRef = useRef();

  const isSignIn = (window.accountToken !== '');

  const handlePersistsChange = () => {
    setLocalPersists(!localPersists);
  };

  useEffect(() => {
    if (networkStatus === 'success') {
      setRoomKeyStatus('success');
      timerRef.current = window.setTimeout(() => {
        setNetworkStatus({ networkStatus: 'default' });
        setInputDisabled({ inputDisabled: false });
        history.push('/room');
      }, 2300);
    }

    return () => {
      clearTimeout(timerRef.current);
    };
  }, [networkStatus]);

  const iconRoomKeyClassName = clsx({
    [classes.iconChecked]: roomKeyStatus === 'checked',
    [classes.iconSuccess]: roomKeyStatus === 'success',
    [classes.iconError]: roomKeyStatus === 'error',
  });

  function ValidationRoomKey(roomKey) {
    let errorMessage;
    if (roomKey.trim().length === 0) {
      errorMessage = 'infoSnackbar.createRoom.error.roomKeyBlank';
    } else {
      errorMessage = 'success';
    }
    return errorMessage;
  }

  const handleCreateRoom = () => {
    const errorMessageRoomKey = ValidationRoomKey(localRoomKey);
    if (errorMessageRoomKey !== 'success') {
      setRoomKeyStatus('error');
      setSnackbar({
        snackbarTitle: 'infoSnackbar.createRoom.title',
        snackbarMessage: errorMessageRoomKey,
        snackbarSeverity: 'error',
        snackbarOpen: true,
      });
    }

    if (errorMessageRoomKey === 'success' && networkStatus !== 'transferring') {
      setRoomKeyStatus('checked');
      setInputDisabled({ inputDisabled: true });
      setNetworkStatus({ networkStatus: 'transferring' });
      const params = {
        roomKey: localRoomKey,
        persists: localPersists,
      };
      requestCreateRoom(params);
    }
  };

  const handleRoomKey = (event) => {
    setLocalRoomKey(event.target.value.trim());
    if (event.target.value !== '') {
      if (ValidationRoomKey(localRoomKey) === 'success') {
        setRoomKeyStatus('default');
      } else {
        setRoomKeyStatus('error');
      }
    }
  };

  const handleRoomKeyInputBlur = (event) => {
    if (roomKeyStatus !== 'success') {
      if (event.target.value !== '') {
        if (ValidationRoomKey(localRoomKey) === 'success') {
          setRoomKeyStatus('checked');
        } else {
          setRoomKeyStatus('error');
        }
      } else {
        setRoomKeyStatus('default');
      }
    }
  };

  const handleInputKeyDown = (event) => {
    const ENTER = 13;
    if (event.keyCode === ENTER && !inputDisabled) {
      handleCreateRoom();
    }
  };

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={4}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
          className={classes.titleGrid}
        >
          <Typography variant="h3" gutterBottom component="div" className={classes.title}>{t('createRoom.title')}</Typography>
          <Divider className={classes.divider} />
          <Typography variant="h5" gutterBottom component="div" className={classes.title}>{t('createRoom.subtitle')}</Typography>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl fullWidth>
            <TextField
              id="room-name"
              label={t('createRoom.roomKeyLabel')}
              autoFocus
              className={classes.formItem}
              error={networkStatus === 'error' || roomKeyStatus === 'error'}
              disabled={inputDisabled}
              InputProps={{
                readOnly: false,
                endAdornment: (
                  <InputAdornment position="end" className={iconRoomKeyClassName}>
                    { inputContent[roomKeyStatus].icon}
                  </InputAdornment>
                ),
              }}
              onChange={(event) => handleRoomKey(event)}
              onBlur={(event) => handleRoomKeyInputBlur(event)}
              onKeyDown={(event) => handleInputKeyDown(event)}
              value={localRoomKey}
            />
            {isSignIn && (
            <FormControlLabel
              control={<Checkbox checked={localPersists} onChange={handlePersistsChange} name="PersistsChecked" color="primary" />}
              label={t('createRoom.persists')}
              className={classes.formItem}
              disabled={inputDisabled}
            />
            )}
            <LoadingButton
              pending={networkStatus === 'transferring'}
              className={classes.buttonItem}
              pendingPosition="end"
              variant="contained"
              size="large"
              onClick={handleCreateRoom}
              disabled={inputDisabled}
              pendingIndicator={
                <CircularProgress color="inherit" size={24} />
              }
              endIcon={<div />}
            >
              {t('createRoom.create')}
            </LoadingButton>
          </FormControl>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={4}>
        <FAQ filter="create_room" />
      </Grid>
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

CreateRoom.propTypes = {
  setSnackbar: PropTypes.func.isRequired,
  requestCreateRoom: PropTypes.func.isRequired,
  inputDisabled: PropTypes.bool.isRequired,
  setInputDisabled: PropTypes.func.isRequired,
  networkStatus: PropTypes.oneOf(['default', 'success', 'error', 'transferring']).isRequired,
  setNetworkStatus: PropTypes.func.isRequired,
};

export default CreateRoom;
