import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import clsx from 'clsx';
import 'animate.css';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import Skeleton from '@material-ui/core/Skeleton';
import LoadingButton from '@material-ui/lab/LoadingButton';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: 0,
  },
  titleGrid: {
    padding: theme.spacing(1),
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  roomGrid: {
    padding: theme.spacing(1),
  },
  roomButton: {
    padding: theme.spacing(2),
    color: theme.palette.text.primary,
    backgroundColor: theme.palette.primary.contrastText,
    '&:hover': {
      color: theme.palette.secondary.contrastText,
      backgroundColor: theme.palette.primary.main,
    },
  },
  skeleton: {
    padding: theme.spacing(1),
    borderRadius: theme.shape.borderRadius,
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
}));

function RoomList(props) {
  const classes = useStyles();
  const { t } = useTranslation('components');
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  const [joinRoom, setJoinRoom] = useState(false);
  const [selectedRoomKey, setSelectedRoomKey] = useState(null);
  const timerRef = useRef();
  const {
    requestGuestRoomList,
    setNetworkStatus,
    guestRoomList,
    networkStatus,
    setInputDisabled,
    joinRoomAsGuest,
    inputDisabled,
  } = props;

  const isGuestRoom = Object.values(guestRoomList).length;

  useEffect(() => {
    if (networkStatus === 'success') {
      setLoading(false);
      if (joinRoom) {
        timerRef.current = window.setTimeout(() => {
          setNetworkStatus({ networkStatus: 'default' });
          setInputDisabled({ inputDisabled: false });
          history.push('/room');
        }, 2300);
      } else {
        setNetworkStatus({ networkStatus: 'default' });
        setInputDisabled({ inputDisabled: false });
      }
    }

    return () => {
      clearTimeout(timerRef.current);
    };
  }, [networkStatus]);

  const roomClassName = clsx(classes.roomButton, 'animate__animated animate__fadeIn');

  const handleRequestGuestRoomList = () => {
    setLoading(!loading);
    requestGuestRoomList();
  };

  useEffect(() => {
    handleRequestGuestRoomList();
  }, []);

  const hadleJoinRoomAsGuest = (roomKey) => {
    setSelectedRoomKey(roomKey);
    setJoinRoom(true);
    setNetworkStatus({ networkStatus: 'transferring' });
    setInputDisabled({ inputDisabled: true });
    joinRoomAsGuest({ roomKey });
  };

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={8}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
          className={classes.titleGrid}
        >
          <Typography variant="h3" gutterBottom component="div" className={classes.title}>{t('roomList.title')}</Typography>
          <Divider className={classes.divider} />
          {(isGuestRoom === 0) && <Typography variant="h5" gutterBottom component="div" className={classes.title}>{t('roomList.noGuestRoom')}</Typography>}
        </Grid>
      </Grid>
      <Grid item xs={12} sm={8}>
        <Grid
          container
          direction="row"
          justifyContent="left"
          alignItems="flex-start"
        >
          {loading
            ? [1, 2, 3, 4].map((value) => <Grid item key={value} xs={12} sm={3} className={classes.roomGrid}><Skeleton variant="rectangular" width="100%" height={56} animation="wave" className={classes.skeleton} /></Grid>)
            : Object.values(guestRoomList).map((value) => (
              <Grid item key={value.room_key} xs={12} sm={3} className={classes.roomGrid}>
                <LoadingButton
                  pending={selectedRoomKey === value.room_key}
                  disabled={inputDisabled}
                  pendingPosition="end"
                  fullWidth
                  variant="contained"
                  size="large"
                  pendingIndicator={
                    <CircularProgress color="inherit" size={24} />
                  }
                  className={roomClassName}
                  onClick={() => { hadleJoinRoomAsGuest(value.room_key); }}
                  endIcon={<div />}
                >
                  {value.room_key}
                </LoadingButton>
              </Grid>
            ))}
        </Grid>
      </Grid>
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

RoomList.propTypes = {
  requestGuestRoomList: PropTypes.func.isRequired,
  setNetworkStatus: PropTypes.func.isRequired,
  guestRoomList: PropTypes.arrayOf(
    PropTypes.exact({
      room_id: PropTypes.string.isRequired,
      room_key: PropTypes.string.isRequired,
    }),
  ).isRequired,
  networkStatus: PropTypes.oneOf(['default', 'success', 'error', 'transferring']).isRequired,
  setInputDisabled: PropTypes.func.isRequired,
  joinRoomAsGuest: PropTypes.func.isRequired,
  inputDisabled: PropTypes.bool.isRequired,
};

export default RoomList;
