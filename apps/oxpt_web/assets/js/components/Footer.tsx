import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { Link as RouterLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';

import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';
import Chip from '@material-ui/core/Chip';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import Link from '@material-ui/core/Link';

import GroupIcon from '@material-ui/icons/Group';
import TwitterIcon from '@material-ui/icons/Twitter';
import MailIcon from '@material-ui/icons/Mail';
import DescriptionIcon from '@material-ui/icons/Description';
import ListIcon from '@material-ui/icons/List';
import SchoolIcon from '@material-ui/icons/School';
import HelpIcon from '@material-ui/icons/Help';
import RuleIcon from '@material-ui/icons/Rule';

import Icon from '@material-ui/core/Icon';
import LogoLeft from '../images/logo_left.png';
import GitLabIcon from '../images/gitlab-icon.svg';
import LogoRight from '../images/logo_right.png';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: theme.palette.primary.main,
    width: '100%',
  },
  footerText: {
    color: theme.palette.primary.light,
    textAlign: 'right',
    padding: theme.spacing(2),
  },
  copyRightLink: {
    color: theme.palette.primary.light,
    textDecoration: 'none',
  },
  divider: {
    margin: theme.spacing(2),
  },
  chip: {
    backgroundColor: theme.palette.primary.contrastText,
  },
  listIcon: {
    color: theme.palette.primary.contrastText,
  },
  nav: {
    color: theme.palette.primary.contrastText,
  },
  listItems: {
    color: theme.palette.primary.contrastText,
    textDecoration: 'none',
  },
  list: {
    direction: 'row',
    justify: 'center',
    alignItems: 'center',
  },
  listRoot: {
    flexGrow: 1,
  },
  bigAvatar: {
    width: 90,
    height: 90,
    objectFit: 'contain',
  },
  smallAvatar: {
    width: 70,
    height: 70,
    objectFit: 'contain',
  },
  avatarR: {
    textAlign: 'center',
    verticalAlign: 'middle',
  },
  avatarL: {
    textAlign: 'center',
    verticalAlign: 'bottom',
  },
}));

function Footer() {
  const classes = useStyles();
  const { t } = useTranslation('components');

  return (
    <Paper className={classes.root} elevation={4} square>
      <Typography variant="body1" className={classes.footerText}>
        &copy;
        {' '}
        <a href="https://www.facebook.com/hryohei" className={classes.copyRightLink}>Ryohei HAYASHI</a>
        {' '}
        All Rights Reserved.
      </Typography>
      <Divider className={classes.divider}>
        <Chip label={t('footer.title')} className={classes.chip} />
      </Divider>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        className={classes.listRoot}
      >
        <Hidden smDown>
          <Grid item xs={6} sm={3} className={classes.avatarR}>
            <Link component={RouterLink} to="/">
              <img
                alt="OXPT"
                src={LogoRight}
                className={classes.bigAvatar}
              />
            </Link>
          </Grid>
        </Hidden>
        <Grid item xs={6} sm={3}>
          <List component="nav" dense>
            <Link component={RouterLink} to="/about">
              <ListItem button>
                <ListItemIcon><HelpIcon className={classes.listIcon} /></ListItemIcon>
                <ListItemText
                  primary={(
                    <Typography className={classes.listItems}>
                      {t('footer.about')}
                    </Typography>
                  )}
                />
              </ListItem>
            </Link>
            <Link component={RouterLink} to="/team">
              <ListItem button>
                <ListItemIcon><GroupIcon className={classes.listIcon} /></ListItemIcon>
                <ListItemText
                  primary={(
                    <Typography className={classes.listItems}>
                      {t('footer.team')}
                    </Typography>
                  )}
                />
              </ListItem>
            </Link>
            <Link component={RouterLink} to="/award">
              <ListItem button>
                <ListItemIcon><SchoolIcon className={classes.listIcon} /></ListItemIcon>
                <ListItemText
                  primary={(
                    <Typography className={classes.listItems}>
                      {t('footer.award')}
                    </Typography>
                  )}
                />
              </ListItem>
            </Link>
          </List>
        </Grid>
        <Grid item xs={6} sm={3}>
          <List component="nav" dense>
            <Link component={RouterLink} to="/usage">
              <ListItem button>
                <ListItemIcon><DescriptionIcon className={classes.listIcon} /></ListItemIcon>
                <ListItemText
                  primary={(
                    <Typography className={classes.listItems}>
                      {t('footer.usage')}
                    </Typography>
                  )}
                />
              </ListItem>
            </Link>
            <Link component={RouterLink} to="/game_list">
              <ListItem button>
                <ListItemIcon><ListIcon className={classes.listIcon} /></ListItemIcon>
                <ListItemText
                  primary={(
                    <Typography className={classes.listItems}>
                      {t('footer.gameList')}
                    </Typography>
                  )}
                />
              </ListItem>
            </Link>
            <Link component={RouterLink} to="/terms_of_service">
              <ListItem button>
                <ListItemIcon><RuleIcon className={classes.listIcon} /></ListItemIcon>
                <ListItemText
                  primary={(
                    <Typography className={classes.listItems}>
                      {t('footer.tos')}
                    </Typography>
                  )}
                />
              </ListItem>
            </Link>
          </List>
        </Grid>
        <Grid item xs={6} sm={3}>
          <Hidden smUp><Divider light /></Hidden>
          <List component="nav" dense>
            <a href="https://gitlab.com/xeejp/oxpt_umbrella" className={classes.listItems}>
              <ListItem button>
                <ListItemIcon>
                  <Icon>
                    <img alt="GitLabIcon" src={GitLabIcon} />
                  </Icon>
                </ListItemIcon>
                <ListItemText
                  primary={(
                    <Typography>
                      {t('footer.gitlab')}
                    </Typography>
                  )}
                />
              </ListItem>
            </a>
            <a href="https://twitter.com/xeejp" className={classes.listItems}>
              <ListItem button>
                <ListItemIcon><TwitterIcon className={classes.listIcon} /></ListItemIcon>
                <ListItemText
                  primary={(
                    <Typography>
                      {t('footer.twitter')}
                    </Typography>
                  )}
                />
              </ListItem>
            </a>
            <Link component={RouterLink} to="/contact">
              <ListItem button>
                <ListItemIcon><MailIcon className={classes.listIcon} /></ListItemIcon>
                <ListItemText
                  primary={(
                    <Typography className={classes.listItems}>
                      {t('footer.contact')}
                    </Typography>
                  )}
                />
              </ListItem>
            </Link>
          </List>
        </Grid>
        <Hidden smUp>
          <Grid item xs={6} sm={3} className={classes.avatarL}>
            <Divider light />
            <Link component={RouterLink} to="/">
              <img
                alt="OXPT"
                src={LogoLeft}
                className={classes.smallAvatar}
              />
            </Link>
          </Grid>
        </Hidden>
      </Grid>
    </Paper>
  );
}

export default Footer;
