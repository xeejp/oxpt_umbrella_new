import React from 'react';
import { useTranslation } from 'react-i18next';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Chip from '@material-ui/core/Chip';
import { jaTermsOfService, enTermsOfService, esTermsOfService } from '../locales/TermsOfService';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
  tosTitle: {
    marginTop: theme.spacing(4),
    height: '100%',
    whiteSpace: 'normal',
  },
  tosDivider: {
    marginBottom: theme.spacing(1),
  },
  tosSubTitleText: {
    padding: theme.spacing(1),
    whiteSpace: 'normal',
  },
  tosSubTitle: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
    height: '100%',
  },
  tosContent: {
    textIndent: '1em',
    textAlign: 'justify',
  },
  tosTitleDiv: {
    backgroundColor: theme.palette.primary.light,
    width: theme.spacing(1),
    display: 'inline-block',
  },
}));

function TermsOfService() {
  const classes = useStyles();
  const { t, i18n } = useTranslation('components');

  let tosContent = jaTermsOfService;
  switch (i18n.language) {
    case 'ja':
      tosContent = jaTermsOfService;
      break;
    case 'en':
      tosContent = enTermsOfService;
      break;
    case 'es':
      tosContent = esTermsOfService;
      break;
    default:
      break;
  }

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={8}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
        >
          <Typography variant="h3" gutterBottom className={classes.title}>{t('tos.title')}</Typography>
          <Divider className={classes.divider} />
          <Typography variant="h5" gutterBottom className={classes.title}>{t('tos.subtitle')}</Typography>
          {tosContent.map((value) => (
            <div key={`tos-${value.title}`}>
              <Typography key={`tos-title-${value.title}`} variant="h4" gutterBottom className={classes.tosTitle} dangerouslySetInnerHTML={{ __html: value.title.toString() }} />
              <Divider key={`tos-divider-${value.title}`} className={classes.tosDivider} />
              {
                value.subSession.map((subValue) => (
                  <div key={`tos-sub-div-${value.title}-${subValue.subtitle}`}>
                    {subValue.subtitle !== '' && <Chip key={`tos-sub-chip-${value.title}-${subValue.subtitle}`} className={classes.tosSubTitle} label={<Typography variant="h6" className={classes.tosSubTitleText} dangerouslySetInnerHTML={{ __html: subValue.subtitle.toString() }} />} />}
                    {
                      subValue.content.map((contentValue) => <Typography key={`tos-content-${value.title}-${subValue.subtitle}-${contentValue}`} variant="body1" gutterBottom className={classes.tosContent} dangerouslySetInnerHTML={{ __html: contentValue.toString() }} />)
                    }
                  </div>
                ))
              }
            </div>
          ))}
        </Grid>
      </Grid>
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

export default TermsOfService;
