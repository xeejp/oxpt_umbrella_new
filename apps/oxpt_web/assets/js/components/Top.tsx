import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import clsx from 'clsx';
import 'animate.css';
import { useHistory } from 'react-router-dom';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import LoadingButton from '@material-ui/lab/LoadingButton';
import InputAdornment from '@material-ui/core/InputAdornment';

import ErrorIcon from '@material-ui/icons/Error';
import CheckIcon from '@material-ui/icons/Check';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CircularProgress from '@material-ui/core/CircularProgress';
import Logo from '../images/logo.png';

const inputContent = {
  default: { joinStatus: 'success', icon: null, helperText: 'form.top.inputDefault' },
  success: { joinStatus: 'success', icon: <CheckCircleIcon />, helperText: 'form.top.inputSuccess' },
  error: { joinStatus: 'error', icon: <ErrorIcon />, helperText: 'form.top.inputError' },
  loading: { joinStatus: 'loading', icon: <CheckIcon />, helperText: 'form.top.inputLoading' },
  checked: { joinStatus: 'checked', icon: <CheckIcon />, helperText: 'form.top.inputChecked' },
};

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
  main: {
    minHeight: 'calc(100vh - 116px)',
    [`${theme.breakpoints.up('xs')} and (orientation: landscape)`]: {
      minHeight: 'calc(100vh - 104px)',
    },
    [theme.breakpoints.up('sm')]: {
      minHeight: 'calc(100vh - 120px)',
    },
    margin: 0,
    padding: 0,
  },
  logo: {
    height: '35vh',
    objectFit: 'contain',
  },
  roomKey: {
    marginTop: theme.spacing(8),
  },
  enterButton: {
    marginTop: theme.spacing(1),
    padding: theme.spacing(2),
  },
  iconChecked: {
    color: theme.palette.text.primary,
  },
  iconSuccess: {
    color: theme.palette.success.main,
  },
  iconError: {
    color: theme.palette.error.main,
  },
}));

function Top(props) {
  const classes = useStyles();
  const { t } = useTranslation('components');
  const {
    setSnackbar, setNetworkStatus, networkStatus, inputDisabled, setInputDisabled, joinRoomAsGuest,
  } = props;
  const [joinStatus, setJoinStatus] = useState('default');
  const [roomKey, setRoomKey] = useState('');
  const history = useHistory();

  const timerRef = useRef();

  useEffect(() => {
    setJoinStatus('default');
    if (networkStatus === 'success') {
      timerRef.current = window.setTimeout(() => {
        setNetworkStatus({ networkStatus: 'default' });
        setInputDisabled({ inputDisabled: false });
        history.push('/room');
      }, 2300);
    }

    return () => {
      clearTimeout(timerRef.current);
    };
  }, [networkStatus]);

  const iconClassName = clsx({
    [classes.iconChecked]: joinStatus === 'checked',
    [classes.iconSuccess]: joinStatus === 'success',
    [classes.iconError]: joinStatus === 'error',
  });

  const handleInputChange = (event) => {
    setRoomKey(event.target.value.trim());
    if (event.target.value !== '') {
      setJoinStatus('default');
    }
  };

  const handleInputBlur = (event) => {
    setRoomKey(event.target.value.trim());
    if (joinStatus !== 'success') {
      if (event.target.value !== '') {
        setJoinStatus('checked');
      } else {
        setJoinStatus('default');
      }
    }
  };

  const hadleJoinRoomAsGuest = () => {
    if (roomKey === '') {
      setJoinStatus('error');
      setSnackbar({
        snackbarTitle: 'infoSnackbar.top.title',
        snackbarMessage: 'infoSnackbar.top.error.noRoomKey',
        snackbarSeverity: 'error',
        snackbarOpen: true,
      });
    } else if (networkStatus !== 'transferring') {
      setNetworkStatus({ networkStatus: 'transferring' });
      setInputDisabled({ inputDisabled: true });
      joinRoomAsGuest({ roomKey });
    }
  };

  const handleInputKeyDown = (event) => {
    setRoomKey(event.target.value.trim());
    const ENTER = 13;
    if (event.keyCode === ENTER && !inputDisabled) {
      hadleJoinRoomAsGuest();
    }
  };

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="center"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={3}>
        <Grid
          container
          direction="column"
          justifyContent="center"
          alignItems="stretch"
          className={classes.main}
        >
          <img id="logo" src={Logo} alt="XEE LOGO" className={classes.logo} />
          <FormControl>
            <TextField
              id="room-key"
              fullWidth
              label={t('top.roomkeyLabel')}
              className={classes.roomKey}
              error={joinStatus === 'error'}
              helperText={t(inputContent[joinStatus].helperText)}
              autoFocus
              value={roomKey}
              onChange={(event) => handleInputChange(event)}
              onBlur={(event) => handleInputBlur(event)}
              onKeyDown={(event) => handleInputKeyDown(event)}
              InputProps={{
                readOnly: inputDisabled,
                endAdornment: (
                  <InputAdornment position="end" className={iconClassName}>
                    { inputContent[joinStatus].icon}
                  </InputAdornment>
                ),
              }}
            />
            <LoadingButton
              pending={networkStatus === 'transferring'}
              disabled={inputDisabled}
              pendingPosition="end"
              variant="contained"
              size="large"
              pendingIndicator={
                <CircularProgress color="inherit" size={24} />
              }
              className={classes.enterButton}
              onClick={hadleJoinRoomAsGuest}
              endIcon={<div />}
            >
              {t('top.join')}
            </LoadingButton>
          </FormControl>
        </Grid>
      </Grid>
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

Top.propTypes = {
  setSnackbar: PropTypes.func.isRequired,
  setNetworkStatus: PropTypes.func.isRequired,
  networkStatus: PropTypes.oneOf(['default', 'success', 'error', 'transferring']).isRequired,
  inputDisabled: PropTypes.bool.isRequired,
  setInputDisabled: PropTypes.func.isRequired,
  joinRoomAsGuest: PropTypes.func.isRequired,
};

export default Top;
