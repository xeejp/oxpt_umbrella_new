import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import clsx from 'clsx';
import 'animate.css';

/* Material UI */
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Divider from '@material-ui/core/Divider';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import LoadingButton from '@material-ui/lab/LoadingButton';
import InputAdornment from '@material-ui/core/InputAdornment';

import ErrorIcon from '@material-ui/icons/Error';
import CheckIcon from '@material-ui/icons/Check';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CircularProgress from '@material-ui/core/CircularProgress';

import FAQ from '../containers/FAQ';

const inputContent = {
  default: { status: 'success', icon: null, helperText: 'top.inputDefault' },
  success: { status: 'success', icon: <CheckCircleIcon />, helperText: 'top.inputSuccess' },
  error: { status: 'error', icon: <ErrorIcon />, helperText: 'top.inputError' },
  loading: { status: 'loading', icon: <CheckIcon />, helperText: 'top.inputLoading' },
  checked: { status: 'checked', icon: <CheckIcon />, helperText: 'top.inputChecked' },
};

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: 0,
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  titleGrid: {
    padding: theme.spacing(1),
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
  buttonItem: {
    padding: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  iconChecked: {
    color: theme.palette.text.primary,
  },
  iconSuccess: {
    color: theme.palette.success.main,
  },
  iconError: {
    color: theme.palette.error.main,
  },
}));

function DeleteAccount(props) {
  const classes = useStyles();
  const { t } = useTranslation('components');
  const {
    setSnackbar,
    requestDeleteAccount,
    inputDisabled,
    setInputDisabled,
    networkStatus,
    setNetworkStatus,
  } = props;

  const [localUsername, setLocalUsername] = useState('');
  const [usernameStatus, setUsernameStatus] = useState('default');
  const history = useHistory();
  const timerRef = useRef();

  useEffect(() => {
    if (networkStatus === 'success') {
      setUsernameStatus('success');
      timerRef.current = window.setTimeout(() => {
        setNetworkStatus({ networkStatus: 'default' });
        setInputDisabled({ inputDisabled: false });
        history.push('/');
        window.location.reload(false);
      }, 2300);
    }

    return () => {
      clearTimeout(timerRef.current);
    };
  }, [networkStatus]);

  const iconUsernameClassName = clsx({
    [classes.iconChecked]: usernameStatus === 'checked',
    [classes.iconSuccess]: usernameStatus === 'success',
    [classes.iconError]: usernameStatus === 'error',
  });

  function ValidationUsername(username) {
    let errorMessage;
    if (username !== window.username || username.trim().length === 0
          || username === undefined || username === null) {
      errorMessage = 'infoSnackbar.deleteAccount.error.usernameNotMatch';
    } else {
      errorMessage = 'success';
    }
    return errorMessage;
  }

  const handleDeleteAccount = () => {
    const errorMessageUsername = ValidationUsername(localUsername);
    if (errorMessageUsername !== 'success') {
      setUsernameStatus('error');
      setSnackbar({
        snackbarTitle: 'infoSnackbar.deleteAccount.title',
        snackbarMessage: errorMessageUsername,
        snackbarSeverity: 'error',
        snackbarOpen: true,
      });
    }

    if (errorMessageUsername === 'success' && networkStatus !== 'transferring') {
      setUsernameStatus('checked');
      setInputDisabled({ inputDisabled: true });
      setNetworkStatus({ networkStatus: 'transferring' });
      requestDeleteAccount({ userToken: window.userToken });
    }
  };

  const handleUsername = (event) => {
    setLocalUsername(event.target.value.trim());
    if (event.target.value !== '') {
      if (ValidationUsername(localUsername) === 'success') {
        setUsernameStatus('default');
      } else {
        setUsernameStatus('error');
      }
    }
  };

  const handleUsernameInputBlur = (event) => {
    if (usernameStatus !== 'success') {
      if (event.target.value !== '') {
        if (ValidationUsername(localUsername) === 'success') {
          setUsernameStatus('checked');
        } else {
          setUsernameStatus('error');
        }
      } else {
        setUsernameStatus('default');
      }
    }
  };

  const handleInputKeyDown = (event) => {
    const ENTER = 13;
    if (event.keyCode === ENTER && !inputDisabled) {
      handleDeleteAccount();
    }
  };

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={4} className={classes.titleGrid}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
        >
          <Typography variant="h3" gutterBottom className={classes.title}>{t('deleteAccount.title')}</Typography>
          <Divider className={classes.divider} />
          <Typography variant="h5" gutterBottom className={classes.title}>{t('deleteAccount.subtitle')}</Typography>
        </Grid>
        <Grid item xs={12} sm={12}>
          <Typography variant="body1" gutterBottom>
            {t('deleteAccount.content')}
          </Typography>
          <FormControl fullWidth>
            <TextField
              id="username"
              label={t('deleteAccount.username')}
              autoFocus
              error={networkStatus === 'error' || usernameStatus === 'error'}
              InputProps={{
                readOnly: false,
                endAdornment: (
                  <InputAdornment position="end" className={iconUsernameClassName}>
                    { inputContent[usernameStatus].icon}
                  </InputAdornment>
                ),
              }}
              onChange={(event) => handleUsername(event)}
              onBlur={(event) => handleUsernameInputBlur(event)}
              onKeyDown={(event) => handleInputKeyDown(event)}
              value={localUsername}
              placeholder={window.username}
            />
            <LoadingButton
              pending={networkStatus === 'transferring'}
              className={classes.buttonItem}
              pendingPosition="end"
              variant="contained"
              size="large"
              onClick={handleDeleteAccount}
              disabled={inputDisabled}
              pendingIndicator={
                <CircularProgress color="inherit" size={24} />
              }
              endIcon={<div />}
            >
              {t('deleteAccount.delete')}
            </LoadingButton>
          </FormControl>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={4}>
        <FAQ filter="delete_account" />
      </Grid>
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

DeleteAccount.propTypes = {
  setSnackbar: PropTypes.func.isRequired,
  requestDeleteAccount: PropTypes.func.isRequired,
  inputDisabled: PropTypes.bool.isRequired,
  setInputDisabled: PropTypes.func.isRequired,
  networkStatus: PropTypes.oneOf(['default', 'success', 'error', 'transferring']).isRequired,
  setNetworkStatus: PropTypes.func.isRequired,
};

export default DeleteAccount;
