import React from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

/* lazy loading */
import loadable from '@loadable/component';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

/* containers */
import Footer from '../containers/Footer';
import Header from '../containers/Header';
import Loading from '../containers/Loading';
import Menu from '../containers/Menu';
import SnackbarInfo from '../containers/SnackbarInfo';

const About = loadable(() => import('../containers/About'), {
  fallback: <Loading />,
});
const Account = loadable(() => import('../containers/Account'), {
  fallback: <Loading />,
});
const Award = loadable(() => import('../containers/Award'), {
  fallback: <Loading />,
});
const Contact = loadable(() => import('../containers/Contact'), {
  fallback: <Loading />,
});
const CreateRoom = loadable(() => import('../containers/CreateRoom'), {
  fallback: <Loading />,
});
const DeleteAccount = loadable(() => import('../containers/DeleteAccount'), {
  fallback: <Loading />,
});
const FAQ = loadable(() => import('../containers/FAQ'), {
  fallback: <Loading />,
});
const GameList = loadable(() => import('../containers/GameList'), {
  fallback: <Loading />,
});
const Room = loadable(() => import('../containers/Room'), {
  fallback: <Loading />,
});
const RoomList = loadable(() => import('../containers/RoomList'), {
  fallback: <Loading />,
});
const RoomManagement = loadable(() => import('../containers/RoomManagement'), {
  fallback: <Loading />,
});
const Team = loadable(() => import('../containers/Team'), {
  fallback: <Loading />,
});
const TermsOfService = loadable(() => import('../containers/TermsOfService'), {
  fallback: <Loading />,
});
const Top = loadable(() => import('../containers/Top'), {
  fallback: <Loading />,
});
const Usage = loadable(() => import('../containers/Usage'), {
  fallback: <Loading />,
});

const useStyles = makeStyles((theme) => ({
  main: {
    minHeight: 'calc(100vh - 116px)',
    [`${theme.breakpoints.up('xs')} and (orientation: landscape)`]: {
      minHeight: 'calc(100vh - 104px)',
    },
    [theme.breakpoints.up('sm')]: {
      minHeight: 'calc(100vh - 120px)',
    },
    margin: 0,
    padding: 0,
  },
  container: {
    margin: 0,
    padding: 0,
  },
}));

const Render = (Component) => () => {
  window.scrollTo(0, 0);
  return <Component />;
};

function App(props) {
  const classes = useStyles();
  const { menuOpen } = props;

  return (
    <>
      <Header />
      <Menu />
      <Grid
        container
        direction="column"
        justify="space-between"
        alignItems="stretch"
        className={classes.container}
      >
        <Grid item className={classes.main}>
          <Switch>
            <Route path="/" render={Render(Top)} exact />
            <Route path="/about" render={Render(About)} exact />
            <Route path="/account" render={Render(Account)} exact />
            <Route path="/award" render={Render(Award)} exact />
            <Route path="/contact" render={Render(Contact)} exact />
            <Route path="/create_room" render={Render(CreateRoom)} exact />
            <Route path="/delete_account" render={Render(DeleteAccount)} exact />
            <Route path="/faq" render={Render(FAQ)} exact />
            <Route path="/game_list" render={Render(GameList)} exact />
            <Route path="/room" render={Render(Room)} exact />
            <Route path="/room_list" render={Render(RoomList)} exact />
            <Route path="/room_management" render={Render(RoomManagement)} exact />
            <Route path="/team" render={Render(Team)} exact />
            <Route path="/terms_of_service" render={Render(TermsOfService)} exact />
            <Route path="/usage" render={Render(Usage)} exact />
            <Route path="/loading" Component={Loading} exact />
          </Switch>
        </Grid>
      </Grid>
      {!menuOpen && <Footer />}
      <SnackbarInfo />
    </>
  );
}

App.propTypes = {
  menuOpen: PropTypes.bool.isRequired,
};

export default App;
