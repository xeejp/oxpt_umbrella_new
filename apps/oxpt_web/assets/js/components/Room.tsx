import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';

import i18next from 'i18next';

const useStyles = makeStyles(() => ({
  iframe: {
    margin: 0,
    padding: 0,
    width: '100%',
    verticalAlign: 'top',
  },
}));

function Room(props) {
  const classes = useStyles();
  const history = useHistory();
  const { t } = useTranslation('common');
  const [didRead, setDidRead] = useState(false);
  const { gameId, guestId } = props;
  const iframeRef = useRef();
  const timerRef = useRef();

  useEffect(() => {
    if (gameId === '' || guestId === '') {
      history.push('/room_list');
    }
  }, []);

  useEffect(() => {
    if (!didRead) {
      window.scrollTo(0, 0);
      setDidRead(true);
    }
  }, []);

  useEffect(() => {
    i18next.on('languageChanged', (lang) => {
      if (iframeRef.current) {
        iframeRef.current.contentWindow.postMessage({ lang }, '*');
      }
    });
  }, []);

  function reSize() {
    if (iframeRef.current.style.height !== `${iframeRef.current.contentWindow.document.body.scrollHeight}px`) {
      timerRef.current = window.setTimeout(() => {
        iframeRef.current.style.height = `${iframeRef.current.contentWindow.document.body.scrollHeight}px`;
        reSize();
      }, 100);
    } else {
      clearTimeout(timerRef.current);
    }
  }

  useEffect(() => {
    window.addEventListener('resize', reSize, false);
    return () => {
      clearTimeout(timerRef.current);
      window.removeEventListener('resize', reSize, false);
    };
  }, []);

  return (
    (gameId !== '' && guestId !== '')
      ? (
        <iframe
          ref={iframeRef}
          id="inlineFrame"
          title="Room Frame"
          scrolling="no"
          allow="fullscreen"
          frameBorder={0}
          src={`/game/${gameId}/${guestId}`}
          className={classes.iframe}
          marginHeight={0}
          marginWidth={0}
          onLoad={() => { reSize(); }}
          onChange={() => { reSize(); }}
        />
      )
      : <div>{t('common:loading')}</div>
  );
}

Room.propTypes = {
  gameId: PropTypes.string.isRequired,
  guestId: PropTypes.string.isRequired,
};

export default Room;
