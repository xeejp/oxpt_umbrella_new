import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import clsx from 'clsx';
import 'animate.css';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import Skeleton from '@material-ui/core/Skeleton';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import Paper from '@material-ui/core/Paper';
import DeleteIcon from '@material-ui/icons/Delete';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: 0,
  },
  titleGrid: {
    padding: theme.spacing(1),
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  roomGrid: {
    padding: theme.spacing(1),
  },
  roomButton: {
    padding: theme.spacing(2),
    color: theme.palette.text.primary,
    backgroundColor: theme.palette.primary.contrastText,
    '&:hover': {
      color: theme.palette.secondary.contrastText,
      backgroundColor: theme.palette.primary.main,
    },
  },
  roomPaper: {
    padding: 0,
  },
  roomPaperSkeleton: {
    maxHeight: '159.33px',
  },
  roomKey: {
    width: '100%',
    textAlign: 'left',
    paddingLeft: theme.spacing(2),
    marginTop: theme.spacing(1),
  },
  roomKeySkeleton: {
    marginTop: theme.spacing(1),
    marginBottom: '11.9px',
  },
  roomItem: {
    padding: theme.spacing(1),
    width: '100%',
    textAlign: 'right',
  },
  roomItemSkeleton: {
    padding: theme.spacing(1),
    width: '100%',
  },
  roomButtonItemSkeleton: {
    margin: theme.spacing(1),
    marginTop: 0,
    display: 'flex',
    justifyContent: 'flex-end',
  },
  roomButtonSkeleton: {
    margin: theme.spacing(1),
    marginLeft: 0,
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
}));

function RoomManagement(props) {
  const classes = useStyles();
  const { t } = useTranslation('components');
  const [loading, setLoading] = useState(true);
  const [joinRoom, setJoinRoom] = useState(false);
  const history = useHistory();
  const timerRef = useRef();
  const {
    requestHostRoomList,
    setNetworkStatus,
    hostRoomList,
    networkStatus,
    setInputDisabled,
    joinRoomAsHost,
    inputDisabled,
  } = props;
  const roomClassName = clsx(classes.roomGrid, 'animate__animated animate__fadeIn');

  const isSignIn = (window.accountToken !== '');
  const isHostRoom = Object.values(hostRoomList).length;

  useEffect(() => {
    if (networkStatus === 'success') {
      setLoading(false);
      if (joinRoom) {
        timerRef.current = window.setTimeout(() => {
          setNetworkStatus({ networkStatus: 'default' });
          setInputDisabled({ inputDisabled: false });
          history.push('/room');
        }, 2300);
      } else {
        setNetworkStatus({ networkStatus: 'default' });
        setInputDisabled({ inputDisabled: false });
      }
    }

    return () => {
      clearTimeout(timerRef.current);
    };
  }, [networkStatus]);

  const handleRequestHostRoomList = () => {
    setLoading(true);
    setJoinRoom(false);
    setNetworkStatus({ networkStatus: 'transferring' });
    setInputDisabled({ inputDisabled: true });
    requestHostRoomList();
  };

  useEffect(() => {
    handleRequestHostRoomList();
  }, []);

  const hadleJoinRoomAsHost = (roomId) => {
    setJoinRoom(true);
    setNetworkStatus({ networkStatus: 'transferring' });
    setInputDisabled({ inputDisabled: true });
    joinRoomAsHost({ roomId });
  };

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={8}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
          className={classes.titleGrid}
        >
          <Typography variant="h3" gutterBottom component="div" className={classes.title}>{t('roomManagement.temporaryRoomList')}</Typography>
          <Divider className={classes.divider} />
          {(isHostRoom === 0) && <Typography variant="h5" gutterBottom component="div" className={classes.title}>{t('roomManagement.noHostRoom')}</Typography>}
        </Grid>
      </Grid>
      <Grid item xs={12} sm={8}>
        <Grid
          container
          direction="row"
          justifyContent="left"
          alignItems="flex-start"
        >
          {loading && hostRoomList
            ? [1, 2, 3, 4].map((value) => (
              <Grid item key={value} xs={12} sm={3} className={classes.roomGrid}>
                <Paper className={classes.roomPaperSkeleton}>
                  <Grid
                    container
                    direction="column"
                    justifyContent="center"
                    alignItems="stretch"
                  >
                    <Grid item className={classes.roomKey}><Skeleton variant="rectangular" width="90%" height={41.33} className={classes.roomKeySkeleton} sx={{ borderRadius: 1 }} animation="wave" /></Grid>
                    <Grid item><Divider /></Grid>
                    <Grid item className={classes.roomButtonItemSkeleton}>
                      <Skeleton variant="circular" width={36} height={36} animation="wave" className={classes.roomButtonSkeleton} />
                      <Skeleton variant="circular" width={36} height={36} animation="wave" className={classes.roomButtonSkeleton} />
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            ))
            : Object.values(hostRoomList).map((value) => !value.persists && (
            <Grid item key={value.room_key} xs={12} sm={3} className={roomClassName}>
              <Paper className={classes.roomPaper}>
                <Grid
                  container
                  direction="column"
                  justifyContent="center"
                  alignItems="stretch"
                >
                  <Grid item className={classes.roomKey}><Typography variant="h4" gutterBottom component="div">{value.room_key}</Typography></Grid>
                  <Grid item><Divider /></Grid>
                  <Grid item className={classes.roomItem} disabled>
                    <Tooltip title={t('roomManagement.delete')} arrow>
                      <IconButton aria-label="delete" disabled={inputDisabled}>
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title={t('roomManagement.manage')} arrow>
                      <IconButton aria-label="play" className={classes.playButton} onClick={() => { hadleJoinRoomAsHost(value.room_id); }} disabled={inputDisabled}>
                        <PlayArrowIcon />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
            ))}
        </Grid>
      </Grid>
      {isSignIn && (
      <Grid item xs={12} sm={8}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
          className={classes.titleGrid}
        >
          <Typography variant="h3" gutterBottom component="div" className={classes.title}>{t('roomManagement.persistentRoomList')}</Typography>
          <Divider className={classes.divider} />
          {(isHostRoom === 0) && <Typography variant="h5" gutterBottom component="div" className={classes.title}>{t('roomManagement.noHostRoom')}</Typography>}
        </Grid>
      </Grid>
      )}
      {isSignIn && (
      <Grid item xs={12} sm={8}>
        <Grid
          container
          direction="row"
          justifyContent="left"
          alignItems="flex-start"
        >
          {loading
            ? [1, 2, 3, 4].map((value) => (
              <Grid item key={value} xs={12} sm={3} className={classes.roomGrid}>
                <Paper className={classes.roomPaperSkeleton}>
                  <Grid
                    container
                    direction="column"
                    justifyContent="center"
                    alignItems="stretch"
                  >
                    <Grid item className={classes.roomKey}><Skeleton variant="rectangular" width="90%" height={41.33} className={classes.roomKeySkeleton} sx={{ borderRadius: 1 }} animation="wave" /></Grid>
                    <Grid item><Divider /></Grid>
                    <Grid item className={classes.roomButtonItemSkeleton}>
                      <Skeleton variant="circular" width={36} height={36} animation="wave" className={classes.roomButtonSkeleton} />
                      <Skeleton variant="circular" width={36} height={36} animation="wave" className={classes.roomButtonSkeleton} />
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            ))
            : Object.values(hostRoomList).map((value) => value.persists && (
            <Grid item key={value.room_key} xs={12} sm={3} className={roomClassName}>
              <Paper className={classes.roomPaper}>
                <Grid
                  container
                  direction="column"
                  justifyContent="center"
                  alignItems="stretch"
                >
                  <Grid item className={classes.roomKey}><Typography variant="h4" gutterBottom component="div">{value.room_key}</Typography></Grid>
                  <Grid item><Divider /></Grid>
                  <Grid item className={classes.roomItem} disabled>
                    <Tooltip title={t('roomManagement.delete')} arrow>
                      <IconButton aria-label="delete">
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title={t('roomManagement.manage')} arrow>
                      <IconButton aria-label="play" className={classes.playButton} onClick={() => { hadleJoinRoomAsHost(value.room_id); }}>
                        <PlayArrowIcon />
                      </IconButton>
                    </Tooltip>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
            ))}
        </Grid>
      </Grid>
      )}
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

RoomManagement.propTypes = {
  requestHostRoomList: PropTypes.func.isRequired,
  setNetworkStatus: PropTypes.func.isRequired,
  hostRoomList: PropTypes.arrayOf(
    PropTypes.exact({
      persists: PropTypes.bool.isRequired,
      room_id: PropTypes.string.isRequired,
      room_key: PropTypes.string.isRequired,
    }),
  ).isRequired,
  networkStatus: PropTypes.oneOf(['default', 'success', 'error', 'transferring']).isRequired,
  setInputDisabled: PropTypes.func.isRequired,
  joinRoomAsHost: PropTypes.func.isRequired,
  inputDisabled: PropTypes.bool.isRequired,
};

export default RoomManagement;
