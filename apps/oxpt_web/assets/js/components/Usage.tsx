import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

/* Material UI */
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import Chip from '@material-ui/core/Chip';

import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import Paper from '@material-ui/core/Paper';
import { jaUsage, enUsage /* , esUsage */ } from '../locales/Usage';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
  usageTitle: {
    height: '100%',
    whiteSpace: 'normal',
  },
  usageDivider: {
    marginBottom: theme.spacing(1),
  },
  usageSubTitleText: {
    padding: theme.spacing(1),
    whiteSpace: 'normal',
  },
  usageSubTitle: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
    height: '100%',
  },
  usageContent: {
    textIndent: '1em',
    textAlign: 'justify',
  },
  usageTitleDiv: {
    backgroundColor: theme.palette.primary.light,
    width: theme.spacing(1),
    display: 'inline-block',
  },
  imgHeader: {
    padding: theme.spacing(1),
    backgroundColor: theme.palette.grey[200],
  },
  img: {
    height: '40vh',
    maxWidth: '100%',
    display: 'block',
    overflow: 'hidden',
  },
  imgItems: {
    maxWidth: '100%',
  },
}));

function ImageStepper(props) {
  const theme = useTheme();
  const { image } = props;
  const classes = useStyles();
  const maxSteps = Object.keys(image).length;

  const [activeStep, setActiveStep] = useState(0);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
    >
      <Grid item>
        <img
          className={classes.img}
          src={image[activeStep].src}
          alt={image[activeStep].label}
        />
        <Paper elevation={0} className={classes.imgHeader}>
          <Typography>{image[activeStep].label}</Typography>
        </Paper>
        <MobileStepper
          variant="dots"
          steps={maxSteps}
          position="static"
          activeStep={activeStep}
          className={classes.imageStepper}
          nextButton={(
            <Button size="small" onClick={handleNext} disabled={activeStep === maxSteps - 1}>
              Next
              {theme.direction === 'rtl' ? (<KeyboardArrowLeft />) : (<KeyboardArrowRight />)}
            </Button>
          )}
          backButton={(
            <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
              {theme.direction === 'rtl' ? (<KeyboardArrowRight />) : (<KeyboardArrowLeft />)}
              Back
            </Button>
          )}
        />
      </Grid>
    </Grid>
  );
}

function Usage() {
  const classes = useStyles();
  const { t, i18n } = useTranslation('components');

  let usageContent = jaUsage;
  switch (i18n.language) {
    case 'ja':
      usageContent = jaUsage;
      break;
    case 'en':
      usageContent = enUsage;
      break;
    /* case 'es':
      usageContent = esUsage;
      break; */
    default:
      break;
  }

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={8}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
        >
          <Typography variant="h3" gutterBottom className={classes.title}>{t('usage.title')}</Typography>
          <Divider className={classes.divider} />
          <Typography variant="h5" gutterBottom className={classes.title}>{t('usage.subtitle')}</Typography>
          {usageContent.map((value) => {
            const subSessionNum = Object.keys(value.subSession).length;
            return (
              <div key={`usage-${value.title}`}>
                <Typography key={`usage-title-${value.title}`} variant="h4" gutterBottom className={classes.usageTitle} dangerouslySetInnerHTML={{ __html: value.title.toString() }} />
                <Divider key={`usage-start-divider-${value.title}`} className={classes.usageDivider} />
                {value.subSession.map((subValue, subIndex) => (
                  <div key={`usage-sub-div-${value.title}-${subValue.subtitle}`}>
                    {(subValue.subtitle.length > 0) && <Chip key={`usage-sub-chip-${value.title}-${subValue.subtitle}`} label={<Typography variant="h6" className={classes.usageSubTitleText} dangerouslySetInnerHTML={{ __html: subValue.subtitle.toString() }} />} className={classes.usageSubTitle} />}
                    {subValue.content.map((contentValue) => <Typography key={`usage-content-${value.title}-${subValue.subtitle}-${contentValue}`} variant="body1" gutterBottom className={classes.usageContent} dangerouslySetInnerHTML={{ __html: contentValue.toString() }} />)}
                    {(subValue.image.length > 0) && <ImageStepper image={subValue.image} />}
                    {(subSessionNum - 1 > subIndex) && <Divider key={`usage-end-divider-${value.title}`} className={classes.usageDivider} />}
                  </div>
                ))}
              </div>
            );
          })}
        </Grid>
      </Grid>
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

ImageStepper.propTypes = {
  image: PropTypes.arrayOf(
    PropTypes.exact({
      label: PropTypes.string,
      src: PropTypes.string,
    }),
  ).isRequired,
};

export default Usage;
