import React from 'react';
import { useTranslation } from 'react-i18next';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Chip from '@material-ui/core/Chip';
import { jaAbout, enAbout, esAbout } from '../locales/About';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
  aboutTitle: {
    marginTop: theme.spacing(4),
    height: '100%',
    whiteSpace: 'normal',
  },
  aboutDivider: {
    marginBottom: theme.spacing(1),
  },
  aboutSubTitleText: {
    padding: theme.spacing(1),
    whiteSpace: 'normal',
    height: '100%',
  },
  aboutSubTitle: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
  },
  aboutContent: {
    textIndent: '1em',
    textAlign: 'justify',
  },
  aboutTitleDiv: {
    backgroundColor: theme.palette.primary.light,
    width: theme.spacing(1),
    display: 'inline-block',
  },
}));

function About() {
  const classes = useStyles();
  const { t, i18n } = useTranslation('components');

  let aboutContent = jaAbout;
  switch (i18n.language) {
    case 'ja':
      aboutContent = jaAbout;
      break;
    case 'en':
      aboutContent = enAbout;
      break;
    case 'es':
      aboutContent = esAbout;
      break;
    default:
      break;
  }

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={8}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
        >
          <Typography variant="h3" gutterBottom className={classes.title}>{t('about.title')}</Typography>
          <Divider className={classes.divider} />
          <Typography variant="h5" gutterBottom className={classes.title}>{t('about.subtitle')}</Typography>
          {aboutContent.map((value) => (
            <div key={`about-${value.title}`}>
              <Typography key={`about-title-${value.title}`} variant="h4" gutterBottom className={classes.aboutTitle} dangerouslySetInnerHTML={{ __html: value.title.toString() }} />
              <Divider key={`about-divider-${value.title}`} className={classes.aboutDivider} />
              {
                value.subSession.map((subValue) => (
                  <div key={`about-sub-div-${value.title}-${subValue.subtitle}`}>
                    {subValue.subtitle !== '' && <Chip key={`about-sub-chip-${value.title}-${subValue.subtitle}`} className={classes.aboutSubTitle} label={<Typography variant="h6" className={classes.aboutSubTitleText} dangerouslySetInnerHTML={{ __html: subValue.subtitle.toString() }} />} />}
                    {
                      subValue.content.map((contentValue) => <Typography key={`about-content-${value.title}-${subValue.subtitle}-${contentValue}`} variant="body1" gutterBottom className={classes.aboutContent} dangerouslySetInnerHTML={{ __html: contentValue.toString() }} />)
                    }
                  </div>
                ))
              }
            </div>
          ))}
        </Grid>
      </Grid>
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

export default About;
