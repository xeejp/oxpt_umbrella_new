import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import clsx from 'clsx';
import 'animate.css';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import LoadingButton from '@material-ui/lab/LoadingButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';

import TwitterIcon from '@material-ui/icons/Twitter';

import ErrorIcon from '@material-ui/icons/Error';
import CheckIcon from '@material-ui/icons/Check';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CircularProgress from '@material-ui/core/CircularProgress';
import FAQ from '../containers/FAQ';

const inputContent = {
  default: { status: 'success', icon: null },
  success: { status: 'success', icon: <CheckCircleIcon /> },
  error: { status: 'error', icon: <ErrorIcon /> },
  loading: { status: 'loading', icon: <CheckIcon /> },
  checked: { status: 'checked', icon: <CheckIcon /> },
};

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: 0,
  },
  titleGrid: {
    padding: theme.spacing(1),
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
  dividerContact: {
    margin: theme.spacing(2),
  },
  formItem: {
    margin: theme.spacing(1),
  },
  buttonItem: {
    padding: theme.spacing(2),
    margin: theme.spacing(1),
  },
  buttonTwitter: {
    textTransform: 'capitalize',
    padding: theme.spacing(2),
    borderColor: '#1DA1F2',
    color: '#1DA1F2',
    backgroundColor: theme.palette.primary.contrastText,
    '&:hover': {
      color: theme.palette.primary.contrastText,
      backgroundColor: '#1DA1F2',
      borderColor: '#1DA1F2',
    },
  },
  iconChecked: {
    color: theme.palette.text.primary,
  },
  iconSuccess: {
    color: theme.palette.success.main,
  },
  iconError: {
    color: theme.palette.error.main,
  },
}));

function Contact(props) {
  const classes = useStyles();
  const { t } = useTranslation('components');
  const [localName, setLocalName] = useState('');
  const [localEmail, setLocalEmail] = useState('');
  const [localSubject, setLocalSubject] = useState('');
  const [localBody, setLocalBody] = useState('');
  const {
    setSnackbar, requestContact, inputDisabled, setInputDisabled, networkStatus, setNetworkStatus,
  } = props;

  const [nameStatus, setNameStatus] = useState('default');
  const [emailStatus, setEmailStatus] = useState('default');
  const [subjectStatus, setSubjectStatus] = useState('default');
  const [bodyStatus, setBodyStatus] = useState('default');

  const history = useHistory();
  const timerRef = useRef();

  useEffect(() => {
    if (networkStatus === 'success') {
      setNameStatus('success');
      setEmailStatus('success');
      setSubjectStatus('success');
      setBodyStatus('success');
      timerRef.current = window.setTimeout(() => {
        setNetworkStatus({ networkStatus: 'default' });
        setInputDisabled({ inputDisabled: false });
        history.push('/');
      }, 2300);
    }

    return () => {
      clearTimeout(timerRef.current);
    };
  }, [networkStatus]);

  const iconNameClassName = clsx({
    [classes.iconChecked]: nameStatus === 'checked',
    [classes.iconSuccess]: nameStatus === 'success',
    [classes.iconError]: nameStatus === 'error',
  });

  const iconEmailClassName = clsx({
    [classes.iconChecked]: emailStatus === 'checked',
    [classes.iconSuccess]: emailStatus === 'success',
    [classes.iconError]: emailStatus === 'error',
  });

  const iconSubjectClassName = clsx({
    [classes.iconChecked]: subjectStatus === 'checked',
    [classes.iconSuccess]: subjectStatus === 'success',
    [classes.iconError]: subjectStatus === 'error',
  });

  const iconBodyClassName = clsx({
    [classes.iconChecked]: bodyStatus === 'checked',
    [classes.iconSuccess]: bodyStatus === 'success',
    [classes.iconError]: bodyStatus === 'error',
  });

  function hankaku2Zenkaku(str) {
    return str.replace(/[Ａ-Ｚａ-ｚ０-９]/g, (s) => String.fromCharCode(s.charCodeAt(0) - 0xFEE0));
  }

  function ValidationName(name) {
    let errorMessage;
    if (name.trim().length === 0) {
      errorMessage = 'infoSnackbar.contact.error.NoName';
    } else {
      errorMessage = 'success';
    }
    return errorMessage;
  }

  function ValidationEmail(email) {
    const replacedEmail = hankaku2Zenkaku(email.trim());
    const reg = /^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/;
    let errorMessage;
    if (replacedEmail.length === 0) {
      errorMessage = 'infoSnackbar.contact.error.NoEmail';
    } else if (!reg.test(email)) {
      errorMessage = 'infoSnackbar.contact.error.NotValidEmail';
    } else {
      errorMessage = 'success';
    }
    return errorMessage;
  }

  function ValidationSubject(subject) {
    let errorMessage;
    if (subject.trim().length === 0) {
      errorMessage = 'infoSnackbar.contact.error.NoSubject';
    } else {
      errorMessage = 'success';
    }
    return errorMessage;
  }

  function ValidationBody(body) {
    let errorMessage;
    if (body.trim().length === 0) {
      errorMessage = 'infoSnackbar.contact.error.NoBody';
    } else {
      errorMessage = 'success';
    }
    return errorMessage;
  }

  const handleClickTwitter = () => {
    window.location.href = 'https://twitter.com/messages/compose?recipient_id=817723236335042562';
  };

  const handleNameChange = (event) => {
    const name = event.target.value.trim();
    setLocalName(name);
    if (name !== '') {
      if (ValidationName(name) === 'success') {
        setNameStatus('default');
      } else {
        setNameStatus('error');
      }
    }
  };

  const handleEmailChange = (event) => {
    const replacedEmail = hankaku2Zenkaku(event.target.value.trim());
    setLocalEmail(replacedEmail);
    if (replacedEmail !== '') {
      if (ValidationEmail(replacedEmail) === 'success') {
        setEmailStatus('default');
      } else {
        setEmailStatus('error');
      }
    }
  };

  const handleSubjectChange = (event) => {
    const subject = event.target.value.trim();
    setLocalSubject(subject);
    if (subject !== '') {
      if (ValidationSubject(subject) === 'success') {
        setSubjectStatus('default');
      } else {
        setSubjectStatus('error');
      }
    }
  };

  const handleBodyChange = (event) => {
    const body = event.target.value.trim();
    setLocalBody(body);
    if (body !== '') {
      if (ValidationBody(body) === 'success') {
        setBodyStatus('default');
      } else {
        setBodyStatus('error');
      }
    }
  };

  const handleNameBlur = (event) => {
    if (nameStatus !== 'success') {
      if (event.target.value !== '') {
        if (ValidationName(localName) === 'success') {
          setNameStatus('checked');
        } else {
          setNameStatus('error');
        }
      } else {
        setNameStatus('default');
      }
    }
  };

  const handleEmailBlur = (event) => {
    if (emailStatus !== 'success') {
      if (event.target.value !== '') {
        if (ValidationEmail(localEmail) === 'success') {
          setEmailStatus('checked');
        } else {
          setEmailStatus('error');
        }
      } else {
        setEmailStatus('default');
      }
    }
  };

  const handleSubjectBlur = (event) => {
    if (subjectStatus !== 'success') {
      if (event.target.value !== '') {
        if (ValidationSubject(localSubject) === 'success') {
          setSubjectStatus('checked');
        } else {
          setSubjectStatus('error');
        }
      } else {
        setSubjectStatus('default');
      }
    }
  };

  const handleBodyBlur = (event) => {
    if (bodyStatus !== 'success') {
      if (event.target.value !== '') {
        if (ValidationBody(localBody) === 'success') {
          setBodyStatus('checked');
        } else {
          setBodyStatus('error');
        }
      } else {
        setBodyStatus('default');
      }
    }
  };

  const handleRequestContact = () => {
    const errorMessageBody = ValidationBody(localBody);
    if (errorMessageBody !== 'success') {
      setBodyStatus('error');
      setSnackbar({
        snackbarTitle: 'infoSnackbar.contact.title',
        snackbarMessage: errorMessageBody,
        snackbarSeverity: 'error',
        snackbarOpen: true,
      });
    }

    const errorMessageSubject = ValidationSubject(localSubject);
    if (errorMessageSubject !== 'success') {
      setSubjectStatus('error');
      setSnackbar({
        snackbarTitle: 'infoSnackbar.contact.title',
        snackbarMessage: errorMessageSubject,
        snackbarSeverity: 'error',
        snackbarOpen: true,
      });
    }

    const errorMessageEmail = ValidationEmail(localEmail);
    if (errorMessageEmail !== 'success') {
      setEmailStatus('error');
      setSnackbar({
        snackbarTitle: 'infoSnackbar.contact.title',
        snackbarMessage: errorMessageEmail,
        snackbarSeverity: 'error',
        snackbarOpen: true,
      });
    }

    const errorMessageName = ValidationName(localName);
    if (errorMessageName !== 'success') {
      setNameStatus('error');
      setSnackbar({
        snackbarTitle: 'infoSnackbar.contact.title',
        snackbarMessage: errorMessageName,
        snackbarSeverity: 'error',
        snackbarOpen: true,
      });
    }

    if (errorMessageName === 'success' && errorMessageEmail === 'success' && errorMessageSubject === 'success' && errorMessageBody === 'success' && networkStatus !== 'transferring') {
      const detail = {
        name: localName,
        email: localEmail,
        subject: localSubject,
        body: localBody,
      };

      setNameStatus('checked');
      setEmailStatus('checked');
      setSubjectStatus('checked');
      setBodyStatus('checked');
      setInputDisabled({ inputDisabled: true });
      setNetworkStatus({ networkStatus: 'transferring' });
      requestContact(detail);
    }
  };

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={4}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
          className={classes.titleGrid}
        >
          <Typography variant="h3" gutterBottom component="div" className={classes.title}>{t('contact.title')}</Typography>
          <Divider className={classes.divider} />
          <Typography variant="h5" gutterBottom component="div" className={classes.title}>{t('contact.contents')}</Typography>
        </Grid>
        <Grid item xs={12} sm={12} className={classes.titleGrid}>
          <Button
            variant="outlined"
            color="primary"
            size="large"
            className={classes.buttonTwitter}
            fullWidth
            startIcon={<TwitterIcon />}
            onClick={handleClickTwitter}
            disabled={inputDisabled}
          >
            Twitter DM
          </Button>
        </Grid>
        <Grid item xs={12} sm={12} className={classes.dividerContact}><Divider><Chip label="OR" /></Divider></Grid>
        <Grid
          container
          direction="row"
          justifyContent="space-around"
          alignItems="stretch"
        >
          <Grid item xs={12} sm={12}>
            <FormControl fullWidth>
              <TextField
                id="contact-name"
                label={t('contact.name')}
                autoFocus
                className={classes.formItem}
                error={networkStatus === 'error' || nameStatus === 'error'}
                onChange={(event) => handleNameChange(event)}
                onBlur={(event) => handleNameBlur(event)}
                value={localName}
                InputProps={{
                  readOnly: false,
                  endAdornment: (
                    <InputAdornment position="end" className={iconNameClassName}>
                      { inputContent[nameStatus].icon}
                    </InputAdornment>
                  ),
                }}
                disabled={inputDisabled}
              />
              <TextField
                id="contact-mail-address"
                label={t('contact.email')}
                className={classes.formItem}
                error={networkStatus === 'error' || emailStatus === 'error'}
                onChange={(event) => handleEmailChange(event)}
                onBlur={(event) => handleEmailBlur(event)}
                value={localEmail}
                InputProps={{
                  readOnly: false,
                  endAdornment: (
                    <InputAdornment position="end" className={iconEmailClassName}>
                      { inputContent[emailStatus].icon}
                    </InputAdornment>
                  ),
                }}
                disabled={inputDisabled}
              />
              <TextField
                id="contact-subject"
                label={t('contact.subject')}
                className={classes.formItem}
                error={networkStatus === 'error' || subjectStatus === 'error'}
                onChange={(event) => handleSubjectChange(event)}
                onBlur={(event) => handleSubjectBlur(event)}
                value={localSubject}
                InputProps={{
                  readOnly: false,
                  endAdornment: (
                    <InputAdornment position="end" className={iconSubjectClassName}>
                      { inputContent[subjectStatus].icon}
                    </InputAdornment>
                  ),
                }}
                disabled={inputDisabled}
              />
              <TextField
                id="contact-message"
                label={t('contact.body')}
                className={classes.formItem}
                error={networkStatus === 'error' || bodyStatus === 'error'}
                onChange={(event) => handleBodyChange(event)}
                onBlur={(event) => handleBodyBlur(event)}
                value={localBody}
                multiline
                rows={8}
                InputProps={{
                  readOnly: false,
                  endAdornment: (
                    <InputAdornment position="end" className={iconBodyClassName}>
                      { inputContent[bodyStatus].icon}
                    </InputAdornment>
                  ),
                }}
                disabled={inputDisabled}
              />
              <LoadingButton
                pending={networkStatus === 'transferring'}
                className={classes.buttonItem}
                pendingPosition="end"
                variant="contained"
                size="large"
                pendingIndicator={
                  <CircularProgress color="inherit" size={24} />
                }
                endIcon={<div />}
                onClick={handleRequestContact}
                disabled={inputDisabled}
              >
                {t('contact.send')}
              </LoadingButton>
            </FormControl>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={4}>
        <FAQ filter="contact" />
      </Grid>
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

Contact.propTypes = {
  setSnackbar: PropTypes.func.isRequired,
  requestContact: PropTypes.func.isRequired,
  inputDisabled: PropTypes.bool.isRequired,
  setInputDisabled: PropTypes.func.isRequired,
  networkStatus: PropTypes.oneOf(['default', 'success', 'error', 'transferring']).isRequired,
  setNetworkStatus: PropTypes.func.isRequired,
};

export default Contact;
