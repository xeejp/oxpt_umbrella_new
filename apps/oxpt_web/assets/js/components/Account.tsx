import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import clsx from 'clsx';
import 'animate.css';

/* Material UI */
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import LoadingButton from '@material-ui/lab/LoadingButton';
import InputAdornment from '@material-ui/core/InputAdornment';

import ErrorIcon from '@material-ui/icons/Error';
import CheckIcon from '@material-ui/icons/Check';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CircularProgress from '@material-ui/core/CircularProgress';
import FAQ from '../containers/FAQ';

const inputContent = {
  default: { status: 'success', icon: null, helperText: 'top.inputDefault' },
  success: { status: 'success', icon: <CheckCircleIcon />, helperText: 'top.inputSuccess' },
  error: { status: 'error', icon: <ErrorIcon />, helperText: 'top.inputError' },
  loading: { status: 'loading', icon: <CheckIcon />, helperText: 'top.inputLoading' },
  checked: { status: 'checked', icon: <CheckIcon />, helperText: 'top.inputChecked' },
};

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 0,
    padding: 0,
  },
  titleGrid: {
    padding: theme.spacing(1),
  },
  title: {
    paddingTop: theme.spacing(1),
  },
  contactGrid: {
    padding: theme.spacing(1),
  },
  divider: {
    borderColor: theme.palette.primary.light,
    borderWidth: theme.spacing(0.5),
    borderRadius: theme.shape.borderRadius,
  },
  dividerContact: {
    margin: theme.spacing(2),
  },
  formItem: {
    margin: theme.spacing(1),
  },
  buttonItem: {
    padding: theme.spacing(2),
  },
  buttonFacebook: {
    textTransform: 'none',
    padding: theme.spacing(2),
    borderColor: '#3b5998',
    color: '#3b5998',
    backgroundColor: theme.palette.primary.contrastText,
    '&:hover': {
      color: theme.palette.primary.contrastText,
      backgroundColor: '#3b5998',
      borderColor: '#3b5998',
    },
  },
  facebookIcon: {
    width: '22px',
    height: '22px',
  },
  googleIcon: {
    width: '22px',
    height: '22px',
    backgroundColor: theme.palette.common.white,
    borderColor: theme.palette.common.white,
    borderRadius: 2,
    borderWidth: '2px',
    borderStyle: 'solid',
  },
  googleButtonText: {
    color: '#4285F4',
    backgroundColor: theme.palette.common.white,
    borderColor: theme.palette.common.white,
    borderRadius: 2,
    borderWidth: '2px',
    borderStyle: 'solid',
    justifyContent: 'baseline',
  },
  buttonGoogle: {
    textTransform: 'none',
    padding: theme.spacing(2),
    borderColor: '#4285F4',
    color: '#4285F4',
    backgroundColor: theme.palette.primary.contrastText,
    '&:hover': {
      color: theme.palette.primary.contrastText,
      backgroundColor: '#4285F4',
      borderColor: '#4285F4',
    },
  },
  imageGrid: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    height: '30vmin',
    objectFit: 'contain',
  },
  accordion: {
    padding: theme.spacing(1),
  },
  answer: {
    textIndent: '1em',
    color: theme.palette.text.secondary,
  },
  iconChecked: {
    color: theme.palette.text.primary,
  },
  iconSuccess: {
    color: theme.palette.success.main,
  },
  iconError: {
    color: theme.palette.error.main,
  },
}));

function Account(props) {
  const classes = useStyles();
  const { t } = useTranslation('components');
  const {
    setSnackbar,
    requestSignUp,
    requestSignIn,
    inputDisabled,
    setInputDisabled,
    networkStatus,
    setNetworkStatus,
    accountToken,
  } = props;
  const [localUsername, setLocalUsername] = useState('');
  const [localPassword, setLocalPassword] = useState('');
  const [usernameStatus, setUsernameStatus] = useState('default');
  const [passwordStatus, setPasswordStatus] = useState('default');
  const history = useHistory();
  const timerRef = useRef(null);

  useEffect(() => {
    if (networkStatus === 'success') {
      setUsernameStatus('success');
      setPasswordStatus('success');
      timerRef.current = window.setTimeout(() => {
        setNetworkStatus({ networkStatus: 'default' });
        setInputDisabled({ inputDisabled: false });
        history.push(`/put_account_token/${accountToken}/room_management`);
        window.location.reload(false);
      }, 2300);
    }

    return () => {
      clearTimeout(timerRef.current);
    };
  }, [networkStatus]);

  const iconUsernameClassName = clsx({
    [classes.iconChecked]: usernameStatus === 'checked',
    [classes.iconSuccess]: usernameStatus === 'success',
    [classes.iconError]: usernameStatus === 'error',
  });

  const iconPasswordClassName = clsx({
    [classes.iconChecked]: passwordStatus === 'checked',
    [classes.iconSuccess]: passwordStatus === 'success',
    [classes.iconError]: passwordStatus === 'error',
  });

  function ValidationUsername(username) {
    let errorMessage;
    if (username.trim().length === 0) {
      errorMessage = 'infoSnackbar.account.signUp.error.usernameBlank';
    } else if (username.length < 4) {
      errorMessage = 'infoSnackbar.account.signUp.error.usernameTooShort';
    } else if (username.length > 64) {
      errorMessage = 'infoSnackbar.account.signUp.error.usernameTooLong';
    } else {
      errorMessage = 'success';
    }
    return errorMessage;
  }

  function ValidationPassword(password) {
    let errorMessage;
    if (password.trim().length === 0) {
      errorMessage = 'infoSnackbar.account.signUp.error.passwordBlank';
    } else if (password.length < 4) {
      errorMessage = 'infoSnackbar.account.signUp.error.passwordTooShort';
    } else if (password.length > 64) {
      errorMessage = 'infoSnackbar.account.signUp.error.passwordTooLong';
    } else {
      errorMessage = 'success';
    }
    return errorMessage;
  }

  const handleRequestSignUp = () => {
    const errorMessagePassword = ValidationPassword(localPassword);
    if (errorMessagePassword !== 'success') {
      setPasswordStatus('error');
      setSnackbar({
        snackbarTitle: 'infoSnackbar.account.signUp.title',
        snackbarMessage: errorMessagePassword,
        snackbarSeverity: 'error',
        snackbarOpen: true,
      });
    }

    const errorMessageUsername = ValidationUsername(localUsername);
    if (errorMessageUsername !== 'success') {
      setUsernameStatus('error');
      setSnackbar({
        snackbarTitle: 'infoSnackbar.account.signUp.title',
        snackbarMessage: errorMessageUsername,
        snackbarSeverity: 'error',
        snackbarOpen: true,
      });
    }

    if (errorMessageUsername === 'success' && errorMessagePassword === 'success' && networkStatus !== 'transferring') {
      setUsernameStatus('checked');
      setPasswordStatus('checked');
      setInputDisabled({ inputDisabled: true });
      setNetworkStatus({ networkStatus: 'transferring' });
      requestSignUp({ username: localUsername, password: localPassword });
    }
  };

  const handleRequestSignIn = () => {
    const errorMessagePassword = ValidationPassword(localPassword);
    if (errorMessagePassword !== 'success') {
      setPasswordStatus('error');
      setSnackbar({
        snackbarTitle: 'infoSnackbar.account.signIn.title',
        snackbarMessage: errorMessagePassword,
        snackbarSeverity: 'error',
        snackbarOpen: true,
      });
    }

    const errorMessageUsername = ValidationUsername(localUsername);
    if (errorMessageUsername !== 'success') {
      setUsernameStatus('error');
      setSnackbar({
        snackbarTitle: 'infoSnackbar.account.signIn.title',
        snackbarMessage: errorMessageUsername,
        snackbarSeverity: 'error',
        snackbarOpen: true,
      });
    }

    if (errorMessageUsername === 'success' && errorMessagePassword === 'success' && networkStatus !== 'transferring') {
      setUsernameStatus('checked');
      setPasswordStatus('checked');
      setInputDisabled({ inputDisabled: true });
      setNetworkStatus({ networkStatus: 'transferring' });
      requestSignIn({ username: localUsername, password: localPassword });
    }
  };

  const handleUsername = (event) => {
    setLocalUsername(event.target.value.trim());
    if (event.target.value !== '') {
      if (ValidationUsername(localUsername) === 'success') {
        setUsernameStatus('default');
      } else {
        setUsernameStatus('error');
      }
    }
  };

  const handlePassword = (event) => {
    setLocalPassword(event.target.value.trim());
    if (event.target.value !== '') {
      if (ValidationPassword(localPassword) === 'success') {
        setPasswordStatus('default');
      } else {
        setPasswordStatus('error');
      }
    }
  };

  const handleUsernameInputBlur = (event) => {
    if (usernameStatus !== 'success') {
      if (event.target.value !== '') {
        if (ValidationUsername(localUsername) === 'success') {
          setUsernameStatus('checked');
        } else {
          setUsernameStatus('error');
        }
      } else {
        setUsernameStatus('default');
      }
    }
  };

  const handlePasswordInputBlur = (event) => {
    if (passwordStatus !== 'success') {
      if (event.target.value !== '') {
        if (ValidationPassword(localPassword) === 'success') {
          setPasswordStatus('checked');
        } else {
          setPasswordStatus('error');
        }
      } else {
        setPasswordStatus('default');
      }
    }
  };

  const handleInputKeyDown = (event) => {
    const ENTER = 13;
    if (event.keyCode === ENTER && !inputDisabled) {
      handleRequestSignIn();
    }
  };

  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="flex-start"
      className={classes.root}
    >
      <Hidden smDown><Grid item /></Hidden>
      <Grid item xs={12} sm={4}>
        <Grid
          container
          direction="column"
          justifyContent="flex-start"
          alignItems="stretch"
          className={classes.titleGrid}
        >
          <Typography variant="h3" gutterBottom component="div" className={classes.title}>{t('account.title')}</Typography>
          <Divider className={classes.divider} />
          <Typography variant="h5" gutterBottom component="div" className={classes.title}>{t('account.subtitle')}</Typography>
        </Grid>
        <Grid item xs={12} sm={12}>
          <FormControl fullWidth>
            <TextField
              id="username"
              label={t('account.username')}
              autoFocus
              className={classes.formItem}
              error={networkStatus === 'error' || usernameStatus === 'error'}
              InputProps={{
                readOnly: false,
                endAdornment: (
                  <InputAdornment position="end" className={iconUsernameClassName}>
                    { inputContent[usernameStatus].icon}
                  </InputAdornment>
                ),
              }}
              onChange={(event) => handleUsername(event)}
              onBlur={(event) => handleUsernameInputBlur(event)}
              value={localUsername}
            />
            <TextField
              id="password"
              label={t('account.password')}
              type="password"
              className={classes.formItem}
              error={networkStatus === 'error' || passwordStatus === 'error'}
              InputProps={{
                readOnly: false,
                endAdornment: (
                  <InputAdornment position="end" className={iconPasswordClassName}>
                    { inputContent[passwordStatus].icon}
                  </InputAdornment>
                ),
              }}
              onChange={(event) => handlePassword(event)}
              onBlur={(event) => handlePasswordInputBlur(event)}
              onKeyDown={(event) => handleInputKeyDown(event)}
              value={localPassword}
            />
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="stretch"
            >
              <Grid item xs={6} sm={6} className={classes.titleGrid}>
                <LoadingButton
                  pending={networkStatus === 'transferring'}
                  className={classes.buttonItem}
                  pendingPosition="end"
                  variant="contained"
                  size="large"
                  fullWidth
                  onClick={handleRequestSignUp}
                  disabled={inputDisabled}
                  pendingIndicator={
                    <CircularProgress color="inherit" size={24} />
                  }
                  endIcon={<div />}
                >
                  {t('account.signUp')}
                </LoadingButton>
              </Grid>
              <Grid item xs={6} sm={6} className={classes.titleGrid}>
                <LoadingButton
                  pending={networkStatus === 'transferring'}
                  className={classes.buttonItem}
                  pendingPosition="end"
                  variant="contained"
                  size="large"
                  fullWidth
                  onClick={handleRequestSignIn}
                  disabled={inputDisabled}
                  pendingIndicator={
                    <CircularProgress color="inherit" size={24} />
                  }
                  endIcon={<div />}
                >
                  {t('account.signIn')}
                </LoadingButton>
              </Grid>
            </Grid>
          </FormControl>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={4}>
        <FAQ filter="account" />
      </Grid>
      <Hidden smDown><Grid item /></Hidden>
    </Grid>
  );
}

Account.propTypes = {
  setSnackbar: PropTypes.func.isRequired,
  requestSignUp: PropTypes.func.isRequired,
  requestSignIn: PropTypes.func.isRequired,
  inputDisabled: PropTypes.bool.isRequired,
  setInputDisabled: PropTypes.func.isRequired,
  networkStatus: PropTypes.oneOf(['default', 'success', 'error', 'transferring']).isRequired,
  setNetworkStatus: PropTypes.func.isRequired,
  accountToken: PropTypes.string.isRequired,
};

export default Account;
