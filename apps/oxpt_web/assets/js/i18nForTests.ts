import i18n from 'i18next';
import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';

import { jaCommon } from './locales/Common';
import { jaComponents } from './locales/Components';

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources: {
      ja: {
        common: jaCommon,
        components: jaComponents,
      },
    },
    lng: 'ja',
    fallbackLng: 'ja',

    // have a common namespace used around the full app
    ns: ['common', 'components'],
    defaultNS: 'components',

    debug: true,

    interpolation: {
      escapeValue: false, // not needed for react!!
    },
  });

export default i18n;
