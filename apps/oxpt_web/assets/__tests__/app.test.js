import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';


import { render, screen, cleanup } from '@testing-library/react';
import { I18nextProvider } from 'react-i18next';
import i18n from '../js/i18nForTests';
import store from '../js/redux/store';

import About from '../js/containers/Footer'
import Account from '../js/containers/Account'
import Award from '../js/containers/Award'
import Contact from '../js/containers/Contact'
import CreateRoom from '../js/containers/CreateRoom'
import DeleteAccount from '../js/containers/DeleteAccount'
import FAQ from '../js/containers/FAQ'
import Footer from '../js/containers/Footer'
import GameList from '../js/containers/GameList'
import Header from '../js/containers/Header'
import Loading from '../js/containers/Loading'
import Menu from '../js/components/Menu'
import Room from '../js/components/Room'
import RoomList from '../js/containers/RoomList'
import RoomManagement from '../js/containers/RoomManagement'
import SnackbarInfo from '../js/components/SnackbarInfo'
import Team from '../js/containers/Team'
import TermsOfService from '../js/containers/TermsOfService'
import Top from '../js/containers/Top'
import Usage from '../js/containers/Usage'

describe('Basic Rendering Test', () => {
  afterEach(cleanup)
  test('About', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <About />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/XEEとは/);
  });

  test('Account', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <Account />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/新規登録/);
  });

  test('Award', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <Award />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/受賞歴と研究助成金/);
  });

  test('Contact', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <Contact />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/お問い合わせ/);
  });

  test('CreateRoom', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <CreateRoom />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/ルーム開設/);
  });

  test('DeleteAccount', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <DeleteAccount />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/アカウント削除/);
  });

  test('FAQ', () => {
    window.scrollTo = jest.fn();
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <FAQ filter={"contact"}/>
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/FAQ/);
  });

  test('Footer', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <Footer />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/More about XEE/);
  });

  test('GameList', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <GameList />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/ゲーム一覧/);
  });

  test('Header', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <Header />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/XEE.JP/);
  });

  test('Loading', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <Loading />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByRole('progressbar', { hidden: true })
  });

  test('Menu', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <Menu menuOpen={true} setMenuOpen={() => {}}/>
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/トップページ/);
  });

  test('Room', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <Room gameId="test_game_id" guestId="test_guest_id" />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getByTitle('Room Frame');
  });

  test('RoomList', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <RoomList />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/ルーム一覧/);
  });

  test('RoomManagement', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <RoomManagement />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/一時ルーム/);
  });

  test('SnackbarInfo', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <SnackbarInfo
              snackbarTitle={'infoSnackbar.account.signUp.title'}
              snackbarMessage={'infoSnackbar.account.signUp.success'}
              snackbarSeverity={'success'}
              snackbarOpen={true}
              setSnackbar={() => {}}
            />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

screen.getAllByText(/新規登録/);
  });

  test('Team', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <Team />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/開発チーム/);
  });

  test('TermsOfService', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <TermsOfService />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/利用規約/);
  });

  test('Top', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <Top />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/ルーム名/);
  });

  test('Usage', () => {
    render(
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <BrowserRouter>
            <Usage />
          </BrowserRouter>
        </I18nextProvider>
      </Provider>);

    screen.getAllByText(/使い方/);
  });
});