defmodule Oxpt.Lounge.Host.GuestManager do
  use Cizen.Automaton
  defstruct [:lounge_id]

  use Cizen.Effects

  alias Cizen.Filter
  alias Oxpt.JoinGame

  alias Oxpt.Lounge.MoveGameAll

  @impl true
  def spawn(%__MODULE__{lounge_id: lounge_id} = saga) do
    perform(%Subscribe{
      event_filter: Filter.new(fn %MoveGameAll{lounge_id: ^lounge_id} -> true end)
    })

    perform(%Subscribe{
      event_filter:
        Filter.new(fn %JoinGame{game_id: ^lounge_id, host: host} ->
          host != true
        end)
    })

    state = %{
      saga: saga,
      guests: MapSet.new([])
    }

    {:loop, state}
  end

  @impl true
  def yield({:loop, socket}) do
    event = perform(%Receive{})

    socket = handle_event_body(event, socket)

    {:loop, socket}
  end

  defp handle_event_body(%MoveGameAll{game_id: game_id}, state) do
    state.guests
    |> MapSet.to_list()
    |> Enum.each(fn guest_id ->
      perform(%Dispatch{
        body: %JoinGame{
          game_id: game_id,
          guest_id: guest_id
        }
      })

      :timer.sleep(20)
    end)

    state
  end

  defp handle_event_body(%JoinGame{guest_id: guest_id}, state) do
    %{state | guests: state.guests |> MapSet.put(guest_id)}
  end
end
