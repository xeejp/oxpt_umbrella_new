defmodule Oxpt.Lounge.Host.HostSocket do
  defstruct [:game_id, :guest_id, :channel_pid]

  use GenServer

  alias Cizen.{Dispatcher, Filter}
  alias Oxpt.Player.{Input, Output}
  alias Oxpt.Lounge.Host.Connected

  alias Oxpt.Lounge.Host.{
    AddGames,
    RemoveGame,
    ResumePreviousGame,
    StartNextGame,
    Show,
    ShowLounge,
    FinishGame,
    Log,
    ResetLog,
    AddDummyGuest,
    UpdateHostState
  }

  require Filter

  @impl GenServer
  def init(%__MODULE__{game_id: game_id} = socket) do
    Dispatcher.listen(Filter.new(fn %UpdateHostState{lounge_host_id: ^game_id} -> true end))

    Dispatcher.dispatch(%Connected{game_id: game_id})

    {:ok, socket}
  end

  @impl GenServer
  def handle_info(%UpdateHostState{event: event, state: state}, socket) do
    Dispatcher.dispatch(%Output{
      game_id: socket.game_id,
      guest_id: socket.guest_id,
      event: event,
      payload: state
    })

    {:noreply, socket}
  end

  def handle_input(%Input{event: "add_games", payload: %{"games" => games}} = input) do
    Dispatcher.dispatch(%AddGames{lounge_host_id: input.game_id, games: games})
  end

  def handle_input(%Input{event: "remove_game", payload: %{"index" => index}} = input) do
    Dispatcher.dispatch(%RemoveGame{lounge_host_id: input.game_id, index: index})
  end

  def handle_input(%Input{event: "resume_previous_game"} = input) do
    Dispatcher.dispatch(%ResumePreviousGame{
      lounge_host_id: input.game_id,
      guest_id: input.guest_id
    })
  end

  def handle_input(%Input{event: "start_next_game"} = input) do
    Dispatcher.dispatch(%StartNextGame{lounge_host_id: input.game_id})
  end

  def handle_input(%Input{event: "show", payload: %{"index" => index}} = input) do
    Dispatcher.dispatch(%Show{lounge_host_id: input.game_id, index: index})
  end

  def handle_input(%Input{event: "show_lounge"} = input) do
    Dispatcher.dispatch(%ShowLounge{lounge_host_id: input.game_id})
  end

  def handle_input(%Input{event: "finish_game"} = input) do
    Dispatcher.dispatch(%FinishGame{lounge_host_id: input.game_id})
  end

  def handle_input(%Input{event: "log", payload: %{"index" => index}} = input) do
    Dispatcher.dispatch(%Log{lounge_host_id: input.game_id, index: index})
  end

  def handle_input(%Input{event: "reset_log"} = input) do
    Dispatcher.dispatch(%ResetLog{lounge_host_id: input.game_id})
  end

  def handle_input(%Input{event: "add_dummy_guest"} = input) do
    Dispatcher.dispatch(%AddDummyGuest{lounge_host_id: input.game_id})
  end
end
