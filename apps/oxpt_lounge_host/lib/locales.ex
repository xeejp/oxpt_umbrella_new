defmodule Oxpt.Lounge.Host.Locales do
  def get do
    %{
      en: %{
        translations: %{
          label_lounge: "Lounge",
          label_resume_previous_game: "Resume previous game",
          label_start_next_game: "Start next game",
          label_show_current_game: "Show game in progress",
          label_show_lounge: "Back to Lounge",
          label_finish_game: "Finish this game",
          label_game_table: "Sequence",
          label_index: "No",
          label_actions: "Actions",
          label_game: "Game",
          label_category: "Category",
          label_status: "Status",
          label_add_game_dialog: "Add Games",
          label_log_dialog: "Game Log",
          label_cancel: "Cancel",
          label_add_selected_games: "Add selected games",
          label_download: "Download",
          text_watch_done_game: """
            Displaying a finished game.
          """,
          text_watch_current_game: """
            Displaying a game in progress.
          """,
          text_lounge: """
            In the Lounge, you can configure the sequence of games.At the end of a game, participants are returned to the lounge. As a host, you can return here and reconfigure the sequence at any time during any game.
          """,
          text_suggest_add_games: """
            The pending game is empty in sequence. Click the add button to call up the adding games dialog.
          """,
          text_log_dialog: """
            Are you sure want to download this game log?
          """,
          label_attendance: "Attendance",
          label_chat: "Chat",
          label_boilerplate: "Boilerplate",
          label_beauty_contest: "Beauty Contest",
          label_double_auction: "Double Auction",
          label_questionnaire: "Questionnaire",
          label_ultimatum_game: "Ultimatum Game",
          label_public_goods: "Public Goods Game",
          category_other: "Other",
          category_game_theory: "Game Theory",
          category_market_mechanism: "Market Mechanism",
          category_behavioural_economics: "Behavioural Economics",
          category_questionnaire: "Questionnaire"
        }
      },
      ja: %{
        translations: %{
          label_lounge: "ラウンジ",
          label_resume_previous_game: "前のゲームを再開",
          label_start_next_game: "次のゲームを開始",
          label_show_current_game: "進行中のゲーム",
          label_show_lounge: "ラウンジ",
          label_finish_game: "終了",
          label_game_table: "ゲーム順序",
          label_index: "進行",
          label_actions: "アクション",
          label_game: "ゲーム",
          label_category: "カテゴリー",
          label_status: "ステータス",
          label_add_game_dialog: "ゲームを追加",
          label_log_dialog: "ゲームログ",
          label_cancel: "キャンセル",
          label_add_selected_games: "選択中のゲームを追加",
          label_download: "ダウンロード",
          text_watch_done_game: """
            終了したゲームを表示しています。
          """,
          text_watch_current_game: """
            進行中のゲームを表示しています。
          """,
          text_lounge: """
            ラウンジではゲーム順序を設定することができます。ゲーム終了時には参加者はラウンジに戻ります。ホストのあなたは、いつでもラウンジに戻ることができ、順序を再設定することができます。
          """,
          text_suggest_add_games: """
            準備中のゲームがありません。追加ボタンをクリックしてゲームを追加しましょう。
          """,
          text_log_dialog: """
            ゲームログをダウンロードします。よろしいですか？
          """,
          label_attendance: "出席管理",
          label_chat: "チャット",
          label_boilerplate: "ボイラープレート",
          label_beauty_contest: "美人投票ゲーム",
          label_double_auction: "ダブルオークション",
          label_questionnaire: "アンケート",
          label_public_goods: "公共財ゲーム",
          label_ultimatum_game: "最後通牒ゲーム",
          category_other: "その他",
          category_game_theory: "ゲーム理論",
          category_market_mechanism: "市場メカニズム",
          category_behavioural_economics: "行動経済学",
          category_questionnaire: "アンケート"
        }
      }
    }
  end
end
