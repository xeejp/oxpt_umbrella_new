defmodule Oxpt.Lounge.MoveGameAll do
  @enforce_keys [:lounge_id, :game_id]
  defstruct [:lounge_id, :game_id]
end

defmodule Oxpt.Lounge.FetchPreloads do
  @enforce_keys [:lounge_id, :guest_id]
  defstruct @enforce_keys
end

defmodule Oxpt.Lounge.UpdatePreloadsAll do
  @enforce_keys [:lounge_id]
  defstruct @enforce_keys ++ [:preloads]
end

defmodule Oxpt.Lounge.UpdatePreloads do
  @enforce_keys [:lounge_id, :guest_id]
  defstruct @enforce_keys ++ [:preloads, :base_url]
end

defmodule Oxpt.Lounge.Host.UpdateHostState do
  @enforce_keys [:lounge_host_id, :state]
  defstruct @enforce_keys ++ [event: "update_state"]
end

defmodule Oxpt.Lounge.Host.Connected do
  @enforce_keys [:game_id]
  defstruct [:game_id]
end

defmodule Oxpt.Lounge.Host.AddGames do
  @enforce_keys [:lounge_host_id, :games]
  defstruct [:lounge_host_id, :games]
end

defmodule Oxpt.Lounge.Host.RemoveGame do
  @enforce_keys [:lounge_host_id, :index]
  defstruct [:lounge_host_id, :index]
end

defmodule Oxpt.Lounge.Host.ResumePreviousGame do
  @enforce_keys [:lounge_host_id, :guest_id]
  defstruct [:lounge_host_id, :guest_id]
end

defmodule Oxpt.Lounge.Host.StartNextGame do
  @enforce_keys [:lounge_host_id]
  defstruct [:lounge_host_id]
end

defmodule Oxpt.Lounge.Host.Show do
  @enforce_keys [:lounge_host_id, :index]
  defstruct [:lounge_host_id, :index]
end

defmodule Oxpt.Lounge.Host.ShowLounge do
  @enforce_keys [:lounge_host_id]
  defstruct [:lounge_host_id]
end

defmodule Oxpt.Lounge.Host.FinishGame do
  @enforce_keys [:lounge_host_id]
  defstruct [:lounge_host_id]
end

defmodule Oxpt.Lounge.Host.Log do
  @enforce_keys [:lounge_host_id, :index]
  defstruct [:lounge_host_id, :index]
end

defmodule Oxpt.Lounge.Host.ResetLog do
  @enforce_keys [:lounge_host_id]
  defstruct [:lounge_host_id]
end

defmodule Oxpt.Lounge.Host.AddDummyGuest do
  @enforce_keys [:lounge_host_id]
  defstruct [:lounge_host_id]
end

defmodule Oxpt.Lounge.Host.AddedDummyGuest do
  @enforce_keys [:game_id, :dummy_guest_list]
  defstruct [:game_id, :dummy_guest_list]
end
