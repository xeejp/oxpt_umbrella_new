defmodule Oxpt.Lounge.Host do
  use Cizen.Automaton
  defstruct [:room_id, :lounge_id, :params]

  alias Cizen.{Filter, Saga}

  use Cizen.Effects

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../..") |> Path.expand(),
    index: "src/index.jsx",
    oxpt_path: "../oxpt"

  alias Oxpt.Lounge.Host.{
    Connected,
    AddGames,
    RemoveGame,
    ResumePreviousGame,
    StartNextGame,
    Show,
    ShowLounge,
    FinishGame,
    Log,
    ResetLog,
    AddDummyGuest,
    AddedDummyGuest,
    UpdateHostState
  }

  alias Oxpt.Persistence
  alias Oxpt.Lounge.{MoveGameAll, FetchPreloads, UpdatePreloadsAll, UpdatePreloads}
  alias Oxpt.Lounge.Host.{GuestManager, Locales}
  alias Oxpt.JoinGame
  alias Oxpt.Guest
  alias Oxpt.GuestRegistry
  alias Oxpt.Game
  alias Oxpt.Game.JoinGuest
  alias Oxpt.GetLog

  # @impl Oxpt.Game
  # def watch_assets(out_dir, public_url) do
  # end

  @impl Oxpt.Game
  def new(room_id, params) do
    # lounge_id means game_id of oxpt_lounge
    lounge_id = params[:lounge_id]
    params = params[:params]
    %__MODULE__{room_id: room_id, lounge_id: lounge_id, params: params}
  end

  @impl Oxpt.Game
  def join_guest(%JoinGuest{game_id: game_id, guest_id: guest_id}) do
    GenServer.start(__MODULE__.HostSocket, %__MODULE__.HostSocket{
      game_id: game_id,
      guest_id: guest_id
    })

    %JoinGuest.Joined{game_id: game_id}
  end

  @impl Oxpt.Game
  def input(input) do
    __MODULE__.HostSocket.handle_input(input)
  end

  @impl Oxpt.Game
  def request(_request) do
    # __MODULE__.HostSocket.handle_request(input)
  end

  @impl true
  def spawn(%__MODULE__{lounge_id: lounge_id, room_id: room_id, params: params}) do
    id = Saga.self()
    Persistence.Game.setup(id, room_id, params[:persists?])

    perform(%Subscribe{
      event_filter: Filter.new(fn %JoinGame{game_id: ^id, host: true} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %Connected{game_id: ^id} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %AddGames{lounge_host_id: ^id} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %RemoveGame{lounge_host_id: ^id} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %ResumePreviousGame{lounge_host_id: ^id} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %StartNextGame{lounge_host_id: ^id} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %Show{lounge_host_id: ^id} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %ShowLounge{lounge_host_id: ^id} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %FinishGame{lounge_host_id: ^id} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %Log{lounge_host_id: ^id} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %ResetLog{lounge_host_id: ^id} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %AddDummyGuest{lounge_host_id: ^id} -> true end)
    })

    perform(%Subscribe{
      event_filter: Filter.new(fn %FetchPreloads{lounge_id: ^lounge_id} -> true end)
    })

    perform(%Fork{
      saga: %GuestManager{lounge_id: lounge_id}
    })

    params = Enum.into(params, %{})

    games =
      Enum.map(
        params.games,
        fn {name, {module, opts}} ->
          {name, %{game: {module, opts}, metadata: module.metadata()}}
        end
      )

    initial_state = %{
      page: "lounge",
      lounge_id: lounge_id,
      room_id: room_id,
      games: games,
      sequences: [{:lounge, games[:lounge]}],
      current_game_index: 0,
      current_finished_game_index: 0,
      current_watch_game_index: 0,
      started_games: [lounge_id],
      host_guest_id: nil,
      host_ids: [nil],
      host_game_ids: [nil],
      locales: Locales.get(),
      dummy_guest_list: [],
      base_url: base_url(params[:cdn_host])
    }

    {:loop, initial_state}
  end

  @impl true
  def yield({:loop, state}) do
    event = perform(%Receive{})

    state = handle_event_body(event, state)

    {:loop, state}
  end

  defp handle_event_body(%JoinGame{guest_id: host_guest_id}, state) do
    %{state | host_guest_id: host_guest_id}
  end

  defp handle_event_body(%FetchPreloads{guest_id: guest_id}, state) do
    perform(%Dispatch{
      body: %UpdatePreloads{
        lounge_id: state.lounge_id,
        guest_id: guest_id,
        preloads: preloads(state),
        base_url: state.base_url
      }
    })

    state
  end

  defp handle_event_body(%Connected{}, state) do
    update_state(state)
  end

  defp handle_event_body(%AddGames{games: games}, state) do
    sequences =
      state.sequences ++
        Enum.map(games, fn game ->
          atom = String.to_existing_atom(game)
          {atom, state.games[atom]}
        end)

    state = %{state | sequences: sequences}

    update_preloads_all(state)

    update_state(state)
  end

  defp handle_event_body(%RemoveGame{index: index}, state) do
    state = %{state | sequences: List.delete_at(state.sequences, index)}

    update_preloads_all(state)

    update_state(state)
  end

  defp handle_event_body(%Show{index: index}, state) do
    # if in range
    if state.current_game_index >= index && index > 0 do
      state = %{state | current_watch_game_index: index, page: "game"}

      update_state(state)
    else
      state
    end
  end

  defp handle_event_body(%ShowLounge{}, state) do
    state = %{
      state
      | page: "lounge",
        current_watch_game_index: 0
    }

    update_state(state)
  end

  defp handle_event_body(%ResumePreviousGame{}, state) do
    id = Saga.self()
    next_game_index = state.current_finished_game_index

    state = %{
      state
      | page: "game",
        current_watch_game_index: next_game_index,
        current_game_index: next_game_index,
        current_finished_game_index: next_game_index - 1
    }

    perform(%Dispatch{
      body: %UpdateHostState{
        lounge_host_id: id,
        state: client_state(state)
      }
    })

    perform(%Dispatch{
      body: %MoveGameAll{
        lounge_id: state.lounge_id,
        game_id: Enum.at(state.started_games, next_game_index)
      }
    })

    update_preloads_all(state)

    state
  end

  defp handle_event_body(%StartNextGame{}, state) do
    prev_index = state.current_game_index
    # if in range
    state =
      if state.current_game_index + 1 < length(state.sequences) do
        next_game_index = state.current_game_index + 1

        %{
          state
          | current_game_index: next_game_index,
            current_watch_game_index: next_game_index,
            page: "game"
        }
      else
        state
      end

    index = state.current_game_index
    # if not started
    state =
      if index == length(state.started_games) do
        {_name, %{game: {module, opts}}} = Enum.at(state.sequences, index)

        game = module.new(state.room_id, opts)

        game_id =
          perform(%Fork{
            saga: game
          })

        host =
          perform(%Fork{
            saga: %Guest{room_id: state.room_id}
          })

        %JoinGuest.Joined{game_id: host_game_id} =
          module.join_guest(%JoinGuest{
            game_id: game_id,
            guest_id: host,
            host: true
          })

        perform(%Dispatch{
          body: %JoinGame{
            game_id: host_game_id,
            guest_id: host,
            host: true
          }
        })

        started_games = List.insert_at(state.started_games, index, game_id)
        host_ids = List.insert_at(state.host_ids, index, host)
        host_game_ids = List.insert_at(state.host_game_ids, index, host_game_id)

        %{state | started_games: started_games, host_ids: host_ids, host_game_ids: host_game_ids}
      else
        state
      end

    if prev_index != index do
      update_state(state)

      perform(%Dispatch{
        body: %MoveGameAll{
          lounge_id: state.lounge_id,
          game_id: Enum.at(state.started_games, index)
        }
      })
    end

    update_preloads_all(state)

    state
  end

  defp handle_event_body(%FinishGame{}, state) do
    state = %{
      state
      | page: "lounge",
        current_watch_game_index: 0,
        current_finished_game_index: state.current_game_index
    }

    update_state(state)

    perform(%Dispatch{
      body: %MoveGameAll{
        lounge_id: state.lounge_id,
        game_id: Enum.at(state.started_games, 0)
      }
    })

    update_preloads_all(state)

    state
  end

  defp handle_event_body(%Log{index: index}, state) do
    id = Saga.self()
    game_id = Enum.at(state.host_game_ids, index)

    name =
      state.sequences
      |> Enum.at(index)
      |> elem(0)

    log =
      Saga.call(game_id, %GetLog{
        game_id: game_id,
        guest_id: state.host_guest_id
      })

    perform(%Dispatch{
      body: %UpdateHostState{
        lounge_host_id: id,
        state: %{temp_log: %{name: name, game_id: game_id, log: log}}
      }
    })

    state
  end

  defp handle_event_body(%ResetLog{}, state) do
    id = Saga.self()

    perform(%Dispatch{
      body: %UpdateHostState{
        lounge_host_id: id,
        state: %{temp_log: nil}
      }
    })

    state
  end

  defp handle_event_body(%AddDummyGuest{}, state) do
    id = Saga.self()
    guest_id = perform(%Fork{saga: %Guest{room_id: state.room_id, dummy: true}})

    perform(%Dispatch{
      body: %JoinGame{
        game_id: state.started_games |> Enum.at(state.current_game_index),
        guest_id: guest_id
      }
    })

    new_state =
      update_in(state, [:dummy_guest_list], fn list ->
        [guest_id | list]
      end)

    perform(%Dispatch{
      body: %UpdateHostState{
        lounge_host_id: id,
        event: "guest added",
        state: %{
          game_id: state.started_games |> Enum.at(state.current_game_index),
          dummy_guest_id: guest_id,
          dummy_guest_list: get_in(new_state, [:dummy_guest_list])
        }
      }
    })

    perform(%Dispatch{
      body: %AddedDummyGuest{
        game_id: state.started_games |> Enum.at(state.current_game_index),
        dummy_guest_list: get_in(new_state, [:dummy_guest_list])
      }
    })

    new_state
  end

  defp update_state(state) do
    id = Saga.self()

    perform(%Dispatch{
      body: %UpdateHostState{
        lounge_host_id: id,
        state: client_state(state)
      }
    })

    state
  end

  defp client_state(state) do
    %{
      page: state.page,
      games:
        Enum.map(state.games, fn {name, %{metadata: metadata}} ->
          %{name: name, label: metadata.label, category: metadata.category}
        end),
      host_ids: state.host_ids,
      host_game_ids: state.host_game_ids,
      sequences:
        Enum.map(state.sequences, fn {name, %{metadata: metadata}} ->
          %{name: name, label: metadata.label, category: metadata.category}
        end),
      current_game_index: state.current_game_index,
      current_finished_game_index: state.current_finished_game_index,
      current_watch_game_index: state.current_watch_game_index,
      locales: state.locales
    }
  end

  defp preloads(state) do
    state.sequences
    |> Enum.slice(state.current_finished_game_index + 1, 2)
    |> Enum.map(fn {name, %{metadata: metadata}} ->
      %{name: name, label: metadata.label, category: metadata.category}
    end)
  end

  defp update_preloads_all(state),
    do:
      perform(%Dispatch{
        body: %UpdatePreloadsAll{
          lounge_id: state.lounge_id,
          preloads: preloads(state)
        }
      })

  defp base_url(nil), do: nil
  defp base_url(host), do: "https://" <> host
end
