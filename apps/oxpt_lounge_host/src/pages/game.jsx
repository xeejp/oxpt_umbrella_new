import React from "react";
import { useTranslation } from "react-i18next";

import { useTheme, makeStyles } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Button from "@material-ui/core/Button";
import Chip from "@material-ui/core/Chip";
import IconButton from "@material-ui/core/IconButton";
import PersonAddIcon from "@material-ui/icons/PersonAdd";

import Frame from "../components/frame";
import { handleShowLounge, handleFinishGame, handleAddDummyGuest } from "../actions";
import { getStatusIcon } from "../components/status";
import i18nInstance from "../i18n";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: 0,
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end",
    paddingBottom: theme.spacing(1),
  },
  button: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  buttonChip: {
    marginTop: 3,
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
}));

export default ({ guestId, gameId, currentWatchStatus }) => {
  const classes = useStyles();
  const [t] = useTranslation("translations", { i18nInstance });
  const theme = useTheme();
  const isLabel = !useMediaQuery(theme.breakpoints.down("xs"));
  const statusIcon = getStatusIcon(currentWatchStatus);

  const handleClickAdd = () => {
    console.log(gameId + ":" + guestId);
    handleAddDummyGuest();
  };

  return (
    <div className={classes.root}>
      <div className={classes.buttons}>
        {currentWatchStatus == "in_progress" && (
          <IconButton onClick={handleClickAdd}>
            <PersonAddIcon />
          </IconButton>
        )}
        {isLabel ? (
          currentWatchStatus == "done" ? (
            <Chip
              variant="outlined"
              label={t("text_watch_done_game")}
              icon={statusIcon}
              className={classes.buttonChip}
            />
          ) : (
            <Chip
              variant="outlined"
              label={t("text_watch_current_game")}
              icon={statusIcon}
              className={classes.buttonChip}
            />
          )
        ) : null}
        <Button variant="outlined" onClick={handleShowLounge} className={classes.button}>
          {t("label_show_lounge")}
        </Button>
        {currentWatchStatus == "in_progress" && (
          <Button variant="outlined" onClick={handleFinishGame} className={classes.button} color="secondary">
            {t("label_finish_game")}
          </Button>
        )}
      </div>
      <Frame gameId={gameId} guestId={guestId} />
    </div>
  );
};
