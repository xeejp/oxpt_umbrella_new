import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import GameTable from "../components/game_table";
import AddGameDialog from "../components/add_game_dialog";
import LogDialog from "../components/log_dialog";
import { handleStartNextGame, handleResumePreviousGame, handleShow } from "../actions";
import i18nInstance from "../i18n";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: 0,
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end",
    paddingBottom: theme.spacing(1),
  },
  button: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  suggest: {
    marginTop: theme.spacing(1),
    textAlign: "center",
  },
}));

export default ({ games, sequences, currentGameIndex, currentFinishedGameIndex, tempLog }) => {
  const classes = useStyles();
  const [t] = useTranslation("translations", { i18nInstance });
  const [openAddGame, setOpenAddGame] = useState(false);
  const [openLog, setOpenLog] = useState(false);

  const handleOpenAddGame = () => setOpenAddGame(true);
  const handleCloseAddGame = () => setOpenAddGame(false);

  const notAnyFinished = currentFinishedGameIndex == 0;
  const isPending = currentFinishedGameIndex == currentGameIndex;
  const isEmpty = sequences.length - 1 <= currentFinishedGameIndex;

  useEffect(() => {
    if (tempLog) setOpenLog(true);
    else setOpenLog(false);
  }, [tempLog]);

  return (
    <div className={classes.root}>
      <Typography variant="h5">{t("label_lounge")}</Typography>
      <Typography variant="subtitle1">{t("text_lounge")}</Typography>
      <div className={classes.buttons}>
        {isPending ? (
          <>
            <Button
              onClick={handleResumePreviousGame}
              disabled={notAnyFinished}
              className={classes.button}
              color="secondary"
              variant="contained"
            >
              {t("label_resume_previous_game")}
            </Button>
            <Button
              onClick={handleStartNextGame}
              disabled={isEmpty}
              className={classes.button}
              color="primary"
              variant="contained"
            >
              {t("label_start_next_game")}
            </Button>
          </>
        ) : (
          <Button onClick={handleShow(currentGameIndex)} className={classes.button} color="primary" variant="outlined">
            {t("label_show_current_game")}
          </Button>
        )}
      </div>
      <AddGameDialog games={games} open={openAddGame} handleClose={handleCloseAddGame} />
      <LogDialog log={tempLog} open={openLog} />
      <GameTable
        sequences={sequences}
        currentGameIndex={currentGameIndex}
        currentFinishedGameIndex={currentFinishedGameIndex}
        handleOpen={handleOpenAddGame}
      />
      {isEmpty && (
        <div className={classes.suggest}>
          <Typography component="p" variant="body1">
            {t("text_suggest_add_games")}
          </Typography>
        </div>
      )}
    </div>
  );
};
