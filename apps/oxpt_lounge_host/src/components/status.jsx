import React from "react";
import DoneIcon from "@material-ui/icons/Done";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import CachedIcon from "@material-ui/icons/Cached";

export function getStatus(index, currentGameIndex, currentFinishedGameIndex) {
  if (index <= currentFinishedGameIndex) return "done";
  else if (index == currentGameIndex) return "in_progress";
  else return "pending";
}

export function getStatusIcon(status) {
  switch (status) {
    case "done":
      return <DoneIcon />;
    case "in_progress":
      return <PlayArrowIcon />;
    case "pending":
      return <CachedIcon />;
  }
}
