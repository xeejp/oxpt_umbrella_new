import React from "react";
import { useTranslation } from "react-i18next";

import { useTheme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import { handleResetLog } from "../actions";
import i18nInstance from "../i18n";

export default ({ log, open }) => {
  const theme = useTheme();
  const [t] = useTranslation("translations", { i18nInstance });
  const fullscreen = useMediaQuery(theme.breakpoints.down("xs"));

  const handleDownload = () => {
    const filename = `${log.name}_${log.gameId}.csv`;
    var element = document.createElement("a");
    element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(log.log.join("\n")));
    element.setAttribute("download", filename);

    element.style.display = "none";
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);

    handleResetLog();
  };

  return (
    <Dialog onClose={handleResetLog} aria-labelledby="customized-dialog-title" open={open} fullScreen={fullscreen}>
      <DialogTitle id="customized-dialog-title" onClose={handleResetLog}>
        {t("label_log_dialog")}
      </DialogTitle>
      <DialogContent dividers>
        <DialogContentText id="alert-dialog-description">{t("text_log_dialog")}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleResetLog} color="secondary">
          {t("label_cancel")}
        </Button>
        <Button onClick={handleDownload} color="primary">
          {t("label_download")}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
