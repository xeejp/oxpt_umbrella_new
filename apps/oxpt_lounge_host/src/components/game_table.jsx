import React from "react";
import { useTranslation } from "react-i18next";

import { useTheme, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Toolbar from "@material-ui/core/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

import IconButton from "@material-ui/core/IconButton";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import VisibilityIcon from "@material-ui/icons/Visibility";
import GetAppIcon from "@material-ui/icons/GetApp";
import useMediaQuery from "@material-ui/core/useMediaQuery";

import { handleShow, handleRemoveGame, handleLog } from "../actions";
import { getStatus, getStatusIcon } from "./status";
import i18nInstance from "../i18n";

function getActions(index, status) {
  switch (status) {
    case "done":
    case "in_progress":
      return (
        <>
          <IconButton aria-label="add" size="small" onClick={handleShow(index)}>
            <VisibilityIcon />
          </IconButton>
          <IconButton aria-label="add" size="small" onClick={handleLog(index)}>
            <GetAppIcon />
          </IconButton>
        </>
      );
    case "pending":
      return (
        <>
          <IconButton aria-label="add" size="small" onClick={handleRemoveGame(index)}>
            <DeleteIcon />
          </IconButton>
        </>
      );
  }
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(1),
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(1),
  },
  table: {
    minWidth: 300,
  },
  tableWrapper: {
    maxHeight: 400,
    overflow: "auto",
  },
  toolbar: {
    paddingLeft: 0,
    paddingRight: 0,
  },
  title: {
    flex: "1 1 100%",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

export default ({ sequences, currentGameIndex, currentFinishedGameIndex, handleOpen }) => {
  const classes = useStyles();
  const theme = useTheme();
  const isWide = useMediaQuery(theme.breakpoints.up("sm"));
  const [t] = useTranslation("translations", { i18nInstance });

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Toolbar className={classes.toolbar}>
          <Typography className={classes.title} variant="h6" id="tableTitle">
            {t("label_game_table")}
          </Typography>
          <Tooltip title="Filter list">
            <Fab aria-label="filter list" onClick={handleOpen} color="primary" size="small">
              <AddIcon />
            </Fab>
          </Tooltip>
        </Toolbar>
        <div className={classes.tableWrapper}>
          <Table className={classes.table} aria-labelledby="tableTitle" aria-label="game-list table">
            <TableHead>
              <TableRow>
                <TableCell key="index" align="left">
                  {t("label_index")}
                </TableCell>
                <TableCell key="actions" align="left">
                  {t("label_actions")}
                </TableCell>
                <TableCell key="game" align="left">
                  {t("label_game")}
                </TableCell>
                {isWide && (
                  <TableCell key="category" align="left">
                    {t("label_category")}
                  </TableCell>
                )}
                <TableCell key="state" align="right">
                  {t("label_status")}
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {sequences.map((row, index) => {
                if (index == 0) return null;
                const status = getStatus(index, currentGameIndex, currentFinishedGameIndex);
                const statusIcon = getStatusIcon(status);
                const actions = getActions(index, status);
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={`${index}-${row.name}`}>
                    <TableCell component="th" scope="row">
                      {index}
                    </TableCell>
                    <TableCell align="left">{actions}</TableCell>
                    <TableCell align="left">{t(row.label)}</TableCell>
                    {isWide && <TableCell align="left">{t(row.category)}</TableCell>}
                    <TableCell align="right">{statusIcon}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </div>
      </Paper>
    </div>
  );
};
