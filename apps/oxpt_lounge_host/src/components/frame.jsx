import React, { useEffect, useRef } from "react";

import { useTranslation } from "react-i18next";
import { makeStyles } from "@material-ui/core/styles";

import i18next from "i18next";

const useStyles = makeStyles(() => ({
  iframe: {
    margin: 0,
    padding: 0,
    width: '100%',
    verticalAlign: "top",
  }
}));

export default ({ gameId, guestId }) => {
  const classes = useStyles();
  const { t } = useTranslation("common");
  const iframeRef = useRef();
  const timerRef = useRef();

  useEffect(() => {
    i18next.on("languageChanged", function (lang) {
      if (document.getElementById("inlineInlineFrame")) {
        document.getElementById("inlineInlineFrame").contentWindow.postMessage({ lang: lang }, "*");
      }
    });
  }, []);

  useEffect(() => {
    window.addEventListener("resize", reSize, false)
    return () => {
      clearTimeout(timerRef.current)
      window.removeEventListener("resize", reSize, false)
    }
  }, []);

  function reSize () {
    if(iframeRef.current.style.height !== iframeRef.current.contentWindow.document.body.scrollHeight + "px") {
      timerRef.current = window.setTimeout(() => {
        iframeRef.current.style.height = iframeRef.current.contentWindow.document.body.scrollHeight + "px"
        reSize()
      }, 50);
    }else{
      clearTimeout(timerRef.current)
    }
  }

  return (
    (gameId !== "" && guestId !== "")
      ? <iframe
        ref={iframeRef}
        id='inlineInlineFrame'
        title='Room Frame'
        scrolling='no'
        allow='fullscreen'
        frameBorder={0}
        src={`/game/${gameId}/${guestId}`}
        className={classes.iframe}
        marginHeight={0}
        marginWidth={0}
        onLoad={() => {reSize()}}
        onChange={() => {reSize()}}
        />
      : <div>{t('common:loading')}</div>
  )
}
