import React, {useState, useCallback, useEffect } from "react";
import ReactDOM from "react-dom";
import channel from './socket'

import { useTranslation } from "react-i18next";

import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import Lounge from "./pages/lounge";
import Game from "./pages/game";
import { getStatus } from "./components/status";
import i18nInstance from "./i18n";

import fromSnakeToCamel from "./util/index";

channel.join();
channel.on("guest added", ({ game_id: gameId, dummy_guest_id: dummyGuestId }) => {
  window.open(`/game/${gameId}/${dummyGuestId}`);
});
let state = {};

const useForceUpdate = () => {
  const [, setTick] = useState(0);
  const update = useCallback(() => {
    setTick((tick) => tick + 1);
  }, []);
  return update;
};

const useStyles = makeStyles((theme) => ({
  container: {
    padding: 0,
    margin: 0,
  },
}));

const LoungeHost = (state) => {
  const {
    currentGameIndex,
    currentWatchGameIndex,
    currentFinishedGameIndex,
    sequences,
    games,
    hostGameIds,
    hostIds,
    page,
    tempLog,
    locales,
  } = state;
  const classes = useStyles();
  const [, i18n] = useTranslation("translations", { i18nInstance });
  const forceUpdate = useForceUpdate();

  const messageListener = (event) => {
    if (event.data.lang && event.data.lang !== i18n.language) {
      i18n.changeLanguage(event.data.lang);
    }
  };

  useEffect(() => {
    window.addEventListener("message", messageListener, false);
    return () => window.removeEventListener("message", messageListener, false);
  }, []);

  useEffect(() => {
    locales &&
      Object.keys(locales).map((lang) => {
        Object.keys(locales[lang]).map((namespace) => {
          i18n.addResourceBundle(lang, namespace, locales[lang][namespace]);
        });
      });
    forceUpdate();
  }, [locales]);

  const gameId = hostGameIds[currentWatchGameIndex];
  const guestId = hostIds[currentWatchGameIndex];
  const currentWatchStatus = getStatus(currentWatchGameIndex, currentGameIndex, currentFinishedGameIndex);

  if (!locales) return null;

  return (
    <Container maxWidth="xl" className={classes.container}>
      {page === "lounge" ? (
        <Lounge
          games={games}
          sequences={sequences}
          page={page}
          currentGameIndex={currentGameIndex}
          currentFinishedGameIndex={currentFinishedGameIndex}
          tempLog={tempLog}
        />
      ) : page === "game" ? (
        <Game gameId={gameId} guestId={guestId} currentWatchStatus={currentWatchStatus} />
      ) : null}
    </Container>
  );
};

channel.on("update_state", (next) => {
  if (process.env.NODE_ENV !== "production") console.log(fromSnakeToCamel(next));
  state = { ...state, ...fromSnakeToCamel(next) };
  ReactDOM.render(<LoungeHost {...state} />, document.getElementById("root"));
});
