const toCamel = (str) => str.replace(/([-_]\w)/g, (g) => g[1].toUpperCase());

export const fromSnakeToCamel = (obj) => {
  if (typeof obj !== "object" || obj === null) {
    return obj;
  } else {
    const result = {};
    Object.keys(obj).forEach((key) => {
      if (obj[key] instanceof Object && !(obj[key] instanceof Array) && key !== "locales" && key !== "players") {
        result[toCamel(key)] = fromSnakeToCamel(obj[key]);
      } else {
        result[toCamel(key)] = obj[key];
      }
    });
    return result;
  }
};

export default fromSnakeToCamel;
