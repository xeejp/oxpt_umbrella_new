import { Socket } from "phoenix";

const guestId = _oxpt.guest_id;
const gameId = _oxpt.game_id;

let socket = new Socket("/game_socket", { params: {}} )
socket.connect();
const channel = socket.channel(`game:${guestId}:${gameId}`, {});
export default channel;
