import channel from './socket'

export function handleAddGames(games) {
  return () => channel.push("input", { event: "add_games", payload: { games } });
}

export function handleRemoveGame(index) {
  return () => channel.push("input", { event: "remove_game", payload: { index } });
}

export function handleResumePreviousGame() {
  channel.push("input", { event: "resume_previous_game", payload: {} });
}

export function handleStartNextGame() {
  channel.push("input", { event: "start_next_game", payload: {} });
}

export function handleShow(index) {
  return () => channel.push("input", { event: "show", payload: { index } });
}

export function handleShowLounge() {
  channel.push("input", { event: "show_lounge", payload: {} });
}

export function handleFinishGame() {
  channel.push("input", { event: "finish_game", payload: {} });
}

export function handleLog(index) {
  return () => channel.push("input", { event: "log", payload: { index } });
}

export function handleResetLog() {
  channel.push("input", { event: "reset_log", payload: {} });
}

export function handleAddDummyGuest() {
  channel.push("input", { event: "add_dummy_guest", payload: {} });
}
