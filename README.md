<img src="https://xee.jp/images/logo_front-7a6f93f53a1d84c8361695491f9180b9.png?vsn=d" height="100px">
> # Oxpt.Umbrella
> ## Online Experiment System

[![pipeline status](https://gitlab.com/xeejp/oxpt_umbrella/badges/master/pipeline.svg)](https://gitlab.com/xeejp/oxpt_umbrella/commits/master)

# Test

```sh
gauge run
```

# Installation for Mac

## oxpt_installer

[oxpt_installer](https://gitlab.com/xeejp/oxpt_installer)
[How to use oxpt_installer](https://gitlab.com/xeejp/oxpt_installer/blob/master/README.md)

# Installation for Windows
[Windowsへのインストール方法(WSL)](https://gitlab.com/xeejp/oxpt_umbrella/blob/master/setup_ja_wsl.md)
Installation guide for windows using command line is as follows:

## Run Windows PowerShell as Administrator

1. If you have the <kbd>Win</kbd> + <kbd>X</kbd> menu set to show Windows PowerShell instead of Command Prompt, then press the <kbd>Win</kbd> + <kbd>X</kbd> keys to open the <kbd>Win</kbd> + <kbd>X</kbd> menu.
2. Click/tap on Windows PowerShell.

## Install Chocolatey

```sh
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

## Install Elixir(>=1.7.4)

```sh
cup all
choco install elixir
```

## Run Command Prompt

1. Press the <kbd>Win</kbd> key.
2. Type `cmd` and press <kbd>Enter</kbd> key or click/tap "OK" button.

## Install Phoenix Framework(>=1.4.0)

Type below and press Enter key at the Command Prompt.

```sh
mix local.hex
mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez
```

## Register GitLab

```sh
ssh-keygen -t rsa -C "mail@domain.com" -b 4096
type %homepath%\.ssh\id_rsa.pub | clip
```

Paste your public SSH key on GitLab > acconts > Settings > SSH Keys > Key textarea.
Then press "Add key" button.

## Docker Compose
```
docker volume create --name=pgdata # first time
docker-compose up
```

## Install Node.js(>=10.13.0 LTS)

[NodeJS](https://nodejs.org/ja/)
Install "LTS" version.

## Visual Studio

Visit [Visual Studio Download Page](https://www.visualstudio.com/downloads/) and download "Visual Studio Community 2017" because it's free.
Install "C++ Desktop Development" from top page and "Git for Windows" from "Individual components".
Make sure that `vcvrsall.bat` has been installed under the `C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build`.
After you install these, run Visual Studio and open new project at once.

## Install PostgreSQL(>=10.5)

```bash
docker run -d -p 5432:5432 -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres -e POSTGRES_DB=oxpt_dev postgres:alpine
```

[PostgreSQL](https://www.postgresql.org/download/)
Install latest version.
User: postgres
Password: postgres

## Clone oxpt_umbrella

[XEE Project](https://gitlab.com/xeejp)

```sh
git clone git@gitlab.com:xeejp/oxpt_umbrella.git
cd oxpt_umbrella
mix deps.get
```

## Compile

```sh
cmd /K "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" amd64
mix deps.compile
```

## Setting Database

```sh
mix ecto.create
mix ecto.migrate
```

## webpack(>=4.25.1)

```sh
cd apps\oxpt_web\assets
npm install
npm run deploy
cd ..\..\..\
```

## Start OXPT

```sh
mix phx.server
```

And Access `http://localhost:4000`.
