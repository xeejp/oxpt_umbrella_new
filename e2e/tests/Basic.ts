import { Step } from "gauge-ts";
const { 
  goto,
  text,
} = require('taiko');
import assert = require("assert");

export default class StepImplementation {

  @Step("Top Page")
  public async openTopPage() {
    await goto("localhost:4000");
    assert.ok(await text("参加する").exists());
  }

  @Step("About Page")
  public async openAboutPage() {
    await goto("localhost:4000/about");
    assert.ok(await text("XEEとは").exists());
  }

  @Step("Account Page")
  public async openAccountPage() {
    await goto("localhost:4000/account");
    assert.ok(await text("新規登録").exists());
  }

  @Step("Award Page")
  public async openAwardPage() {
    await goto("localhost:4000/award");
    assert.ok(await text("受賞歴と研究助成金").exists());
  }

  @Step("Contact Page")
  public async openContactPage() {
    await goto("localhost:4000/contact");
    assert.ok(await text("お問い合わせ").exists());
  }

  @Step("CreateRoom Page")
  public async openCreateRoomPage() {
    await goto("localhost:4000/create_room");
    assert.ok(await text("ルーム開設").exists());
  }

  @Step("DeleteAccount Page")
  public async openDeleteAccountPage() {
    await goto("localhost:4000/delete_account");
    assert.ok(await text("アカウント削除").exists());
  }

  @Step("FAQ Page")
  public async openFAQPage() {
    await goto("localhost:4000/faq");
    assert.ok(await text("FAQ").exists());
  }

  @Step("GameList Page")
  public async openGameListPage() {
    await goto("localhost:4000/game_list");
    assert.ok(await text("ゲーム一覧").exists());
  }

  @Step("RoomList Page")
  public async openRoomListPage() {
    await goto("localhost:4000/room_list");
    assert.ok(await text("ゲストルーム").exists());
  }

  @Step("RoomManagement Page")
  public async openRoomManagementPage() {
    await goto("localhost:4000/room_management");
    assert.ok(await text("一時ルーム").exists());
  }

  @Step("Team Page")
  public async openTeamPage() {
    await goto("localhost:4000/team");
    assert.ok(await text("開発チーム").exists());
  }

  @Step("TermsOfService Page")
  public async openTOSPage() {
    await goto("localhost:4000/terms_of_service");
    assert.ok(await text("利用規約").exists());
  }

  @Step("Usage Page")
  public async openUsagePage() {
    await goto("localhost:4000/usage");
    assert.ok(await text("使い方").exists());
  }
}