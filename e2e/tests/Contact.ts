import { Step } from "gauge-ts";
const { 
  into,
  button,
  textBox,
  click,
  write,
  text,
  clear,
  waitFor,
  hover
} = require('taiko');
import assert = require("assert");

let name: string = "Name";
let email: string = "test@xee.jp";
let subject: string = "Subject";
let body: string = "Body";

export default class StepImplementation {
  @Step("Open Contact Page")
  public async openContactPage() {
    await click("お問い合わせ");
    assert.ok(await text("お気軽にご連絡ください。").exists());
  }

  @Step("Contact Error: noName")
  public async contactNoName() {
    await clear(textBox({"id": "contact-name"}));
    await write("", into(textBox({"id": "contact-name"})));
    await click(button("送信する"));
    assert.ok(await text("お名前を入力してください。").exists());
    await hover("お問い合わせ");
  }

  @Step("Contact Error: noEmail")
  public async contactNoEmail() {
    await clear(textBox({"id": "contact-name"}));
    await write(name, into(textBox({"id": "contact-name"})));
    await clear(textBox({"id": "contact-mail-address"}));
    await write("", into(textBox({"id": "contact-mail-address"})));
    await click(button("送信する"));
    assert.ok(await text("メールアドレスを入力してください。").exists());
    await waitFor(button("送信する"));
    await hover("お問い合わせ");
  }

  @Step("Contact Error: invalidEmail")
  public async contactInvalidEmail() {
    await clear(textBox({"id": "contact-name"}));
    await write(name, into(textBox({"id": "contact-name"})));
    await clear(textBox({"id": "contact-mail-address"}));
    await write("abcdefg", into(textBox({"id": "contact-mail-address"})));
    await clear(textBox({"id": "contact-subject"}));
    await write("", into(textBox({"id": "contact-subject"})));
    await click(button("送信する"));
    assert.ok(await text("メールアドレスが間違っています。").exists());
    await waitFor(button("送信する"));
    await hover("お問い合わせ");
  }

  @Step("Contact Error: noSubject")
  public async contactNoSubject() {
    await clear(textBox({"id": "contact-name"}));
    await write(name, into(textBox({"id": "contact-name"})));
    await clear(textBox({"id": "contact-mail-address"}));
    await write(email, into(textBox({"id": "contact-mail-address"})));
    await clear(textBox({"id": "contact-subject"}));
    await write("", into(textBox({"id": "contact-subject"})));
    await click(button("送信する"));
    assert.ok(await text("件名を入力してください。").exists());
    await waitFor(button("送信する"));
    await hover("お問い合わせ");
  }

  @Step("Contact Error: noBody")
  public async contactNoBody() {
    await clear(textBox({"id": "contact-name"}));
    await write(name, into(textBox({"id": "contact-name"})));
    await clear(textBox({"id": "contact-mail-address"}));
    await write(email, into(textBox({"id": "contact-mail-address"})));
    await clear(textBox({"id": "contact-subject"}));
    await write(subject, into(textBox({"id": "contact-subject"})));
    await clear(textBox({"id": "contact-message"}));
    await write("", into(textBox({"id": "contact-message"})));
    await click(button("送信する"));
    assert.ok(await text("お問い合わせ内容を入力してください。").exists());
    await waitFor(button("送信する"));
    await hover("お問い合わせ");
  }

  @Step("Contact")
  public async contact() {
    await clear(textBox({"id": "contact-name"}));
    await write(name, into(textBox({"id": "contact-name"})));
    await clear(textBox({"id": "contact-mail-address"}));
    await write(email, into(textBox({"id": "contact-mail-address"})));
    await clear(textBox({"id": "contact-subject"}));
    await write(subject, into(textBox({"id": "contact-subject"})));
    await clear(textBox({"id": "contact-message"}));
    await write(body, into(textBox({"id": "contact-message"})));
    await click(button("送信する"));
    assert.ok(await text("メッセージが開発チームに送信されました。").exists());
    await hover("お問い合わせ");
  }
}