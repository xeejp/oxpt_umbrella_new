import { Step, BeforeSuite, AfterSuite } from "gauge-ts";
const { 
  openBrowser,
  goto,
  button,
  click,
  closeBrowser, 
  evaluate,
  reload
} = require('taiko');
import assert = require("assert");

export default class StepImplementation {
  @BeforeSuite()
  public async beforeSuite() {
    await openBrowser({ headless: false });
  }

  @AfterSuite()
  public async afterSuite() {
    await closeBrowser();
  };

  @Step("Open OXPT local site")
  public async openOxpt() {
    await goto("localhost:4000");
  }

  @Step("Reload OXPT local site")
  public async reloadOxpt() {
    await reload("localhost:4000", {ignoreCache: true});
  }

  @Step("Back to Top")
  public async backToTop() {
    await click(button({"aria-label": "メニュー"}));
    await click("トップページ");
  }

  @Step("Clear all tasks")
    public async clearAllTasks() {
    // @ts-ignore
    await evaluate(() => localStorage.clear());
  }
}