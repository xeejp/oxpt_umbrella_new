import { Step } from "gauge-ts";
const { 
  into,
  focus,
  button,
  textBox,
  click,
  write,
  text,
  below,
  clear
} = require('taiko');
import assert = require("assert");

let roomKey: string = Date.now().toString();

export default class StepImplementation {
  @Step("Create New Room")
  public async CreateNewRoom() {
    await click(button({"aria-label": "メニュー"}));
    await click("ルーム開設");
    await write(roomKey);
    await click("開設する");
    await text("新しいルームが開設されました。").exists();
    assert.ok(await text("ラウンジではゲーム順序を設定することができます。").exists());
  }

  @Step("Create Exist Room")
  public async CreateExistRoom() {
    await click(button({"aria-label": "メニュー"}));
    await click("ルーム開設");
    await write(roomKey);
    await click("開設する");
    assert.ok(await text("すでに使用されているルーム名です。").exists());
  }

  @Step("RoomKey Error: noRoomKey")
  public async noRoomKey() {
    await focus(textBox({"id": "room-key"}));
    await write("", into(textBox({"id": "room-key"})));
    await click("参加する");
    assert.ok(await text("ルーム名を入力してください。").exists());
  }

  @Step("Join Not Exist Room as Guest")
  public async JoinNotExistRoomAsGuest() {
    await focus(textBox({"id": "room-key"}));
    await clear(textBox({"id": "room-key"}));
    await write(roomKey + "_NOT_EXIST", into(textBox({"id": "room-key"})));
    await click("参加する");
    assert.ok(await text("存在しないルーム名です。").exists());
  }

  @Step("Join Exist Room as Guest")
  public async JoinExistRoomAsGuest() {
    await focus(textBox({"id": "room-key"}));
    await clear(textBox({"id": "room-key"}));
    await write(roomKey, into(textBox({"id": "room-key"})));
    await click("参加する");
    assert.ok(await text("しばらくお待ちください。").exists());
  }

  @Step("Get Guest Room List")
  public async GetGuestRoomList() {
    await click(button({"aria-label": "メニュー"}));
    await click("ルーム一覧");
    assert.ok(await text(roomKey).exists());
  }

  @Step("Join via Guest Room List")
  public async JoinViaGuestRoomList() {
    await click(roomKey);
    assert.ok(await text("しばらくお待ちください。").exists());
  }

  @Step("Get Host Room List")
  public async GetHostRoomList() {
    await click(button({"aria-label": "メニュー"}));
    await click("ルーム管理");
    assert.ok(await text(roomKey).exists());
  }

  @Step("Join via Host Room List")
  public async JoinViaHostRoomList() {
    await click(button({"aria-label": "play"}, below(text(roomKey))));
    await click(roomKey);
    assert.ok(await text("ラウンジではゲーム順序を設定することができます。").exists());
  }
}