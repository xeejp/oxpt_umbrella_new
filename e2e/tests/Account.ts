import { Step } from "gauge-ts";
const { 
  into,
  button,
  textBox,
  click,
  write,
  text,
  clear,
  waitFor,
  focus,
  image
} = require('taiko');
import assert = require("assert");

let username: string = Date.now().toString();
let password: string = Date.now().toString();

export default class StepImplementation {
  @Step("Open Account Page")
  public async OpenAccountPage() {
    await click(button({"aria-label": "account"}));
    await click("新規登録 / サインイン");
    assert.ok(await text("新規登録").exists());
  }

  @Step("Username Error: noUsername")
  public async noUsername() {
    await clear(textBox({"id": "username"}));
    await write("", into(textBox({"id": "username"})));
    await click(button("新規登録"));
    assert.ok(await text("ユーザ名が入力されていません。").exists());
  }

  @Step("Username Error: shortUsername")
  public async shortUsername() {
    await clear(textBox({"id": "username"}));
    await write("1", into(textBox({"id": "username"})));
    await click(button("新規登録"));
    assert.ok(await text("ユーザ名が短すぎます。").exists());
  }

  @Step("Username Error: longUsername")
  public async longUsername() {
    await clear(textBox({"id": "username"}));
    await write("12345678901234567890123456789012345678901234567890123456789012345", into(textBox({"id": "username"})));
    await click(button("新規登録"));
    assert.ok(await text("ユーザ名が長すぎます。").exists());
  }

  @Step("Password Error: noPassword")
  public async noPassword() {
    await clear(textBox({"id": "username"}));
    await write(username, into(textBox({"id": "username"})));
    await clear(textBox({"id": "password"}));
    await write("", into(textBox({"id": "password"})));
    await click(button("新規登録"));
    assert.ok(await text("パスワードが入力されていません。").exists());
  }

  @Step("Password Error: shortPassword")
  public async shortPassword() {
    await clear(textBox({"id": "username"}));
    await write(username, into(textBox({"id": "username"})));
    await clear(textBox({"id": "password"}));
    await write("1", into(textBox({"id": "password"})));
    await click(button("新規登録"));
    assert.ok(await text("パスワードが短すぎます。").exists());
  }

  @Step("Password Error: longPassword")
  public async longPassword() {
    await clear(textBox({"id": "username"}));
    await write(username, into(textBox({"id": "username"})));
    await clear(textBox({"id": "password"}));
    await write("12345678901234567890123456789012345678901234567890123456789012345", into(textBox({"id": "password"})));
    await click(button("新規登録"));
    assert.ok(await text("パスワードが長すぎます。").exists());
  }

  @Step("Sign Up")
  public async SignUp() {
    await clear(textBox({"id": "username"}));
    await write(username, into(textBox({"id": "username"})));
    await clear(textBox({"id": "password"}));
    await write(password, into(textBox({"id": "password"})));
    await click(button("新規登録"));
    assert.ok(await text("登録しました。").exists());
    await waitFor(5000, image({"id": "signed"}));
  }

  @Step("Sign Out")
  public async SignOut() {
    await click(button({"aria-label": "account"}));
    await click("サインアウト");
    assert.ok(await text("サインアウトしました。").exists());
    await waitFor(5000, image({"id": "not-signed"}));
  }

  @Step("Username Error: notExistUsername")
  public async notExistUsername() {
    await clear(textBox({"id": "username"}));
    await write(username + "_NOT_EXIST", into(textBox({"id": "username"})));
    await clear(textBox({"id": "password"}));
    await write(password, into(textBox({"id": "password"})));
    await click(button("サインイン"));
    assert.ok(await text("ユーザが見つかりませんでした。").exists());
  }

  @Step("Password Error: invalidPassword")
  public async invalidPassword() {
    await clear(textBox({"id": "username"}));
    await write(username, into(textBox({"id": "username"})));
    await clear(textBox({"id": "password"}));
    await write(password + "_NOT_EXIST", into(textBox({"id": "password"})));
    await click(button("サインイン"));
    assert.ok(await text("ユーザ名かパスワードが間違っています。").exists());
  }

  @Step("Sign In")
  public async signIn() {
    await focus(textBox({"id": "username"}));
    await clear(textBox({"id": "username"}));
    await write(username, into(textBox({"id": "username"})));
    await clear(textBox({"id": "password"}));
    await write(password, into(textBox({"id": "password"})));
    await click(button("サインイン"));
    assert.ok(await text("サインインしました。").exists());
    await waitFor(5000, image({"id": "signed"}));
  }

  @Step("Sign Up with Exist Username")
  public async signUpWithExistUsername() {
    await clear(textBox({"id": "username"}));
    await write(username, into(textBox({"id": "username"})));
    await clear(textBox({"id": "password"}));
    await write(password, into(textBox({"id": "password"})));
    await click(button("新規登録"));
    assert.ok(await text("すでに登録されているユーザ名です。").exists());
  }

  @Step("Open Delete Page")
  public async openDeletePage() {
    await click(button({"aria-label": "account"}));
    await click(username);
    assert.ok(await text("アカウント削除").exists());
  }

  @Step("Delete Error: noUsername")
  public async deleteNoUsername() {
    await clear(textBox({"id": "username"}));
    await write("", into(textBox({"id": "username"})));
    await click(button("削除する"));
    assert.ok(await text("ユーザ名が一致しません。").exists());
  }

  @Step("Delete Error: notExistUsername")
  public async deleteNotExistUsername() {
    await clear(textBox({"id": "username"}));
    await write(username + "_NOT_EXIST", into(textBox({"id": "username"})));
    await click(button("削除する"));
    assert.ok(await text("ユーザ名が一致しません。").exists());
  }

  @Step("Sign In Again Error: notExistUsername")
  public async signInAgainNotExistUsername() {
    await focus(textBox({"id": "username"}));
    await clear(textBox({"id": "username"}));
    await write(username, into(textBox({"id": "username"})));
    await clear(textBox({"id": "password"}));
    await write(password, into(textBox({"id": "password"})));
    await click(button("サインイン"));
    assert.ok(await text("ユーザが見つかりませんでした。").exists());
  }

  @Step("Delete")
  public async Delete() {
    await clear(textBox({"id": "username"}));
    await write(username, into(textBox({"id": "username"})));
    await click(button("削除する"));
    assert.ok(await text("削除しました。").exists());
    await waitFor(5000, image({"id": "not-signed"}));
  }
}